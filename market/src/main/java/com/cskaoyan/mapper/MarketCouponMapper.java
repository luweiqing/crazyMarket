package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.MarketCoupon;



import com.cskaoyan.bean.po.MarketCouponExample;

import com.cskaoyan.bean.wx.coupon.WxCouponSelectListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketCouponMapper {
    long countByExample(MarketCouponExample example);

    int deleteByExample(MarketCouponExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCoupon record);

    int insertSelective(MarketCoupon record);

    List<MarketCoupon> selectByExample(MarketCouponExample example);

    MarketCoupon selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCoupon record, @Param("example") MarketCouponExample example);

    int updateByExample(@Param("record") MarketCoupon record, @Param("example") MarketCouponExample example);

    int updateByPrimaryKeySelective(MarketCoupon record);

    int updateByPrimaryKey(MarketCoupon record);

    /*
     * @Author makabaka
     * @Description //TODO 以下是WXCOUPON
     * @Date 11:06 2022/11/22
     * @Param
     * @return
     **/

    List<WxCouponSelectListVO> selectwxCouponlist(Integer userId, short status);

}