package com.cskaoyan.mapper;

import com.cskaoyan.bean.admin.role.vo.RoleSystemPermissionAVo;
import com.cskaoyan.bean.po.MarketPermissionOptions;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface MarketPermissionOptionsMapper {

    List<RoleSystemPermissionAVo> select();

    void insert(MarketPermissionOptions marketPermissionOptions);

    List<String> selectApi(@Param("list") ArrayList<String> permissions);
}
