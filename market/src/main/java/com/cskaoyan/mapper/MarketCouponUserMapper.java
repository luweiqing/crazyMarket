package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.MarketCouponUser;
import com.cskaoyan.bean.po.MarketCouponUserExample;
import com.cskaoyan.bean.wx.coupon.WxCouponSelectListVO;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface MarketCouponUserMapper {
    long countByExample(MarketCouponUserExample example);

    int deleteByExample(MarketCouponUserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCouponUser record);

    int insertSelective(MarketCouponUser record);

    List<MarketCouponUser> selectByExample(MarketCouponUserExample example);

    MarketCouponUser selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCouponUser record, @Param("example") MarketCouponUserExample example);

    int updateByExample(@Param("record") MarketCouponUser record, @Param("example") MarketCouponUserExample example);

    int updateByPrimaryKeySelective(MarketCouponUser record);

    int updateByPrimaryKey(MarketCouponUser record);


    List<WxCouponSelectListVO> selectByUserIdAndOrderPriceSum(@Param("orderPriceSum") BigDecimal orderPriceSum, @Param("userId") Integer userId, @Param("order") String order);


    int selectAvailableCouponLength(@Param("userId") int userId, @Param("goodsTotalPrice") Double goodsTotalPrice);


    int updateById(Integer id);
}