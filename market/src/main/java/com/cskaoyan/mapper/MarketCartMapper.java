package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.MarketCart;
import com.cskaoyan.bean.po.MarketCartExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketCartMapper {
    long countByExample(MarketCartExample example);

    int deleteByExample(MarketCartExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCart record);

    int insertSelective(MarketCart record);

    List<MarketCart> selectByExample(MarketCartExample example);

    MarketCart selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCart record, @Param("example") MarketCartExample example);

    int updateByExample(@Param("record") MarketCart record, @Param("example") MarketCartExample example);

    int updateByPrimaryKeySelective(MarketCart record);

    int updateByPrimaryKey(MarketCart record);

    Integer selectGoodsCount(int userId);

    Double selectGoodsAmount(int userId);

    Integer selectCheckedGoodsCount(int userId);

    Double selectCheckedGoodsAmount(int userId);
}