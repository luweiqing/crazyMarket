package com.cskaoyan.mapper;


import com.cskaoyan.bean.admin.category.vo.CategoryL1vo;



import com.cskaoyan.bean.admin.goods.GoodsCategoryVO;


// import com.cskaoyan.bean.admin.goods.GoodsCatAndBrandVO;




import com.cskaoyan.bean.po.MarketCategory;
import com.cskaoyan.bean.po.MarketCategoryExample;

import com.cskaoyan.bean.wx.cataloge.CatalogVo1;
import com.cskaoyan.bean.wx.cataloge.CatalogVo2;
import com.cskaoyan.bean.wx.cataloge.CatalogVo3;
import jdk.nashorn.internal.ir.LiteralNode;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MarketCategoryMapper {
    long countByExample(MarketCategoryExample example);

    int deleteByExample(MarketCategoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MarketCategory record);

    int insertSelective(MarketCategory record);

    List<MarketCategory> selectByExample(MarketCategoryExample example);

    MarketCategory selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") MarketCategory record, @Param("example") MarketCategoryExample example);

    int updateByExample(@Param("record") MarketCategory record, @Param("example") MarketCategoryExample example);

    int updateByPrimaryKeySelective(MarketCategory record);

    int updateByPrimaryKey(MarketCategory record);








    List<CategoryL1vo>select();


    GoodsCategoryVO[] selectIdAndName(int pid);

    List<CatalogVo1> selectCategoryList();

    CatalogVo2 selectCurrentCategory(int id);


    List<CatalogVo3> selectSubCategory(int id);
}