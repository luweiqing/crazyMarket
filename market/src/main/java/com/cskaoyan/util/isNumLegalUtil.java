package com.cskaoyan.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author
 * @version 1.0
 * @date 2022/11/22 19:49:38
 */
public class isNumLegalUtil {
    public static boolean isNumLegal(String input){
        String reExp ="^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";
        Pattern p = Pattern.compile (reExp);
        Matcher m =p.matcher (input);
        return m.matches ();
    }
}
