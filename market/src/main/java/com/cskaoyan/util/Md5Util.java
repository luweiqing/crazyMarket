package com.cskaoyan.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author lwq
 * @since 2022/11/23 20:39
 */

public class Md5Util {
    public static String getMd5(String content, String salt) {
        // 加盐
        content = content + "!" + salt + "&";
        byte[] contentBytes = content.getBytes();
        try {
            // 获得信息摘要算法
            MessageDigest messageDigest = MessageDigest.getInstance("md5");
            byte[] digestBytes = messageDigest.digest(contentBytes);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte digestByte : digestBytes) {
                int i = digestByte & 0xff;
                String str = Integer.toHexString(i);
                if (str.length() == 1) {
                    // 保证一个字节转换成两位十六进制数
                    stringBuffer.append(0);
                }
                stringBuffer.append(str);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
