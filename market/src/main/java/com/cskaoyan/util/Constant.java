package com.cskaoyan.util;


import java.util.HashMap;
import java.util.Map;

/**
 * @author lwq
 * @since 2022/11/20 22:44
 */

public class Constant {
    public static final String imgUrl = "http://localhost:8083/wx/storage/fetch/";
    public static final String diskUrl = "D:\\wx\\storage\\fetch\\";


    // 统计数据所用的常量数组
    public static final String[] USER_COLUMS = new String[]{"day", "users"};
    public static final String[] ORDER_COLUMS = new String[]{"day", "orders", "customers", "amount", "pcr"};
    public static final String[] GOODS_COLUMS = new String[]{"day", "orders", "products", "amount"};

    // 订单状态的常量
    public static Map<Integer, String> ORDER_STATUS_TEXT = new HashMap<>();

    // 快递公司的英文简写和名称
    public static Map<String, String> ORDER_SHIP_NAME = new HashMap<>();


    static {
        // 给订单状态赋值
        ORDER_STATUS_TEXT.put(101, "未付款");
        // ORDER_STATUS_TEXT.put(102, "用户取消");
        // ORDER_STATUS_TEXT.put(103, "系统取消");
        // 把上面两种状态合并回显
        ORDER_STATUS_TEXT.put(102, "已取消");
        ORDER_STATUS_TEXT.put(103, "已取消");

        ORDER_STATUS_TEXT.put(201, "已付款");
        ORDER_STATUS_TEXT.put(202, "订单取消，退款中");
        ORDER_STATUS_TEXT.put(203, "已退款");
        ORDER_STATUS_TEXT.put(301, "已发货");
        // ORDER_STATUS_TEXT.put(401, "用户收货");
        // ORDER_STATUS_TEXT.put(402, "系统发货");
        // 把上面两种状态合并回显
        ORDER_STATUS_TEXT.put(401, "已收货");
        ORDER_STATUS_TEXT.put(402, "已收货");
        // 给快递公司赋值
        ORDER_SHIP_NAME.put("ZTO", "中通快递");
        ORDER_SHIP_NAME.put("YTO", "圆通速递");
        ORDER_SHIP_NAME.put("YD", "韵达快递");
        ORDER_SHIP_NAME.put("YZPY", "邮政快递包裹");
        ORDER_SHIP_NAME.put("EMS", "EMS");
        ORDER_SHIP_NAME.put("DBL", "德邦快递");
        ORDER_SHIP_NAME.put("FAST", "快捷快递");
        ORDER_SHIP_NAME.put("ZJS", "宅急送");
        ORDER_SHIP_NAME.put("TNT", "TNT快递");
        ORDER_SHIP_NAME.put("UPS", "UPS");
        ORDER_SHIP_NAME.put("DHL", "DHL");
        ORDER_SHIP_NAME.put("FEDEX", "FEDEX联邦（国内件）");
        ORDER_SHIP_NAME.put("FEDEX_GJ", "FEDEX联邦（国外件）");

    }

    // 日志 操作类型
    // 安全操作
    public static final int OPERATION_TYPE_AUTH = 1;
    // 订单操作
    public static final int OPERATION_TYPE_ORDER = 2;
}
