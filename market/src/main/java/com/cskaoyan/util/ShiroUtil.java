package com.cskaoyan.util;

import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.bean.po.MarketUser;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

/**
 * @Description: shiro工具类，可调取对应方法获取session对象、MarketAdmin对象/MarketUser对象
 * @Author: Pinocao
 * @CreateTime: 2022-11-24  17:14
 * @Version: 1.0
 */
public class ShiroUtil {

    // 获取session
    public static Session getSession() {
        Subject subject = SecurityUtils.getSubject();
        Session session = subject.getSession();
        return session;
    }

    // 获取当前后台系统登录对象
    public static MarketAdmin getMarketAdmin() {
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principals = subject.getPrincipals();
        if (principals == null) {
            return null;
        }
        return (MarketAdmin) principals.getPrimaryPrincipal();
    }

    // 获取当前小程序登录对象
    public static MarketUser getMarketUser() {
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principals = subject.getPrincipals();
        if (principals == null) {
            return null;
        }
        return (MarketUser) principals.getPrimaryPrincipal();
    }

    // 获取后台系统当前登录用户id
    public static Integer getAdminId(){
        MarketAdmin marketAdmin = getMarketAdmin();
        if (marketAdmin == null){
            return null;
        }
        Integer adminId = marketAdmin.getId();
        return adminId;
    }

    // 获取小程序当前登录用户id
    public static Integer getUserId(){
        MarketUser marketUser = getMarketUser();
        if (marketUser == null){
            return null;
        }
        Integer userId = marketUser.getId();
        return userId;
    }

}