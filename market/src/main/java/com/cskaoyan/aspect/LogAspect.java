package com.cskaoyan.aspect;


import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.bean.po.MarketLog;
import com.cskaoyan.bean.po.MarketOrder;
import com.cskaoyan.mapper.MarketLogMapper;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.util.Constant;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * @author lwq
 * @since 2022/11/21 11:04
 */
@Component
@Aspect
public class LogAspect {
    @Autowired
    MarketLogMapper marketLogMapper;

    @Autowired
    HttpServletRequest request;
    @Autowired
    MarketOrderMapper marketOrderMapper;

    @Pointcut("@annotation(com.cskaoyan.annotation.LogAddAnnotation)")
    public void logAddPointcut() {

    }

    @Around("logAddPointcut()")
    public BaseRespVo logAdd(ProceedingJoinPoint proceedingJoinPoint) {
        Boolean status = true;
        BaseRespVo proceed = BaseRespVo.invalidData("失败");
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principals = subject.getPrincipals();
        MarketAdmin principal = null;
        if (principals != null) {
            principal = (MarketAdmin) principals.getPrimaryPrincipal();
        }
        try {
            proceed = (BaseRespVo) proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
            if (!"成功".equals(proceed.getErrmsg())) {
                status = false;
            }
        } catch (Throwable throwable) {
            if (throwable instanceof DuplicateKeyException) {
                proceed = BaseRespVo.invalidData("名称不能重复", 642);
            }
            status = false;
        }
        if (principals == null) {
            principals = subject.getPrincipals();
            principal = (MarketAdmin) principals.getPrimaryPrincipal();
        }
        // 当前管理员
        String admin = principal.getUsername();
        String ip = request.getRemoteAddr();
        String requestURI = request.getRequestURI();
        String[] split = requestURI.split("/");
        int type = 0;
        String action = "";
        String result = "";
        String comment = "";
        if ("auth".equals(split[2].trim())) {
            // 登录退出模块
            type = Constant.OPERATION_TYPE_AUTH;
            if ("login".equals(split[3].trim())) {
                action = "登录";
                if (!status) {
                    result = "认证失败";
                }
            }
            if ("logout".equals(split[3].trim())) {
                action = "退出";
            }
        } else if ("admin".equals(split[2].trim())) {
            // 管理员模块
            type = Constant.OPERATION_TYPE_AUTH;
            Object[] args = proceedingJoinPoint.getArgs();
            MarketAdmin marketAdmin = (MarketAdmin) args[0];
            if ("create".equals(split[3].trim())) {
                action = "添加管理员";
                result = marketAdmin.getUsername();
            }
            if ("update".equals(split[3].trim())) {
                action = "编辑管理员";
                result = marketAdmin.getUsername();
            }
            if ("delete".equals(split[3].trim())) {
                action = "删除管理员";
                result = marketAdmin.getUsername();
            }
        } else if ("order".equals(split[2].trim())) {
            // 订单模块
            type = Constant.OPERATION_TYPE_ORDER;
            Object[] args = proceedingJoinPoint.getArgs();
            Map orderMap = (Map) args[0];
            Integer orderId = (Integer) orderMap.get("orderId");
            if ("ship".equals(split[3].trim())) {
                action = "发货";
                MarketOrder marketOrder = marketOrderMapper.selectByPrimaryKey(orderId);
                result = "订单编号" + marketOrder.getOrderSn();
            }
        }
        MarketLog marketLog = new MarketLog(null, admin, ip, type, action, status, result, comment, new Date(), new Date(), false);
        marketLogMapper.insertSelective(marketLog);
        return proceed;
    }

}
