package com.cskaoyan.config.exception;

import com.cskaoyan.bean.common.BaseRespVo;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authz.AuthorizationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @Description: 异常捕获响应
 * @Author: Pinocao
 * @CreateTime: 2022-11-21  23:28
 * @Version: 1.0
 */
@RestControllerAdvice
public class ExceptionControllerAdvice {

    /*@ExceptionHandler(ArithmeticException.class)
    //@ResponseBody
    public BaseRespVo method1(ArithmeticException exception) {
        return BaseRespVo.fail(exception.getMessage());
    }
    @ExceptionHandler(NumberFormatException.class)
    public BaseRespVo method2(NumberFormatException exception) {
        return BaseRespVo.fail(exception.getMessage());
    }*/
    // 如果不同的异常，异常处理的业务逻辑不同的话，放到不同方法中；
    // 如果处理的逻辑相同，也可以放到同一个方法中AuthorizationException
    @ExceptionHandler(AuthorizationException.class)
    public BaseRespVo method2(Exception exception) {
        System.out.println("无访问权限：ExceptionHandler: " + exception.getMessage());
        return BaseRespVo.invalidParameter("无访问权限：ExceptionHandler: " + exception.getMessage());
    }

    @ExceptionHandler(IncorrectCredentialsException.class)
    public void method3(Exception exception) {
        System.out.println("认证失败：ExceptionHandler: " + exception.getMessage());
    }


    // // 手机号参数校验，如果位数不是11或不为空，就抛出异常
    // // 用于wx端的地址管理中的手机号校验
    // @ExceptionHandler(ExceptionWxAddressController.class)
    // public BaseRespVo telException(Exception exception) {
    //     return BaseRespVo.invalidParameter ("请输入11位手机号 " + exception.getMessage ());
    // }

    /**
     * 处理主键重复的异常
     *
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/24 10:54
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public BaseRespVo DuplicateKey() {
        return BaseRespVo.invalidData("名称不能重复", 642);
    }
}