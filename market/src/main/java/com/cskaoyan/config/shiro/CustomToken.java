package com.cskaoyan.config.shiro;

import org.apache.shiro.authc.UsernamePasswordToken;

/**
 * @Description: 自定义token，增加登录类型判断
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  17:02
 * @Version: 1.0
 */
public class CustomToken extends UsernamePasswordToken {
    //登录类型，判断是后台登陆，还是小程序登录
    private String loginType;

    public CustomToken(final String username, final String password, String loginType) {
        super(username,password);
        this.loginType = loginType;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

}