package com.cskaoyan.config.shiro;

import com.cskaoyan.util.StringUtil;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * @Description: Shiro框架代码使用，增强SessionManage中的方法，保证跨域场景下的session一致
 * @Author: Pinocao
 * @CreateTime: 2022-11-21  21:10
 * @Version: 1.0
 */
public class MarketSessionManage extends DefaultWebSessionManager {
    private static final String MARKET_ADMIN_TOKEN = "X-CskaoyanMarket-Admin-Token";// 后台系统
    private static final String MARKET_User_TOKEN = "X-CskaoyanMarket-Token";// 小程序

    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response) {
        // 强转后获取请求头
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String sessionId;
        if (httpServletRequest.getHeader(MARKET_ADMIN_TOKEN)!=null){
            sessionId = httpServletRequest.getHeader(MARKET_ADMIN_TOKEN);
        }else {
            sessionId = httpServletRequest.getHeader(MARKET_User_TOKEN);
        }
        if (!StringUtil.isEmpty(sessionId)){
            return sessionId;
        }
        return super.getSessionId(request, response);
    }
}