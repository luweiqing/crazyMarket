package com.cskaoyan.config;

import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.multipart.MultipartResolver;

import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author stone
 * @date 2022/01/06 16:19
 */

@Configuration
public class WebConfiguration implements WebMvcConfigurer {


    /*@Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("*")
                .allowedOrigins("*")
                .allowedMethods("*");
    }*/

    // @Override
    // public void addResourceHandlers(ResourceHandlerRegistry registry) {
    //     //  /** SpringMVC中的特殊的写法，多级任意url → /pic/banner.png  /pic/tmp/banner.png
    //
    //     registry.addResourceHandler("/**").addResourceLocations("file:f:/app2/");
    // }



    // @Bean
    // public MultipartResolver multipartResolver() {
    //     CommonsMultipartResolver commonsMultipartResolver = new CommonsMultipartResolver();
    //     return commonsMultipartResolver;
    // }
    //
    //
    // @Bean
    // public MultipartResolver multipartResolver() {
    //     return new CommonsMultipartResolver();
    // }

    // 注册Advisor组件AuthorizationAttributeSourceAdvisor，用到AspectJ，使用注解方式将URL和权限绑定
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }


}
