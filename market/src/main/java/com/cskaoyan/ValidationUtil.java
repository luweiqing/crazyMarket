package com.cskaoyan;

import com.cskaoyan.bean.common.BaseRespVo;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.xml.bind.ValidationException;


public class ValidationUtil {
    public static BaseRespVo getBaseRespVo(BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {// 校验是否通过，如果没通过，这个方法返回值为true
            FieldError fieldError = bindingResult.getFieldError(); //拿到校验错误的成员变量以及对应的错误信息
            String field = fieldError.getField();// 成员变量名 → 请求参数名
            Object rejectedValue = fieldError.getRejectedValue();// 错误的值
            String defaultMessage = fieldError.getDefaultMessage();// 错误的message的信息
            StringBuffer sb = new StringBuffer();
            sb.append("请求参数").append(field).append("传入了错误的值").append(rejectedValue)
                    .append(":").append(defaultMessage);
            return BaseRespVo.invalidParameter(sb.toString());
        }
        return null;
    }

    public static void valid(BindingResult bindingResult) throws ValidationException {
        if (bindingResult.hasFieldErrors()) {// 校验是否通过，如果没通过，这个方法返回值为true
            FieldError fieldError = bindingResult.getFieldError(); //拿到校验错误的成员变量以及对应的错误信息
            String field = fieldError.getField();// 成员变量名 → 请求参数名
            Object rejectedValue = fieldError.getRejectedValue();// 错误的值
            String defaultMessage = fieldError.getDefaultMessage();// 错误的message的信息
            StringBuffer sb = new StringBuffer();
            sb.append("请求参数").append(field).append("传入了错误的值").append(rejectedValue)
                    .append(":").append(defaultMessage);
            throw new ValidationException(sb.toString());
        }
    }


}
