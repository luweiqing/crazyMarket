package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.role.bo.RoleSetPermissionsBo;
import com.cskaoyan.bean.admin.role.vo.RoleCreateVo;
import com.cskaoyan.bean.admin.role.vo.RoleGetPermissionsVo;
import com.cskaoyan.bean.admin.role.vo.RoleSystemPermissionAVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketRole;
import com.cskaoyan.mapper.MarketPermissionOptionsMapper;
import com.cskaoyan.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author lwq
 * @since 2022/11/20 19:05
 */
@RestController
@RequestMapping("admin/role")
public class RoleController {
    @Autowired
    RoleService roleService;


    @RequestMapping("options")
    public BaseRespVo options() {
        CommonData commonData = roleService.options();
        return BaseRespVo.ok(commonData);
    }

    /**
     * 查询角色
     * page=1&limit=20&name=aa&sort=add_time&order=desc
     *
     * @param basePageInfo
     * @param name
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 11:52
     */
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo, String name) {
        CommonData commonData = roleService.list(basePageInfo, name);
        if (commonData != null) {
            return BaseRespVo.ok(commonData);
        }
        return BaseRespVo.invalidData("查询角色失败");
    }

    /**
     * 添加角色
     * {"name":"仓库管理员","desc":"123"}
     *
     * @param marketRole
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 14:07
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketRole marketRole) {
        RoleCreateVo role = roleService.create(marketRole);
        if (role != null) {
            return BaseRespVo.ok(role);
        }
        return BaseRespVo.invalidData("添加角色失败");
    }

    /**
     * 修改角色
     *
     * @param marketRole
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 14:31
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketRole marketRole) {
        int result = roleService.update(marketRole);
        if (result == 1) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("角色修改失败",642);
    }

    /**
     * 删除角色
     *
     * @param marketRole
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 15:11
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketRole marketRole) {
        int result = roleService.delete(marketRole);
        if (result == 2) {
            return BaseRespVo.invalidData("当前角色存在管理员，不能删除",642);
        }
        if (result == 1) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("删除角色失败");
    }

    /**
     * 查询角色权限
     * roleId=19
     *
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 17:53
     */
    @GetMapping("permissions")
    public BaseRespVo getPermissions(Integer roleId) {
        RoleGetPermissionsVo roleGetPermissionsVo = roleService.getPermissions(roleId);
        if (roleGetPermissionsVo != null) {
            return BaseRespVo.ok(roleGetPermissionsVo);
        }
        return BaseRespVo.invalidData("查询角色权限失败");
    }

    /**
     * 修改角色权限
     *
     * @param roleSetPermissionsBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 21:00
     */
    @PostMapping("permissions")
    public BaseRespVo setPermissions(@RequestBody RoleSetPermissionsBo roleSetPermissionsBo) {
        int result = roleService.setPermissions(roleSetPermissionsBo);
        if (result == roleSetPermissionsBo.getPermissions().size()) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("修改角色权限失败");
    }

    /**
     * 将所有接口json字符串插入到权限选项表中
     *
     * @param roleGetPermissionsVo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 19:58
     */
    @RequestMapping("change")
    public BaseRespVo change(@RequestBody RoleGetPermissionsVo roleGetPermissionsVo) {
        roleService.change(roleGetPermissionsVo);
        return BaseRespVo.ok();
    }
}
