package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketKeyword;
import com.cskaoyan.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangzhenyu
 * @since 2022/11/21 15:59
 */
@RestController
@RequestMapping("admin/keyword")
public class KeywordController {
    @Autowired
    KeywordService keywordService;

    // 接收数据：page=1&limit=20&keyword=%E4%B9%A1%E6%9D%91&url=o&sort=add_time&order=desc

    /**
     * 显示keyword列表数据
     *
     * @param basePageInfo 分页相关数据
     * @param keyword      关键字
     * @param url          跳转url
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo getKeywordList(BasePageInfo basePageInfo, String keyword, String url) {
        CommonData keywordListVo = keywordService.getKeywordList(basePageInfo, keyword, url);
        return BaseRespVo.ok(keywordListVo);
    }

    // {"keyword":"测试123KK","url":"abcdef","isHot":true,"isDefault":true}
    // 返回数据：{"errno":0,"data":{"id":21,"keyword":"测试123KK","url":"abcdef","isHot":true,"isDefault":true,"addTime":"2022-11-21 16:20:32","updateTime":"2022-11-21 16:20:32"},"errmsg":"成功"}
    /**
     * 创建关键字
     * @param marketKeyword 接收的创建关键字的信息
     * @return json字符串
     */
    @RequestMapping("create")
    public BaseRespVo createKeyword(@RequestBody MarketKeyword marketKeyword) {
        MarketKeyword keywordVo = keywordService.creatKeyword(marketKeyword);
        return BaseRespVo.ok(keywordVo);
    }

    // 接收数据：{"id":21,"keyword":"测试123KK","url":"123456","isHot":true,"isDefault":true,"addTime":"2022-11-21 16:20:32","updateTime":"2022-11-21 16:20:32"}
    // 返回数据：{"errno":0,"data":{"id":21,"keyword":"测试123KK","url":"123456","isHot":true,"isDefault":true,"addTime":"2022-11-21 16:20:32","updateTime":"2022-11-21 16:36:43"},"errmsg":"成功"}
    @RequestMapping("update")
    public BaseRespVo updateKeyword(@RequestBody MarketKeyword marketKeyword) {
        MarketKeyword marketKeywordVo = keywordService.updateKeyword(marketKeyword);
        return BaseRespVo.ok(marketKeywordVo);
    }

    // 接收的数据：{"id":21,"keyword":"测试123KK","url":"123456","isHot":true,"isDefault":true,"addTime":"2022-11-21 16:20:32","updateTime":"2022-11-21 16:20:32"}

    /**
     * 逻辑删除关键字
     * @param marketKeyword 接收的关于关键字的参数
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo deleteKeyword(@RequestBody MarketKeyword marketKeyword) {
        keywordService.deleteKeyword(marketKeyword);
        return BaseRespVo.ok();
    }

}
