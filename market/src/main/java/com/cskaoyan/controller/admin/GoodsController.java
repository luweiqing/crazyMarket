package com.cskaoyan.controller.admin;


import com.cskaoyan.bean.admin.goods.GoodsCateAndBrandVO;
import com.cskaoyan.bean.admin.goods.GoodsCreateBO;
import com.cskaoyan.bean.admin.goods.GoodsDetailVO;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/20 16:27
 */
@RestController
@RequestMapping("admin/goods")
public class GoodsController {

        @Autowired
        GoodsService goodsService;

    /**
     *  商品列表
     * @param pageInfo page,limit,sort,order
     * @param goodsSn 商品编号
     * @param name 商品名称
     * @param goodsId   商品ID
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo goodsList( BasePageInfo pageInfo, String goodsSn, String name, Integer goodsId){
        CommonData<MarketGoods> data = goodsService.list(pageInfo,goodsSn,name,goodsId);
        return BaseRespVo.ok(data);
    }

    /**
     * 根据商品id，展示商品详情
     * @param id
     * @return
     */
    @RequestMapping("detail")
    public BaseRespVo goodsCreate(Integer id){
        GoodsDetailVO goodsDetailVO = goodsService.detail(id);
        return BaseRespVo.ok(goodsDetailVO);
    }

    /*
     展示所有商品类目（父类目及子类目）
     */
    @RequestMapping("catAndBrand")
    public BaseRespVo goodsCatAndBrand(){
        GoodsCateAndBrandVO goodsCategoryVO = goodsService.catAndBrand();
        return BaseRespVo.ok(goodsCategoryVO);
    }

    /**
     * 新增商品
     * @param createBO 包含4张表，attributes商品参数表（数组），goods商品基本信息（单张表）
     *   products商品货品表（数组），商品规格表（数组）
     * @return
     */
    @RequestMapping("create")
    public BaseRespVo goodsCreate(@RequestBody GoodsCreateBO createBO){
        goodsService.create(createBO);
        return BaseRespVo.ok("成功",0);
    }

    /**
     *  修改商品信息，参数同上（新增商品）
     * @param update
     * @return
     */
    @RequestMapping("update")
    public BaseRespVo goodsUpdate(@RequestBody GoodsCreateBO update){
        goodsService.update(update);
        return BaseRespVo.ok(null);
    }

    /**
     * 删除商品
     * @param goods
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo goodsDelete(@RequestBody MarketGoods goods){
        goodsService.delete(goods);
        return BaseRespVo.ok(null);
    }
}
