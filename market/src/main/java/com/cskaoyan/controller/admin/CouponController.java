package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCoupon;
import com.cskaoyan.bean.po.MarketCouponUser;
import com.cskaoyan.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Date;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 16:15
 */

@RestController
@RequestMapping("admin/coupon")
public class CouponController {
    @Autowired
    CouponService couponService;

    /*
     * @Author makabaka
     * @Description //TODO
     * @Date 16:22 2022/11/20
     * @Param  page=1&limit=20& name=22&type=0&status=0  &sort=add_time&order=desc
     * @return
     **/
    @RequestMapping("list")
    public BaseRespVo<CommonData> list(BasePageInfo info, MarketCoupon marketCouponInfo) {
        CommonData<MarketCoupon> data = couponService.list(info, marketCouponInfo);
        return BaseRespVo.ok(data, "成功", 0);
    }

    @RequestMapping("create")
    public BaseRespVo<MarketCoupon> Coupon(@RequestBody MarketCoupon marketCouponinfo) {
        boolean checkResult = checkParameter(marketCouponinfo);
        if (checkResult == false) {
            return BaseRespVo.ok(null, "请检查金额、日期等的输入合理性", 401);
        }
        //校验后进入service
        MarketCoupon marketCoupon = couponService.CouponCreate(marketCouponinfo);
        return BaseRespVo.ok(marketCoupon, "成功", 0);
    }

    @RequestMapping("update")
    public BaseRespVo<MarketCoupon> CouponUpdate(@RequestBody MarketCoupon marketCouponinfo) {
        //数据校验：name、优惠券名称前端已经判断不可为空，
        boolean checkResult = checkParameter(marketCouponinfo);
        if (checkResult == false) {
            return BaseRespVo.ok(null, "请检查金额、日期等的输入合理性", 401);
        }
        MarketCoupon marketCoupon = couponService.CouponUpdatePost(marketCouponinfo);
        return BaseRespVo.ok(marketCoupon, "成功", 0);

    }

    @RequestMapping("delete")
    public BaseRespVo deleteCoupon(@RequestBody MarketCoupon marketCoupon) {
        boolean result = couponService.CouponDelete(marketCoupon);
        if (result) {
            return BaseRespVo.ok(null, "成功", 0);
            //删除成功
        }//删除失败
        return BaseRespVo.ok(null, "删除出现了一些问题", 401);

    }
    /*
     * @Author makabaka
     * @Description //TODO
     * @Date 20:00 2022/11/20
     * @Param page=1&limit=20    &couponId=3&userId=33&status=0  &sort=add_time&order=desc
     * @return
     **/

    @RequestMapping("listuser")
    public BaseRespVo ListUser(BasePageInfo info, MarketCouponUser marketCouponUser) {
        CommonData<MarketCouponUser> couponListUsers = couponService.CouponListUser(info, marketCouponUser);
        return BaseRespVo.ok(couponListUsers, "成功", 0);
    }
    @RequestMapping("read")
    public  BaseRespVo read(Integer id){
       MarketCoupon marketCoupon=couponService.CouponRead(id);
       return  BaseRespVo.ok(marketCoupon,"成功",0);

    }

    private boolean checkParameter(MarketCoupon marketCouponInfo) {
        //优惠券数量大于定于0
        Integer total = marketCouponInfo.getTotal();
        if (total != null && total < 0) {
            return false;
        }
        //每人限领 不为负
        Short limit = marketCouponInfo.getLimit();
        if (limit != null && limit < 0) {
            return false;
        }
        //有效期 不为负
        Short days = marketCouponInfo.getDays();
        if (days != null && days <= 0) {
            return false;
        }

        //指定绝对性时间 过期没事主要判断是否结束时间小于开始时间
        Date startTime = marketCouponInfo.getStartTime();
        Date endTime = marketCouponInfo.getEndTime();
        //当类型是指定时间时判断是否有日期
        Short timeType = marketCouponInfo.getTimeType();
        if (timeType == 1) {
            if (startTime == null || endTime == null) {
                return false;
            }
        }
        if (startTime != null && endTime != null) {
            //endTime>startTime
            if (endTime.compareTo(startTime) < 0) {
                return false;
            }
        }
        //最低消费  满减金额应该小于最低金额；且大于0
        BigDecimal min = marketCouponInfo.getMin();
        //满减金额
        BigDecimal discount = marketCouponInfo.getDiscount();
        if (min != null && discount != null) {
            if (min.compareTo(BigDecimal.ZERO) < 0 || discount.compareTo(BigDecimal.ZERO) < 0) {
                return false;
            }
            if (min.compareTo(discount) < 0) {
                return false;
            }
        }
        return true;
    }

}
