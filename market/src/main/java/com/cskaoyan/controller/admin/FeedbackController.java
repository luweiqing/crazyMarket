package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  16:11
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/feedback")
public class FeedbackController {
    @Autowired
    FeedbackService feedbackService;

    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo, String username, Integer id) {
        CommonData commonData = feedbackService.list(basePageInfo, username, id);
        return BaseRespVo.ok(commonData);
    }
}