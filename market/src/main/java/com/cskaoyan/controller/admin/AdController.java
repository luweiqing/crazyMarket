package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAd;
import com.cskaoyan.service.AdService;
import com.cskaoyan.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * TODO  推广管理
 *
 * @author makabaka
 * @date 2022-11-19 16:09
 */
@RestController
@RequestMapping("admin/ad")
public class AdController {

    @Autowired
    AdService adService;


    //点击广告管理进行显示,
    // page=1&limit=20&name=%E7%94%B5%E5%95%86&content=%E4%BF%83%E9%94%80&sort=add_time&order=desc
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo Info, String name, String content) {
        CommonData<MarketAd> data = adService.list(Info, name, content);

        return BaseRespVo.ok(data);
    }
    /*
     * @Author makabaka
     * @Description //TODO  编辑广告
     * @Date 12:54 2022/11/20
     * @Param
     * @return
     **/

    @RequestMapping("update")
    public BaseRespVo AdUpdate(@RequestBody MarketAd marketAd) {
        MarketAd marketAd1 = adService.AdUpdate(marketAd);

        return BaseRespVo.ok(marketAd1, "成功", 0);

    }

    @RequestMapping("delete")
    public BaseRespVo AdDelete(@RequestBody MarketAd marketAd) {
        boolean result = adService.deleteAd(marketAd);
        if (result) {
            return BaseRespVo.ok(null, "成功", 0);

        }
        return BaseRespVo.ok(null, "删除出现问题", 0);
    }

    @RequestMapping("create")
    public BaseRespVo<MarketAd> Create(@RequestBody MarketAd marketAdinfo) {
        MarketAd marketAd = adService.AdCreate(marketAdinfo);
        return  BaseRespVo.ok(marketAd,"成功",0);

    }


}
