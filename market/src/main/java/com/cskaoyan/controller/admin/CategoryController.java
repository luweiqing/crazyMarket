package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.category.bo.CategoryCreateBo;
import com.cskaoyan.bean.admin.category.bo.CategoryUpdateBo;
import com.cskaoyan.bean.admin.category.vo.CategoryL1vo;
import com.cskaoyan.bean.admin.topic.vo.CategoryWithChildrenResponseVO;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCategory;
import com.cskaoyan.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 13:46
 */
@RestController
@RequestMapping("admin/category")
public class CategoryController {
    @Autowired
    CategoryService categoryService;

    @RequestMapping("list")
    public BaseRespVo list() {
        CategoryWithChildrenResponseVO categoryWithChildrenResponseVO = categoryService.selectList ();
        return BaseRespVo.ok (categoryWithChildrenResponseVO, "成功", 0);

    }

    /**
     * http://182.92.235.201:8083/admin/category/l1
     * gzh
     */
    @RequestMapping("l1")
    public BaseRespVo l1() {
        CommonData commonData = categoryService.queryl1 ();
        return BaseRespVo.ok (commonData);
    }

    /**
     * http://182.92.235.201:8083/admin/category/create
     * post 请求
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketCategory marketCategory) {
        int code = categoryService.insertCategory (marketCategory);
        if (code == 200) {
            return BaseRespVo.ok (marketCategory);
        } else {
            return BaseRespVo.invalidParameter ("创建失败");
        }

    }

    /**
     * 删除类目
     * http://182.92.235.201:8083/admin/category/delete
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketCategory marketCategory) {
        int code = categoryService.deleteCategory (marketCategory);
        if (code == 200) {
            return BaseRespVo.ok ();
        } else {
            return BaseRespVo.invalidParameter ("删除失败");
        }
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketCategory marketCategory) {
        int code = categoryService.updateCategory (marketCategory);
        if (code == 200) {
            return BaseRespVo.ok ("成功", 0);
        }
        return BaseRespVo.ok ("父类目不能为空",401);
    }
}



