package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketIssue;
import com.cskaoyan.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author wangzhenyu
 * @since 2022/11/21 14:46
 */
@RestController
@RequestMapping("admin/issue")
public class IssueController {
    @Autowired
    IssueService issueService;

    //page=1&limit=20&question=%E5%A6%82%E4%BD%95&sort=add_time&order=desc
    /**
     *
     * @param basePageInfo 接收分页相关的参数
     * @param question 搜索时传入的问题，模糊搜素
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo issueList(BasePageInfo basePageInfo, String question) {
        CommonData issueList = issueService.getIssueList(basePageInfo,question);
        return BaseRespVo.ok(issueList);
    }

    // {"question":"测试问题","answer":"123456"}

    /**
     * 实现新建问题的业务
     * @param map 接收json字符串 question 和 answer
     * @return
     */
    @RequestMapping("create")
    public BaseRespVo createIssue(@RequestBody Map map) {
        String question = (String) map.get("question");
        String answer = (String) map.get("answer");
        MarketIssue marketIssue = issueService.creatIssue(question,answer);
        return BaseRespVo.ok(marketIssue);
    }

    // {"id":11,"question":"测试问题KK","answer":"123456789","addTime":"2022-11-21 15:14:56","updateTime":"2022-11-21 15:14:56"}
    /**
     *
     * @param marketIssue 接收json字符串，要修改的数据
     * @return
     */
    @RequestMapping("update")
    public BaseRespVo updateIssue(@RequestBody MarketIssue marketIssue) {
        MarketIssue marketIssueVo = issueService.updateIssue(marketIssue);
        return BaseRespVo.ok(marketIssueVo);
    }

    // {"id":11,"question":"测试问题KK","answer":"123456789","addTime":"2022-11-21 15:14:56","updateTime":"2022-11-21 15:14:56"}

    /**
     * 逻辑删除问题
     * @param issue 接收json字符串，要删除的数据（逻辑删除）
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo deleteIssue(@RequestBody MarketIssue issue) {
        issueService.deleteIssue(issue);
        return BaseRespVo.ok(null);
    }
}
