package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.service.UserListService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-19  21:48
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/user")
public class UserListController {
    @Autowired
    UserListService userListService;

    // @RequiresPermissions("aaaa") // 将请求URL和需要的权限绑定，shiro框架用
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo, String username, String mobile) {
        CommonData data = userListService.list(basePageInfo, username, mobile);
        return BaseRespVo.ok(data);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketUser marketUser) {
        int status = userListService.update(marketUser);
        if (status != 1) {
            return BaseRespVo.invalidParameter("请求错误");
        }
        return BaseRespVo.ok(1);
    }

    @RequestMapping("detail")
    public BaseRespVo detail(Integer id){
        if (id == null){
            return BaseRespVo.invalidParameter("参数异常，请重试");
        }
        MarketUser marketUser = userListService.detail(id);
        return BaseRespVo.ok(marketUser);
    }
}