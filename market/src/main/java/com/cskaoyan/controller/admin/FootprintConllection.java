package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.FootprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  15:13
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/footprint")
public class FootprintConllection {
    @Autowired
    FootprintService footprintService;

    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo,Integer goodsId,Integer userId){
        CommonData commonData = footprintService.list(basePageInfo,goodsId,userId);
        return BaseRespVo.ok(commonData);
    }
}