package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketComment;
import com.cskaoyan.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/21 16:13
 */
@RestController
@RequestMapping("admin/comment")
public class CommentController {
    @Autowired
    CommentService commentService;

    /**
     *  展示所有商品的评论
     * @param info     分页信息
     * @param userId    用户id
     * @param valueId   商品id
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo commentList(BasePageInfo info,String userId,String valueId){
        CommonData<MarketComment> comments = commentService.list(info, userId, valueId);
        return BaseRespVo.ok(comments);
    }

    /**
     *  删除评论
     * @param comment 评论类
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo commentDelete(@RequestBody MarketComment comment){
        commentService.delete(comment);
        return BaseRespVo.ok();
    }
}
