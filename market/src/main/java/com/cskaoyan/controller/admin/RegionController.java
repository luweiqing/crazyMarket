package com.cskaoyan.controller.admin;


import com.cskaoyan.bean.admin.region.vo.RegionListVoa;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * http://182.92.235.201:8083/admin/region/list
 * @author gzh
 * @version 1.0
 * @date 2022/11/21 21:24:53
 */

/**
 * get 请求
 *  RegionListVo->ChildrenBeanVo1->ChildrenBeanVo2
 */
@RestController
@RequestMapping("admin/region")
public class RegionController {

    @Autowired
    RegionService regionService;

    @RequestMapping("list")
    public BaseRespVo list(){
        CommonData<RegionListVoa> commonData =regionService.queryList();
        return BaseRespVo.ok (commonData);
    }
}
