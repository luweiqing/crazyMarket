package com.cskaoyan.controller.admin;


import com.cskaoyan.bean.admin.storage.vo.StorageCreateVo;
import com.cskaoyan.bean.admin.storage.vo.StorageUpdateVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketStorage;
import com.cskaoyan.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;

// import com.cskaoyan.bean.admin.storage.vo.StorageCreateVo;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;

/**
 * 上传图片
 * http://182.92.235.201:8083/admin/storage/create
 * 请求参数 file
 * 返回参数 {
 * "id":83,
 * "key":"tjo7wm8enbo7n041apg7.png",
 * "name":"1541445967645114dd75f6b0edc4762d.png",
 * "type":"image/png",
 * "size":461777,
 * "url":"http://182.92.235.201:8083/wx/storage/fetch/tjo7wm8enbo7n041apg7.png",
 * "addTime":"2022-11-20 14:37:21",
 * "updateTime":"2022-11-20 14:37:21"}
 *
 * @author gzh
 * @author lwq
 * @version 1.0
 * @date 2022/11/20 14:45:27
 * =======
 * import java.io.IOException;
 * <p>
 * <p>
 * <p>
 * /**
 * @since 2022/11/20 20:14
 */
@RestController
@RequestMapping("admin/storage")
public class StorageController {
    @Autowired
    StorageService storageService;

    /**
     * 创建图片
     *
     * @param file
     * @return
     */
    // @PostMapping("create")
    // public BaseRespVo create(MultipartFile file  ) throws IOException {
    //     String originalFilename = file.getOriginalFilename ();
    //     String uuid = UUID.randomUUID ().toString ();
    //     String uuuid = uuid+originalFilename;
    //     File file1 = new File ("F:\\app2", uuuid);
    //     file.transferTo (file1);
    //     MarketStorage storage =storageService.createStorage (file,uuuid);
    //     return BaseRespVo.ok (storage);
    //     // return BaseRespVo.ok (null);
    @RequestMapping("create")
    public BaseRespVo create(MultipartFile file) throws IOException {
        StorageCreateVo storageCreateVo = storageService.create(file);
        if (storageCreateVo != null) {
            return BaseRespVo.ok(storageCreateVo);
        }
        return BaseRespVo.invalidData("图片上传失败");

    }

    /**
     * 查询对象
     *
     * @param basePageInfo
     * @param key
     * @param name
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 16:18
     */
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo, String key, String name) {
        CommonData commonData = storageService.list(basePageInfo, key, name);
        if (commonData != null) {
            return BaseRespVo.ok(commonData);
        }
        return BaseRespVo.invalidData("查询对象失败");
    }

    /**
     * 修改对象
     *
     * @param marketStorage
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 16:18
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketStorage marketStorage) {
        StorageUpdateVo storageUpdateVo = storageService.update(marketStorage);
        if (storageUpdateVo != null) {
            return BaseRespVo.ok(storageUpdateVo);
        }
        return BaseRespVo.invalidData("修改对象失败");
    }

    /**
     * 逻辑删除对象
     *
     * @param marketStorage
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 16:31
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketStorage marketStorage) {
        int result = storageService.delete(marketStorage);
        if (result == 1) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("删除对象失败");
    }
}
