package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  14:58
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/collect")
public class CollectController {
    @Autowired
    CollectService collectService;

    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo,Integer valueId,Integer userId){
        CommonData commonData = collectService.list(basePageInfo,valueId,userId);
        return BaseRespVo.ok(commonData);
    }
}