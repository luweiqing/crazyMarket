package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.HistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  15:55
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/history")
public class HistoryController {
    @Autowired
    HistoryService historyService;

    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo,Integer userId,String keyword){
        CommonData commonData = historyService.list(basePageInfo,userId,keyword);
        return BaseRespVo.ok(commonData);
    }
}