package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.topic.bo.TopicBatchDeleteBO;
import com.cskaoyan.bean.admin.topic.vo.TopicReadVO;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketTopic;
import com.cskaoyan.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 20:50
 */

@RestController
@RequestMapping("admin/topic")
public class TopicController {

    @Autowired
    TopicService topicService;

    /*
     * @Author makabaka
     * @Description //TODO []
     * @Date 21:02 2022/11/20
     * @Param page=1&limit=20&title=tf&subtitle=yanse&sort=add_time&order=desc
     * @return
     **/
    @RequestMapping("list")
    public BaseRespVo TopicList(BasePageInfo info, MarketTopic marketTopic) {
        CommonData<MarketTopic> marketTopicCommonData = topicService.TopicList(info, marketTopic);
        return BaseRespVo.ok(marketTopicCommonData, "成功", 0);
    }

    @RequestMapping("create")
    public BaseRespVo TopicCreate(@RequestBody MarketTopic marketTopic) {
        MarketTopic marketTopic1 = topicService.TopicCreate(marketTopic);
        return BaseRespVo.ok(marketTopic1, "成功", 0);

    }

    @RequestMapping("update")
    public BaseRespVo updateTopic(@RequestBody MarketTopic marketTopic) {
        MarketTopic marketTopicInfo = topicService.updateTopic(marketTopic);
        if (marketTopicInfo != null) {
            return BaseRespVo.ok(marketTopic, "成功", 0);
        } else {
            return BaseRespVo.ok(null, "更新失败", 401);
        }

    }

    @RequestMapping("read")
    public BaseRespVo read(Integer id) {
        TopicReadVO topicRead =topicService.TopicRead(id);
        return  BaseRespVo.ok(topicRead,"成功",0);

    }
    @RequestMapping("delete")
    public  BaseRespVo Delete(@RequestBody MarketTopic marketTopic){
        boolean result = topicService.TopicDelete(marketTopic);
        if (result) {
            return BaseRespVo.ok(null, "成功", 0);
        } else {
            return BaseRespVo.ok(null, "删除失败", 401);
        }
    }
    @RequestMapping("batch-delete")
    public  BaseRespVo BatchDelete(@RequestBody TopicBatchDeleteBO ids){
        boolean result=topicService.topicBatchDelete(ids);
        if (result) {
            return BaseRespVo.ok(null, "成功", 0);
        } else {
            return BaseRespVo.ok(null, "删除失败", 401);
        }



    }


}
