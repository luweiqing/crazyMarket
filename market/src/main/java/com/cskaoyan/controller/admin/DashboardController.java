package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.dashboard.DashboardVo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-19  20:28
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/dashboard")
public class DashboardController {
    @Autowired
    DashboardService dashboardService;

    /**
     * @description:接收请求admin/dashboard
     * @author: Pinocao
     * @date: 2022/11/19 21:13
     * @param: []
     * @return: com.cskaoyan.bean.common.BaseRespVo
     **/
    @RequestMapping("")
    public BaseRespVo dashboard(){
        DashboardVo dashboardVo = dashboardService.dashboard();
        return BaseRespVo.ok(dashboardVo);
    }
}