package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.brand.bo.BrandCreateBo;
import com.cskaoyan.bean.admin.brand.vo.BrandListVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketBrand;
import com.cskaoyan.bean.po.MarketBrandExample;
import com.cskaoyan.service.AdminService;
import com.cskaoyan.service.BrandService;

import com.cskaoyan.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * http://182.92.235.201:8083/admin/brand/list?page=1&limit=20&sort=add_time&order=desc
 * @author gzh
 * @version 1.0
 * @date 2022/11/20 10:03:56
 */
@RestController
@RequestMapping("admin/brand")
public class BrandController {

    @Autowired
    BrandService brandService;

    /**
     * 查询
     * @param info
     * @param id
     * @param name
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo info,Integer id,String name){
        MarketBrandExample example = new MarketBrandExample ();
        MarketBrandExample.Criteria criteria = example.createCriteria ();
        criteria.andDeletedEqualTo (false);
        CommonData<MarketBrand> commonData = brandService.queryBrand(info,id,name);
        return BaseRespVo.ok (commonData);
    }
    /**
     *
     *添加品牌制造商
     * http://182.92.235.201:8083/admin/brand/create
     */
    @PostMapping("create")
    public BaseRespVo create(@RequestBody MarketBrand marketBrand){

        int code = brandService.createBrand(marketBrand);
        if (code==200){
            return BaseRespVo.ok ("成功",0);
        }else if (code ==404){
            return BaseRespVo.invalidParameter ("参数无效");
        }else {
            return  BaseRespVo.invalidData ("删除失败",400);
        }
    }
    @PostMapping("delete")
    public BaseRespVo delete(@RequestBody MarketBrand marketBrand){
        if(marketBrand.getId ()==null){
            return BaseRespVo.ok ("删除失败，请找联系管理员!");
        }
        int code =brandService.deleteBrand(marketBrand);
        if (code==200) {
            return BaseRespVo.ok (code);
        }else {
            return BaseRespVo.invalidParameter ("删除失败");
        }
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketBrand marketBrand){
        int code = brandService.updateBrand(marketBrand);
        if (code==200){
            return BaseRespVo.ok (marketBrand);
        }else {
            return BaseRespVo.invalidData ("修改失败",404);
        }

    }




}
