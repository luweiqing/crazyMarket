package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.stat.StatVo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wangzhenyu
 * @since 2022/11/21 17:17
 */
@RestController
@RequestMapping("admin/stat")
public class StatController {
    @Autowired
    StatService statService;

    /**
     * 实现统计用户数据功能
     * @return
     */
    @RequestMapping("user")
    public BaseRespVo getStatUser() {
        StatVo statUser = statService.getStatUser();
        return BaseRespVo.ok(statUser);
    }
    /**
     * 实现统计订单数据功能
     * @return
     */
    @RequestMapping("order")
    public BaseRespVo getStatOrder() {
        StatVo statVo = statService.getStatOrder();
        return BaseRespVo.ok(statVo);
    }
    /**
     * 实现统计商品数据功能
     * @return
     */
    @RequestMapping("goods")
    public BaseRespVo getStatGoods() {
        StatVo statVo = statService.getStatGoods();
        return BaseRespVo.ok(statVo);
    }
}
