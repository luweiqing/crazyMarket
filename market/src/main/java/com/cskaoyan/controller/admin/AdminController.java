package com.cskaoyan.controller.admin;

import com.cskaoyan.annotation.LogAddAnnotation;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAd;
import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/19 16:05
 */

@RestController
@RequestMapping("admin/admin")
public class AdminController {
    @Autowired
    AdminService adminService;

    /**
     * 查询管理员
     * page=1&limit=20&username=admin&sort=add_time&order=desc
     *
     * @param basePageInfo
     * @param username
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/19 17:11
     */
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo, String username) {
        CommonData commonData = adminService.list(basePageInfo, username);
        return BaseRespVo.ok(commonData);
    }

    /**
     * 添加管理员
     * {"username":"aaa111","password":"123456","avatar":"http://localhost:8083/wx/storage/fetch/c831e0a7-91ac-4566-8d84-96538e73ae1e.png","roleIds":[1]}
     *
     * @param marketAdmin
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/20 21:35
     */
    @LogAddAnnotation
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody MarketAdmin marketAdmin) {
        MarketAdmin admin = adminService.create(marketAdmin);
        if (admin != null) {
            return BaseRespVo.ok(admin);
        }
        return BaseRespVo.invalidData("管理员已经存在", 602);
    }

    /**
     * 修改管理员
     * {"id":6,"username":"aaa222","avatar":"http://localhost:8083/wx/storage/fetch/7891a854-580f-49ce-96c8-b1973c743259.png","roleIds":[1,3],"password":"111111"}
     *
     * @param marketAdmin
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/20 21:49
     */
    @LogAddAnnotation
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MarketAdmin marketAdmin) {
        MarketAdmin admin = adminService.update(marketAdmin);
        if (admin != null) {
            return BaseRespVo.ok(admin);
        }
        return BaseRespVo.invalidData("修改管理员失败");
    }

    /**
     * 删除管理员
     * {"id":6,"username":"aaa222","avatar":"http://localhost:8083/wx/storage/fetch/7891a854-580f-49ce-96c8-b1973c743259.png","roleIds":[1,3],"password":"111111"}
     *
     * @param marketAdmin
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/20 22:06
     */
    @LogAddAnnotation
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketAdmin marketAdmin) {
        int result = adminService.delete(marketAdmin);
        if (result == 1) {
            return BaseRespVo.ok(null);
        }
        return BaseRespVo.invalidData("删除失败");
    }
}
