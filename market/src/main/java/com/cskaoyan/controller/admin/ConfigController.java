package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.mall.ConfigMallVO;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * TODO    用到keyset进行迭代 取出键值对，速度没有enteryset速度快
 *  TODO     传过来时没有条件，只是key-value 可以用map接收
 *
 * @author makabaka
 * @date 2022-11-21 15:02
 */
@RestController
@RequestMapping("admin/config")
public class ConfigController {

    @Autowired
    ConfigService configService;

    @GetMapping("mall")
    public BaseRespVo mallGet() {
        ConfigMallVO configMallVO = configService.selectMall();
        return BaseRespVo.ok(configMallVO, "成功", 0);
    }

    @PostMapping("mall")
    public BaseRespVo mallpost(@RequestBody Map<String, String> map) {
        configService.selectConfigMall(map);
        return BaseRespVo.ok("成功", 0);

    }
    @GetMapping("express")
    public BaseRespVo express() {
        ConfigMallVO configMallVO = configService.selectExpress();
        return BaseRespVo.ok(configMallVO, "成功", 0);
    }

    @PostMapping("express")
    public BaseRespVo expresspost(@RequestBody Map<String, String> map) {
        configService.selectConfigExpress(map);
        return BaseRespVo.ok("成功", 0);

    }
    @GetMapping("order")
    public BaseRespVo orderGet() {
        ConfigMallVO configMallVO = configService.selectOrder();
        return BaseRespVo.ok(configMallVO, "成功", 0);
    }

    @PostMapping("order")
    public BaseRespVo orderspost(@RequestBody Map<String, String> map) {
        configService.selectConfigOrder(map);
        return BaseRespVo.ok("成功", 0);

    }

    @GetMapping("wx")
    public BaseRespVo wxGet() {
        ConfigMallVO configMallVO = configService.selectWx();
        return BaseRespVo.ok(configMallVO, "成功", 0);
    }
    @PostMapping("wx")
    public BaseRespVo wxspost(@RequestBody Map<String, String> map) {
        configService.selectConfigwx(map);
        return BaseRespVo.ok("成功", 0);

    }

}
