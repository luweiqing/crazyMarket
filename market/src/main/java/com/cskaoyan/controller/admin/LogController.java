package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lwq
 * @since 2022/11/21 09:09
 */
@RestController
@RequestMapping("admin/log")
public class LogController {
    @Autowired
    LogService logService;

    /**
     * 查询日志
     * page=1&limit=20&name=ma&sort=add_time&order=desc
     *
     * @param basePageInfo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/21 9:14
     */
    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo, String name) {
        CommonData commonData = logService.list(basePageInfo, name);
        if (commonData != null) {
            return BaseRespVo.ok(commonData);
        }
        return BaseRespVo.invalidData("日志查询失败");
    }
}
