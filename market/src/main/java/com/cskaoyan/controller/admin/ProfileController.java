package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.admin.profile.ProfilePasswordBo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  10:41
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/profile")
public class ProfileController {
    @Autowired
    ProfileService profileService;

    @RequestMapping("nnotice")
    public BaseRespVo nnotice(){
        return BaseRespVo.ok("成功",0);
    }

    // 后台右上角密码修改，1表示修改成功，0表示原密码输入不正确
    @RequestMapping("password")
    public BaseRespVo password(@RequestBody ProfilePasswordBo profilePasswordBo){
        int i = profileService.password(profilePasswordBo);
        if (i == 0){
            return BaseRespVo.invalidParameter("原密码输入错误！");
        }
        return BaseRespVo.ok();
    }
}