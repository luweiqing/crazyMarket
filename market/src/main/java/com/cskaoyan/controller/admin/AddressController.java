package com.cskaoyan.controller.admin;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  14:03
 * @Version: 1.0
 */
@RestController
@RequestMapping("admin/address")
public class AddressController {
    @Autowired
    AddressService addressService;

    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo basePageInfo,Integer userId,String name){
        CommonData commonData = addressService.list(basePageInfo,userId,name);
        return BaseRespVo.ok(commonData);
    }
}