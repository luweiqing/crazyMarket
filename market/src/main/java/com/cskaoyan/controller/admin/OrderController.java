package com.cskaoyan.controller.admin;
import com.cskaoyan.bean.admin.order.*;
import com.cskaoyan.annotation.LogAddAnnotation;
import com.cskaoyan.bean.admin.order.CourierVo;
import com.cskaoyan.bean.admin.order.OrderDetailVo;
import com.cskaoyan.bean.admin.order.OrderListBo;
import com.cskaoyan.bean.admin.order.OrderListWithPageVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.service.OrderService;
import com.cskaoyan.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 处理订单相关的请求
 *
 * @author wangzhenyu
 * @since 2022/11/19 16:41
 */
@RestController
@RequestMapping("admin/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    /**
     * @param basePageInfo     用于分页

     * @return
     * @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
     */
    @RequestMapping("list")
    public BaseRespVo orderList(BasePageInfo basePageInfo, OrderListBo orderListBo) throws ParseException {
        //      * @param start            查询的订单开始时间
        //      * @param end              查询的订单结束时间
        //      * @param orderSn          订单编号
        //      * @param userId           用户ID
        //      * @param orderStatusArray 订单状态的数组
        //      * @param timeArray        订单的时间范围
        Integer userId = orderListBo.getUserId();
        String orderSn = orderListBo.getOrderSn();
        Short[] orderStatusArray = orderListBo.getOrderStatusArray();
        String startByString = orderListBo.getStart();
        String endByString = orderListBo.getEnd();
        String[] timeArrayByString = orderListBo.getTimeArray();
        Date start = null;
        Date end = null;
        Date[] timeArray = null;
        if (!StringUtil.isEmpty(startByString)) {
            // 字符串转换成date
            start = parseToDate(startByString);
        }
        if (!StringUtil.isEmpty(endByString)) {
            // 字符串转换成date
            end = parseToDate(endByString);
        }
        if (timeArrayByString != null && timeArrayByString.length > 0) {
            // 字符串转换成date
            timeArray = new Date[timeArrayByString.length];
            for (int i = 0; i < timeArray.length; i++) {
                String s = timeArrayByString[i];
                Date date = parseToDate(s);
            }
        }

        // List<OrderListVo> orderListVos = orderService.orderList(basePageInfo,start,end,orderStatusArray,orderSn,userId,timeArray);
        OrderListWithPageVo orderListVos = orderService.orderList(basePageInfo,start,end,orderStatusArray,orderSn,userId,timeArray);
        return BaseRespVo.ok(orderListVos);
        // return BaseRespVo.ok(null);
    }

    private Date parseToDate(String dateString) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = simpleDateFormat.parse(dateString);
        return date;
    }

    /**
     * @param id 订单的id
     * @return
     */
    @RequestMapping("detail")
    public BaseRespVo orderDetail(Integer id) {
        OrderDetailVo orderDetail = orderService.queryOrderDetail(id);
        return BaseRespVo.ok(orderDetail);
    }


    // {"orderId":15,"refundMoney":101}

    /**
     * 退款业务
     *
     * @param map orderId -- 订单id，refund ---退款数目
     * @return
     */
    @RequestMapping("refund")
    public BaseRespVo orderRefund(@RequestBody Map map) {
        Integer orderId = (Integer) map.get("orderId");
        BigDecimal refund = (BigDecimal) map.get("refund");
        orderService.orderRefund(orderId, refund);
        return BaseRespVo.ok(null);
    }

    /**
     * 发货业务实现
     *
     * @param map
     * @return
     */
    @LogAddAnnotation
    @RequestMapping("ship")
    public BaseRespVo orderShip(@RequestBody Map map) {
        Integer orderId = (Integer) map.get("orderId");
        String shipChannel = (String) map.get("shipChannel");
        String shipSn = (String) map.get("shipSn");
        orderService.orderShip(orderId, shipChannel, shipSn);
        return BaseRespVo.ok(null);
    }

    /**
     * 实现返回快递公司列表，直接写死的
     *
     * @return
     */
    @RequestMapping("channel")
    public BaseRespVo orderChannel() {
        List<CourierVo> courier = orderService.getOrderChannel();
        return BaseRespVo.ok(courier);
    }

    // {"orderId":2}
    // {"errno":623,"errmsg":"订单不能删除"}

    /**
     * 实现逻辑删除订单
     *
     * @param map 中存储orderId
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo orderDelete(@RequestBody Map map) {
        Integer orderId = (Integer) map.get("orderId");
        Integer code = orderService.deleteOrder(orderId);
        if (code == 200) {
            return BaseRespVo.ok(null);
        } else if (code == 623) {
            return BaseRespVo.invalidParameter("订单已经是删除状态");
        }
        return BaseRespVo.invalidParameter("订单删除失败");
    }

    // todo 商品评论待写
    @RequestMapping("reply")
    public BaseRespVo orderReply(@RequestBody OrderCommentReplyBO replyBO) {
        try {
            orderService.replyComment(replyBO);
            return BaseRespVo.ok(null);
        }catch (Exception e){
            return BaseRespVo.invalidData("回复失败，请刷新重试");
        }

    }

}
