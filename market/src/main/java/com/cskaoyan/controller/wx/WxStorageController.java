package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.admin.storage.vo.StorageCreateVo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * TODO  复用  Storage
 *
 * @author makabaka
 * @date 2022-11-22 20:09
 *
 */

@RestController
@RequestMapping("wx/storage")
public class WxStorageController {
    @Autowired
    StorageService storageService;

    @RequestMapping("upload")
    public BaseRespVo wxStorageUpload(MultipartFile file) throws IOException {
        StorageCreateVo storageCreateVo = storageService.create(file);
        return BaseRespVo.ok(storageCreateVo, "成功", 0);


    }
}
