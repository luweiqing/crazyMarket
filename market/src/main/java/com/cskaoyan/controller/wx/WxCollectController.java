package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCollect;
import com.cskaoyan.bean.wx.collect.CollectListVo;
import com.cskaoyan.service.CollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  21:53
 * @Version: 1.0
 */
@RestController
@RequestMapping("wx/collect")
public class WxCollectController {
    @Autowired
    CollectService collectService;

    @RequestMapping("list")
    public BaseRespVo list(BasePageInfo pageInfo){
        CommonData commonData = collectService.getCollectList(pageInfo);
        if (commonData == null){
            return BaseRespVo.invalidParameter("请登录后访问");
        }
        return BaseRespVo.ok(commonData);
    }

    @RequestMapping("addordelete")
    public BaseRespVo addordelete(@RequestBody MarketCollect marketCollect){
        if (marketCollect == null || marketCollect.getType() == null || marketCollect.getValueId() == null){
            return BaseRespVo.invalidParameter("参数异常，请登录后重试！");
        }
        collectService.addordelete(marketCollect);
        return BaseRespVo.ok();
    }
}