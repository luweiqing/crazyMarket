package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.wx.user.UserIndexOrderVo;
import com.cskaoyan.bean.wx.user.UserIndexVo;
import com.cskaoyan.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  16:25
 * @Version: 1.0
 */
@RestController
@RequestMapping("wx/user")
public class WxUserController {
    @Autowired
    OrderService orderService;

    @RequestMapping("index")
    public BaseRespVo index(){
        UserIndexVo userIndexVo = orderService.index();
        if (userIndexVo==null){
            return BaseRespVo.invalidParameter("未登录，访问失败！");
        }
        return BaseRespVo.ok(new UserIndexOrderVo(userIndexVo));
    }
}