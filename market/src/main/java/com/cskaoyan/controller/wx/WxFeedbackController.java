package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.po.MarketFeedback;
import com.cskaoyan.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  14:23
 * @Version: 1.0
 */
@RestController
@RequestMapping("wx/feedback")
public class WxFeedbackController {
    @Autowired
    FeedbackService feedbackService;

    @RequestMapping("submit")
    public BaseRespVo submit(@RequestBody MarketFeedback marketFeedback) {
        if (marketFeedback == null) {
            return BaseRespVo.invalidParameter("参数异常");
        }
        int i = feedbackService.submit(marketFeedback);
        if (i == 0) {
            return BaseRespVo.invalidParameter("操作失败，请重试！");
        }
        return BaseRespVo.ok();
    }
}