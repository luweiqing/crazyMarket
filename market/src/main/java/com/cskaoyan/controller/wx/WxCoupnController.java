package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.wx.coupon.WxCouponExchangeBO;
import com.cskaoyan.bean.wx.coupon.WxCouponReceiveBO;
import com.cskaoyan.service.CouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * TODO  优惠券逻辑有点蒙圈 ，梅西也输了
 *
 * @author makabaka
 * @date 2022-11-22 10:16
 */

@RestController
@RequestMapping("wx/coupon")
public class WxCoupnController {
    @Autowired
    CouponService couponService;


    /*
     * @Author makabaka
     * @Description
     * @Date 10:22 2022/11/22
     * @Param      TODO status=0&page=1&limit=10
     * @return
     **/
    @GetMapping("mylist")
    public BaseRespVo wxCouponMylist(BasePageInfo basePageInfo, short status) {
       //mylist
        CommonData commonData = couponService.wxCouponMylist(basePageInfo, status);
        return BaseRespVo.ok(commonData);
    }
    /*
     * @Author makabaka
     * @Description //TODO   绷不住了，逻辑混乱  receive和 exchange，这俩可以合起来
     * @Date 17:41 2022/11/23
     * @Param
     * @return
     **/

    @RequestMapping("receive")
    public BaseRespVo wxCouponReceive(@RequestBody WxCouponReceiveBO wxCouponReceiveBO) {


        boolean result = couponService.wxCouponReceive(wxCouponReceiveBO);
        if (result) {
            return BaseRespVo.ok(null, "领取成功", 0);
        }
        //{"errno":740,"errmsg":"优惠券已经领取过"}
        return BaseRespVo.ok(null, "优惠券已领取", 740);
    }

    @GetMapping("list")
    public BaseRespVo wxCouponList(BasePageInfo basePageInfo) {
        CommonData commonData = couponService.wxCouponList(basePageInfo);
        return BaseRespVo.ok(commonData, "成功", 0);
    }


    /*
     * @Author makabaka
     * @Description //TODO
     * @Date 15:08 2022/11/22
     * @Param       {"errno":740,"errmsg":"优惠券已兑换"}  {"errno":742,"errmsg":"优惠券不正确"}
     * @return      {"errno":0,"errmsg":"成功"}
     **/

    @PostMapping("exchange")
    public BaseRespVo wxCouponExchange(@RequestBody WxCouponExchangeBO wxCouponExchangeBO) {
        int resultCode = couponService.wxCouponExChange(wxCouponExchangeBO);
        if (resultCode == 0) {
            return BaseRespVo.ok("兑换成功", 0);
        }
        if (resultCode == 742) {
            return BaseRespVo.ok("兑换码不正确", 742);
        }
        return BaseRespVo.ok("兑换失败", 401);
    }

    /*
     * @Author makabaka
     * @Description //TODO
     * @Date 9:48 2022/11/23
     * @Param []  cartId=98&grouponRulesId=0   还有cartId=0的情况    团购不写
     * @return
     **/


    @RequestMapping("selectlist")
    public BaseRespVo wxCouponSelectList(Integer cartId, Integer grouponRulesId) {
        CommonData commonData = couponService.wxCouponSelectlist(cartId, grouponRulesId);
        return BaseRespVo.ok(commonData);

    }


}
