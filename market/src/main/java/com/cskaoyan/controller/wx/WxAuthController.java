package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.auth.AuthLoginVo;
import com.cskaoyan.bean.wx.auth.AuthRegisterBo;
import com.cskaoyan.bean.wx.auth.AuthRegisterVo;
import com.cskaoyan.bean.wx.auth.UserInfoDate;
import com.cskaoyan.config.shiro.CustomToken;
import com.cskaoyan.service.AuthService;
import com.cskaoyan.util.Md5Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  14:57
 * @Version: 1.0
 */
@RestController
@RequestMapping("wx/auth")
public class WxAuthController {
    @Autowired
    AuthService authService;

    @RequestMapping("login")
    public BaseRespVo login(@RequestBody Map map){
        String username = (String) map.get("username");
        String password = (String) map.get("password");
        // 密码MD5加密
        String md5 = Md5Util.getMd5(password, "crazy");
        // 获取subject
        Subject subject = SecurityUtils.getSubject();
        CustomToken customToken = new CustomToken(username, md5, "wxLogin");
        try {
            subject.login(customToken);
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        if (!subject.isAuthenticated()){
            return BaseRespVo.invalidParameter("认证失败，账号密码不正确！");
        }
        // 获取session
        Session session = subject.getSession();
        // 获取principal，即当前登录的用户对象
        MarketUser marketUser = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        // 返回响应信息
        AuthLoginVo authLoginVo = new AuthLoginVo();
        UserInfoDate userInfoDate = new UserInfoDate();
        userInfoDate.setNickName(marketUser.getNickname());
        userInfoDate.setAvatarUrl(marketUser.getAvatar());
        authLoginVo.setUserInfo(userInfoDate);
        authLoginVo.setToken((String) session.getId());
        return BaseRespVo.ok(authLoginVo);
    }

    @RequestMapping("logout")
    public BaseRespVo logout(){
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return BaseRespVo.ok(null);
    }

    // 临时登录使用
    // @RequestMapping("login")
    // public BaseRespVo login(@RequestBody Map map){
    //     String username = (String) map.get("username");
    //     String password = (String) map.get("password");
    //     // 返回响应信息
    //     AuthLoginVo authLoginVo = new AuthLoginVo();
    //     UserInfoDate userInfoDate = new UserInfoDate();
    //     userInfoDate.setNickName("username");
    //     userInfoDate.setAvatarUrl("");
    //     authLoginVo.setUserInfo(userInfoDate);
    //     authLoginVo.setToken("03f420d9-5f58-4686-971d-733d9645870e");
    //     return BaseRespVo.ok(authLoginVo);
    // }

    @RequestMapping("regCaptcha")
    public BaseRespVo regCaptcha(@RequestBody Map map){
        String mobileStr = (String) map.get("mobile");
        if (mobileStr == null || mobileStr.length() != 11){
            return BaseRespVo.invalidParameter("手机号码参数异常，请重试");
        }
        int i = authService.regCaptcha(mobileStr);
        if (i ==1){
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidParameter("参数异常");
    }

    @RequestMapping("register")
    public BaseRespVo register(@RequestBody AuthRegisterBo authRegisterBo){
        AuthRegisterVo authRegisterVo = authService.register(authRegisterBo);
        if (authRegisterVo == null){
            return BaseRespVo.invalidParameter("用户注册失败，请重试！");
        }
        return BaseRespVo.ok(authRegisterVo);
    }

    @RequestMapping("reset")
    public BaseRespVo reset(@RequestBody AuthRegisterBo authRegisterBo){
        int i = authService.reset(authRegisterBo);
        if (i == 0){
            return BaseRespVo.invalidParameter("密码或验证码输入错误，请重试");
        }
        return BaseRespVo.ok();
    }

}