package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.cart.bo.*;
import com.cskaoyan.bean.wx.cart.vo.CartCheckoutVo;
import com.cskaoyan.bean.wx.cart.vo.CartIndexVo;
import com.cskaoyan.service.CartService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lwq
 * @since 2022/11/22 15:32
 */
@RestController
@RequestMapping("wx/cart")
public class WxCartController {
    @Autowired
    CartService cartService;

    /**
     * 统计购物车所有商品数量
     *
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/22 17:04
     */
    @RequestMapping("goodscount")
    public BaseRespVo goodsCount() {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        int goodsCount = cartService.goodsCount(userId);
        return BaseRespVo.ok(goodsCount);
    }

    /**
     * 购物车页面
     *
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/22 16:00
     */
    @RequestMapping("index")
    public BaseRespVo index() {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        CartIndexVo cartIndexVo = cartService.index(1);
        if (cartIndexVo != null) {
            return BaseRespVo.ok(cartIndexVo);
        }
        return BaseRespVo.invalidData("购物车加载失败");
    }

    /**
     * 添加商品至购物车
     *
     * @param cartAddBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/22 19:30
     */
    @RequestMapping("add")
    public BaseRespVo add(@RequestBody CartAddBo cartAddBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        int goodsCount = cartService.add(cartAddBo, userId);
        return BaseRespVo.ok(goodsCount);
    }

    /**
     * 修改购物车
     *
     * @param cartUpdateBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/22 19:32
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody CartUpdateBo cartUpdateBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        int result = cartService.update(cartUpdateBo, userId);
        if (result == 1) {
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("修改购物车失败");
    }

    /**
     * 选择商品
     *
     * @param cartCheckedBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/22 19:49
     */
    @RequestMapping("checked")
    public BaseRespVo checked(@RequestBody CartCheckedBo cartCheckedBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        CartIndexVo cartIndexVo = cartService.checked(cartCheckedBo, userId);
        if (cartCheckedBo != null) {
            return BaseRespVo.ok(cartIndexVo);
        }
        return BaseRespVo.invalidData("选择商品失败");

    }

    /**
     * 添加购物车并付款
     *
     * @param cartAddBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/22 20:09
     */
    @RequestMapping("fastadd")
    public BaseRespVo fastAdd(@RequestBody CartAddBo cartAddBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        int cartId = cartService.fastAdd(cartAddBo, userId);
        return BaseRespVo.ok(cartId);
    }

    /**
     * 下单
     *
     * @param cartCheckoutBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/23 9:52
     */
    @RequestMapping("checkout")
    public BaseRespVo checkout(CartCheckoutBo cartCheckoutBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        CartCheckoutVo cartCheckoutVo = cartService.checkout(cartCheckoutBo, userId);
        if (cartCheckoutVo != null) {
            return BaseRespVo.ok(cartCheckoutVo);
        }
        return BaseRespVo.invalidData("下单失败");
    }

    /**
     * 删除购物车中商品
     *
     * @param cartDeleteBo
     * @return com.cskaoyan.bean.common.BaseRespVo
     * @author lwq
     * @since 2022/11/23 15:21
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody CartDeleteBo cartDeleteBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        int userId = principal.getId();
        CartIndexVo cartIndexVo = cartService.delete(cartDeleteBo, userId);
        if (cartIndexVo != null) {
            return BaseRespVo.ok(cartIndexVo);
        }
        return BaseRespVo.invalidData("删除失败");
    }
}
