package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.service.FootprintService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author wangzhenyu
 * @since 2022/11/24 11:29
 */
@RestController
@RequestMapping("wx/footprint")
public class WxFootprintController {
    @Autowired
    FootprintService footprintService;

    @RequestMapping("list")
    public BaseRespVo getFootprintList(BasePageInfo basePageInfo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        CommonData footprintList = footprintService.getFootprintList(basePageInfo,userId);
        return BaseRespVo.ok(footprintList);
    }

    // {"id":927}
    @RequestMapping("delete")
    public BaseRespVo deleteFootprint(@RequestBody Map map) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        Integer id = (Integer) map.get("id");
        footprintService.deleteFootprint(id);
        return BaseRespVo.ok();
    }
}
