package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAddress;
import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.service.AddressService;
import com.cskaoyan.util.StringUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author gzh
 * @version 1.0
 * @date 2022/11/22 11:17:33
 */
@RestController
@RequestMapping("wx/address")
public class WxAddressController {
    @Autowired
    AddressService addressService;

    /**
     * 地址的页面显示
     *
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo list() {
        CommonData commonData = addressService.querylist ();
        return BaseRespVo.ok (commonData);
    }

    /**
     * 地址的创建  手机号非空且必须为11位
     *
     * @return
     */

    @RequestMapping("save")
    public BaseRespVo save(@RequestBody MarketAddress marketAddress) {
        // ValidationUtil.validErr (bindingResult);
        // if (StringUtil.isEmpty (marketAddress.getTel ()) || marketAddress.getTel ().length () != 11) {
        //     return BaseRespVo.invalidParameter ("请输入11位手机号");
        // }
        // if (isNumLegalUtil (marketAddress.getTel ()) == false) {
        //     return 401;
        // }
        //详细地址也不能为空
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer id = principal.getId ();
        int code = addressService.insertAddress (marketAddress,id);
        if (code == 405) {
           return BaseRespVo.invalidParameter ("请输入11位手机号");

        }
        return BaseRespVo.ok (code);

    }

    /**
     * 修改地址详情  点击地址详情发出一个get请求 点击保存是create操作
     * http://182.92.235.201:8083/wx/address/detail?id=4
     */
    @RequestMapping("detail")
    public BaseRespVo detail(Integer id) {
        MarketAddress marketAddress = addressService.detailId (id);
        return BaseRespVo.ok (marketAddress);
    }

    /**
     * http://182.92.235.201:8083/wx/address/delete  post 请求
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody MarketAddress marketAddress) {

        Integer id = marketAddress.getId ();
        int code = addressService.deleteAddress (id);
        if (code == 200) {
            return BaseRespVo.ok ("成功", 0);
        } else if (code == 401) {
            return BaseRespVo.ok ("已经删除");
        }
        return BaseRespVo.invalidParameter ("删除失败");

    }
}
