package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketOrder;
import com.cskaoyan.bean.po.MarketOrderGoods;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.order.bo.OrderCommentBo;
import com.cskaoyan.bean.wx.order.bo.SubmitOrderBo;
import com.cskaoyan.bean.wx.order.vo.SubmitOrderVo;
import com.cskaoyan.bean.wx.order.vo.WxOrderDetailVo;
import com.cskaoyan.service.CouponService;
import com.cskaoyan.service.OrderService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author wangzhenyu
 * @since 2022/11/22 09:59
 */
@RestController
@RequestMapping("wx/order")
public class WxOrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    CouponService couponService;

    // showType=0&page=1&limit=10

    /**
     * 显示小程序页面显示商品订单
     *
     * @param basePageInfo 分页相关数据
     * @param showType     显示类型
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo getOrderList(BasePageInfo basePageInfo, Integer showType) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        CommonData orderList = orderService.getOrderList(basePageInfo, showType, userId);
        return BaseRespVo.ok(orderList);
    }

    // orderId=26

    /**
     * 实现显示订单的详情
     *
     * @param orderId 订单id
     * @return
     */
    @RequestMapping("detail")
    public BaseRespVo getOrderDetail(Integer orderId) {
        WxOrderDetailVo orderDetailVo = orderService.getWxOrderDetail(orderId);
        return BaseRespVo.ok(orderDetailVo);
    }

    // {"cartId":49,"addressId":10,"couponId":2,"userCouponId":33,"message":"测试用例","grouponRulesId":0,"grouponLinkId":0}

    /**
     * 提交订单
     *
     * @param submitOrderBo 接收购物车id，地址id，优惠劵id，todo ，订单留言，团购规则ID，关联market_groupon_rules表ID字段，如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
     * @return
     */
    @RequestMapping("submit")
    public BaseRespVo submitOrder(@RequestBody SubmitOrderBo submitOrderBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        MarketOrder marketOrder = orderService.submitOrder(submitOrderBo, userId);
        if (marketOrder == null) {
            return BaseRespVo.invalidParameter("请勿重复操作");
        }



        //优惠券用完修改状态
        if(submitOrderBo.getCouponId()!=-1) {
            couponService.wxCoupobUserBy(submitOrderBo.getUserCouponId());
        }


        return BaseRespVo.ok(new SubmitOrderVo(marketOrder.getId(), submitOrderBo.getGrouponLinkId()));
    }

    // {"orderId":134}

    /**
     * 实现付款下单功能，目前状态为未付款,执行后为付款状态（但是在submit 接口中已经实现了）
     *
     * @param map
     * @return
     */
    @RequestMapping("prepay")
    public BaseRespVo prepayOrder(@RequestBody Map map) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        // 付款功能待实现
        return BaseRespVo.ok();
    }

    // {orderId: 134}
    /**
     * 实现退款功能
     *
     * @param map 存储订单id
     * @return
     */
    @RequestMapping("refund")
    public BaseRespVo refundOrder(@RequestBody Map map) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        Integer orderId = (Integer) map.get("orderId");
        orderService.wxRefundOrder(userId, orderId);
        return BaseRespVo.ok();
    }

    /**
     * 逻辑删除订单
     *
     * @param map 存储订单id
     * @return
     */
    @RequestMapping("delete")
    public BaseRespVo deleteOrder(@RequestBody Map map) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        Integer orderId = (Integer) map.get("orderId");
        orderService.deleteOrder(orderId);
        return BaseRespVo.ok();
    }


    // {"orderId":151}

    /**
     * 实现取消订单的功能
     *
     * @param map
     * @return
     */
    @RequestMapping("cancel")
    public BaseRespVo cancelOrder(@RequestBody Map map) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        Integer orderId = (Integer) map.get("orderId");
        orderService.wxCancelOrder(orderId, userId);
        return BaseRespVo.ok();
    }

    // {"orderId":153}
    /**
     * 实现用户收货功能
     *
     * @param map
     * @return
     */
    @RequestMapping("confirm")
    public BaseRespVo confirmOrder(@RequestBody Map map) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        Integer orderId = (Integer) map.get("orderId");
        orderService.wxConfirmOrder(orderId, userId);
        return BaseRespVo.ok();
    }

    // orderId=153&goodsId=1181039
    /**
     * 实现待评价商品的回显功能
     *
     * @param orderId 订单id
     * @param goodsId 商品ID
     * @return
     */
    @RequestMapping("goods")
    public BaseRespVo getOrderGoods(Integer orderId, Integer goodsId) {

        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        MarketOrderGoods marketOrderGoods = orderService.getOrderGoods(orderId, goodsId);
        return BaseRespVo.ok(marketOrderGoods);
    }

    /**
     * 实现商品评论功能
     * @param orderCommentBo ：{"orderGoodsId":98,"content":"测试","star":4,"hasPicture":true,"picUrls":["http://182.92.235.201:8083/wx/storage/fetch/w8gt4guz1whsc7kf9bnz.jpg"]}
     * @return
     */
    @RequestMapping("comment")
    public BaseRespVo orderComment(@RequestBody OrderCommentBo orderCommentBo) {
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        orderService.wxOrderComment(orderCommentBo);
        return BaseRespVo.ok();
    }


}
