package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.goods.WxGoodsCategoryVO;
import com.cskaoyan.bean.wx.goods.WxGoodsDetailVO;
import com.cskaoyan.bean.wx.goods.WxGoodsListVO;
import com.cskaoyan.service.FootprintService;
import com.cskaoyan.service.GoodsService;
import com.cskaoyan.util.StringUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 10:03
 */
@RestController
@RequestMapping("wx/goods")
public class WxGoodsController {

    @Autowired
    GoodsService goodsService;
    @Autowired
    FootprintService footprintService;

    /**
     *
     *  显示商品类目
     * @param id
     * @return
     */
    @RequestMapping("category")
    public BaseRespVo wxGoodsCategory(Integer id) {
        WxGoodsCategoryVO categoryVO = goodsService.getCategorysInfo(id);
        return BaseRespVo.ok(categoryVO);
    }


    /**
     * 显示单个子类目里面的商品
     * @param goods    类目的一部分信息，包装在商品对象里
     * @param info       分页信息
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo wxGoodsList(MarketGoods goods, BasePageInfo info,String keyword) {
        if (!StringUtil.isEmpty(keyword)){
            goods.setKeywords(keyword);
        }
        WxGoodsListVO goodsListVO = goodsService.selectGoodsByCondition(goods, info);
        return BaseRespVo.ok(goodsListVO);
    }

    /**
     *
     * @return 所有商品的数量
     */
    @RequestMapping("count")
    public BaseRespVo wxGoodsCount(){
        int count = goodsService.count();
        return BaseRespVo.ok(count);
    }

    /**
     *  显示商品详情
     * @param id 商品id
     * @return
     */
    @RequestMapping("detail")
    public BaseRespVo wxGoodsDetail(Integer id){
        WxGoodsDetailVO detailVO = goodsService.selectGoodsDetail(id);
        Subject subject = SecurityUtils.getSubject();
        MarketUser principal = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = principal.getId();
        // Integer userId = 1;
        footprintService.addFootprint(id,userId);
        return BaseRespVo.ok(detailVO);
    }

    /**
     *  商品详情界面的相关商品，即同一类目下的商品
     * @param id
     * @return
     */
    @RequestMapping("related")
    public BaseRespVo wxGoodsRelated(Integer id){
        CommonData<MarketGoods> relatedGoods = goodsService.selectRelatedGoods(id);
        return BaseRespVo.ok(relatedGoods);
    }

}
