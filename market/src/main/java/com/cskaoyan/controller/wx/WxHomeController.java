package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.wx.goods.WxHomeVO;
import com.cskaoyan.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 11:13
 */
@RestController
@RequestMapping("wx/home")
public class WxHomeController {

    @Autowired
    GoodsService goodsService;


    /**
     * 小程序商城首页
     * @return
     */
    @RequestMapping("index")
    public BaseRespVo wxHomeIndex(){
        WxHomeVO homeVO = goodsService.showWxHome();
        return  BaseRespVo.ok(homeVO);
    }
}
