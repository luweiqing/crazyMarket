package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketComment;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.comment.WxComentListVO;
import com.cskaoyan.bean.wx.comment.WxCommentBO;
import com.cskaoyan.bean.wx.comment.WxCommentCountVO;
import com.cskaoyan.service.CommentService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 20:47
 */
@RestController
@RequestMapping("wx/comment")
public class WxCommentController {
    @Autowired
    CommentService commentService;

    /**
     *   显示商品的评论
     * @param commentBO
     * @return
     */
    @RequestMapping("list")
    public BaseRespVo wxCommentList(WxCommentBO commentBO){
        CommonData<WxComentListVO> commentCommonData = commentService.wxList(commentBO);
        return BaseRespVo.ok(commentCommonData);
    }



    /**
     *  统计评论的数量和其中带图评论的数量
     * @param commentBO
     * @return
     */
    @RequestMapping("count")
    public BaseRespVo wxCommentCount(WxCommentBO commentBO){
        WxCommentCountVO countVO = commentService.count(commentBO);
        return BaseRespVo.ok(countVO);
    }

    @RequestMapping("post")
    public BaseRespVo wxCommentPost(@RequestBody MarketComment comment){
        // 要求先登陆，才能评论
        try {
            Subject subject = SecurityUtils.getSubject();
            MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
                commentService.insert(comment,user);
                return BaseRespVo.ok();
        }catch (Exception e){
            return BaseRespVo.invalidData("请先登陆再发表评论");
        }

    }
}
