package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.po.MarketCategory;
import com.cskaoyan.bean.po.MarketCategoryExample;
import com.cskaoyan.bean.wx.cataloge.CatalogData;
import com.cskaoyan.bean.wx.cataloge.CatalogVo;
import com.cskaoyan.mapper.MarketCategoryMapper;
import com.cskaoyan.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author gzh
 * @version 1.0
 * @date 2022/11/22 21:49:07
 */
@RestController
@RequestMapping("wx/catalog")
public class WxCatalogController {
    @Autowired
    CategoryService categoryService;

    @RequestMapping("index")
    public BaseRespVo index() {
        CatalogVo catalogVo = categoryService.selectCategoryList ();
        return BaseRespVo.ok (catalogVo);
    }
    /**
     * http://182.92.235.201:8083/wx/catalog/current?id=1005001
     * get
     */
    @RequestMapping("current")
    public BaseRespVo current(Integer id){
        CatalogData catalogData = categoryService.selectCurrent (id);
        return BaseRespVo.ok (catalogData);
    }

}
