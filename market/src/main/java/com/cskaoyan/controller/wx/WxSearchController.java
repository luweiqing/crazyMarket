package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.wx.search.SearchIndexVo;
import com.cskaoyan.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  15:14
 * @Version: 1.0
 */
@RestController
@RequestMapping("wx/search")
public class WxSearchController {
    @Autowired
    SearchService searchService;

    @RequestMapping("index")
    public BaseRespVo index() {
        SearchIndexVo searchIndexVo = searchService.index();
        return BaseRespVo.ok(searchIndexVo);
    }

    @RequestMapping("helper")
    public BaseRespVo helper(String keyword) {
        List<String> list = searchService.helper(keyword);
        return BaseRespVo.ok(list);
    }

    @RequestMapping("clearhistory")
    public BaseRespVo clearHistory() {
        int i = searchService.clearHistory();
        if (i==0){
            return BaseRespVo.invalidParameter("操作失败，请重试！");
        }
        return BaseRespVo.ok();
    }
}