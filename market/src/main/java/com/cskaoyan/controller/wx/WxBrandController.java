package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketBrand;
import com.cskaoyan.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author
 * @version 1.0
 * @date 2022/11/23 10:59:48
 */
@RequestMapping("wx/brand")
@RestController
public class WxBrandController {
    @Autowired
    BrandService brandService;

    @RequestMapping("list")
    public BaseRespVo list(Integer page, Integer limit) {
        CommonData commonData = brandService.list (page, limit);
        return BaseRespVo.ok (commonData);
    }
    /**
     * http://localhost:8083/wx/brand/detail?id=1046008
     */
    @RequestMapping("detail")
    public BaseRespVo detail(Integer id){
        MarketBrand id1 =brandService.detail(id);
        return BaseRespVo.ok (id1);
    }
}
