package com.cskaoyan.controller.wx;

import com.cskaoyan.bean.admin.topic.vo.TopicReadVO;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;

import com.cskaoyan.service.TopicService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-22 16:04
 */
@RestController
@RequestMapping("wx/topic")
public class WxTopicController {

    @Autowired
    TopicService topicService;

    @RequestMapping("list")
    public BaseRespVo wxTopicList(Integer page, Integer limit) {
        CommonData commonData = topicService.wxTopicList(page, limit);
        return BaseRespVo.ok(commonData, "成功", 0);
    }


    @RequestMapping("detail")
    public BaseRespVo wxTopicDetail(Integer id) {
        TopicReadVO topicReadVO = topicService.TopicRead(id);
        return BaseRespVo.ok(topicReadVO, "成功", 0);
    }

    @RequestMapping("related")
    public BaseRespVo wxTopicRelated(Integer id) {
        CommonData data = topicService.wxTopicRealted(id);
        return BaseRespVo.ok(data, "成功", 0);
    }


}
