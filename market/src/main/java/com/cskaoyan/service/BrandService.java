package com.cskaoyan.service;

import com.cskaoyan.bean.admin.brand.bo.BrandCreateBo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketBrand;

/**
 * @author
 * @version 1.0
 * @date 2022/11/20 13:42:38
 */
public interface BrandService {
    CommonData<MarketBrand> queryBrand(BasePageInfo info, Integer id, String name);

    int createBrand(MarketBrand brandCreateBo);

    int deleteBrand(MarketBrand marketBrand);

    int updateBrand(MarketBrand marketBrand);

    CommonData list(Integer page, Integer limit);

    MarketBrand detail(Integer id);
}
