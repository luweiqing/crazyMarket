package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAd;
import com.cskaoyan.bean.po.MarketAdExample;
import com.cskaoyan.mapper.MarketAdMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-19 20:57
 */
@Service
public class AdServiceImpl implements AdService {
    @Autowired
    MarketAdMapper marketAdMapper;

    /*
     * @Author makabaka
     * @Description //TODO [info, name, content]
     * @Date 21:44 2022/11/19
     * @Param [info, name, content]
     * @return
     **/
    @Override
    public CommonData<MarketAd> list(BasePageInfo info, String name, String content) {
        //开启分页
        PageHelper.startPage(info.getPage(), info.getLimit());
        MarketAdExample Example = new MarketAdExample();
        //排序
        Example.setOrderByClause(info.getSort() + " " + info.getOrder());

        MarketAdExample.Criteria criteria = Example.createCriteria();
        if (!StringUtil.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (!StringUtil.isEmpty(content)) {
            criteria.andContentLike("%" + content + "%");
        }

        //没有删除的
        criteria.andDeletedEqualTo(false);


        List<MarketAd> list = marketAdMapper.selectByExample(Example);
        PageInfo<MarketAd> PageInfo = new PageInfo<>(list);
        return CommonData.data(PageInfo);


    }

    /*
     * @Author makabaka
     * @Description //TODO  更新广告，先更新时间
     * @Date 13:05 2022/11/20
     * @Param
     * @return
     **/

    @Override
    public MarketAd AdUpdate(MarketAd marketAd) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        try {
            Date time = sdf.parse(format);
            marketAd.setUpdateTime(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        MarketAdExample marketAdExample = new MarketAdExample();
        MarketAdExample.Criteria criteria = marketAdExample.createCriteria();
        criteria.andIdEqualTo(marketAd.getId());
        //更新
        marketAdMapper.updateByExampleSelective(marketAd, marketAdExample);

        return marketAd;

    }

    /*
     * @Author makabaka
     * @Description //TODO 删除广告
     * @Date 13:06 2022/11/20
     * @Param
     * @return
     **/

    @Override
    public boolean deleteAd(MarketAd marketAd) {
        MarketAd marketAdDelete = new MarketAd();
        marketAdDelete.setDeleted(true);
        MarketAdExample marketAdExample = new MarketAdExample();
        MarketAdExample.Criteria criteria = marketAdExample.createCriteria();
        criteria.andIdEqualTo(marketAd.getId());
        int i = marketAdMapper.updateByExampleSelective(marketAdDelete, marketAdExample);
        if (i == 1) {
            return true;
        }
        return false;


    }

    @Override
    public MarketAd AdCreate(MarketAd marketAdinfo) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        try {
            Date time = sdf.parse(format);
            marketAdinfo.setAddTime(time);
            marketAdinfo.setUpdateTime(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //需要获得自增主键
        marketAdMapper.insertSelective(marketAdinfo);
        return marketAdinfo;

    }


}
