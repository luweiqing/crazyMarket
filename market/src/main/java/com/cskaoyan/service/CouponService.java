package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCoupon;
import com.cskaoyan.bean.po.MarketCouponUser;
import com.cskaoyan.bean.wx.coupon.WxCouponExchangeBO;
import com.cskaoyan.bean.wx.coupon.WxCouponReceiveBO;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 16:17
 */
public interface CouponService {




    CommonData<MarketCoupon> list(BasePageInfo info, MarketCoupon marketCouponInfo);

    MarketCoupon CouponCreate(MarketCoupon marketCouponinfo);

    MarketCoupon CouponUpdatePost(MarketCoupon marketCouponinfo);

    boolean CouponDelete(MarketCoupon marketCoupon);

    CommonData<MarketCouponUser> CouponListUser(BasePageInfo info, MarketCouponUser marketCouponUser);

    MarketCoupon CouponRead(Integer id);




    /*
     * @Author makabaka TODO   分割
     * @Description //TODO  下面是 WX 后台
     * @Date 10:27 2022/11/22
     * @Param
     * @return
     **/

    CommonData wxCouponMylist(BasePageInfo basePageInfo, short status);

    boolean wxCouponReceive(WxCouponReceiveBO wxCouponReceiveBO);

    CommonData wxCouponList(BasePageInfo basePageInfo);

    int wxCouponExChange(WxCouponExchangeBO wxCouponExchangeBO);

    CommonData wxCouponSelectlist(Integer cartId, Integer grouponRulesId);

    void wxCoupobUserBy(Integer couponId);

}
