package com.cskaoyan.service;

import com.cskaoyan.bean.admin.role.bo.RoleSetPermissionsBo;
import com.cskaoyan.bean.admin.role.vo.RoleCreateVo;
import com.cskaoyan.bean.admin.role.vo.RoleGetPermissionsVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketRole;

public interface RoleService {
    CommonData options();

    CommonData list(BasePageInfo basePageInfo, String name);

    RoleCreateVo create(MarketRole marketRole);

    int update(MarketRole marketRole);

    int delete(MarketRole marketRole);

    RoleGetPermissionsVo getPermissions(Integer roleId);

    void change(RoleGetPermissionsVo roleGetPermissionsVo);

    int setPermissions(RoleSetPermissionsBo roleSetPermissionsBo);
}
