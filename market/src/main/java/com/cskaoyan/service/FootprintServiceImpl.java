package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketFootprint;
import com.cskaoyan.bean.po.MarketFootprintExample;
import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.wx.footprint.FootprintVo;
import com.cskaoyan.mapper.MarketFootprintMapper;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  15:16
 * @Version: 1.0
 */
@Service
public class FootprintServiceImpl implements FootprintService {
    @Autowired
    MarketFootprintMapper marketFootprintMapper;
    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public CommonData list(BasePageInfo basePageInfo, Integer goodsId, Integer userId) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketFootprintExample marketFootprintExample = new MarketFootprintExample();
        marketFootprintExample.setOrderByClause(basePageInfo.getSort() + " " + basePageInfo.getOrder());
        MarketFootprintExample.Criteria criteria = marketFootprintExample.createCriteria();
        if (goodsId != null) {
            criteria.andGoodsIdEqualTo(goodsId);
        }
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        criteria.andDeletedEqualTo(false);
        List<MarketFootprint> list = marketFootprintMapper.selectByExample(marketFootprintExample);
        PageInfo<MarketFootprint> pageInfo = new PageInfo<>(list);
        return CommonData.data(pageInfo);
    }

    /**
     * 获取
     * @param basePageInfo
     * @param userId
     * @return
     */
    @Override
    public CommonData getFootprintList(BasePageInfo basePageInfo, Integer userId) {
        PageHelper.startPage(basePageInfo.getPage(),basePageInfo.getLimit());
        MarketFootprintExample marketFootprintExample = new MarketFootprintExample();
        marketFootprintExample.setOrderByClause("add_time desc");
        MarketFootprintExample.Criteria criteria = marketFootprintExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andDeletedEqualTo(false);
        List<MarketFootprint> marketFootprints = marketFootprintMapper.selectByExample(marketFootprintExample);
        // 遍历封装
        List<FootprintVo> footprintVos = getFootprintVos(marketFootprints);
        PageInfo<FootprintVo> marketFootprintPageInfo = null;
        if (footprintVos != null) {
            marketFootprintPageInfo = new PageInfo<>(footprintVos);
        } else {
            return null;
        }
        return CommonData.data(marketFootprintPageInfo);
    }

    @Override
    public void deleteFootprint(Integer id) {
        MarketFootprint marketFootprint = new MarketFootprint();
        marketFootprint.setDeleted(true);
        marketFootprint.setUpdateTime(new Date());
        marketFootprintMapper.updateByPrimaryKeySelective(marketFootprint);
    }

    /**
     * 把足迹加入足迹表中
     * @param id 商品id
     */
    @Override
    public void addFootprint(Integer id, Integer userId) {
        MarketFootprint marketFootprint = new MarketFootprint();
        Date date = new Date();
        marketFootprint.setGoodsId(id);
        marketFootprint.setUserId(userId);
        marketFootprint.setAddTime(date);
        marketFootprint.setUpdateTime(date);
        marketFootprint.setDeleted(false);
        marketFootprintMapper.insertSelective(marketFootprint);
    }

    private List<FootprintVo> getFootprintVos(List<MarketFootprint> marketFootprints) {
        List<FootprintVo> footprintVos = new LinkedList<>();
        for (MarketFootprint marketFootprint : marketFootprints) {
            FootprintVo footprintVo = new FootprintVo();
            // todo 日期格式可能有问题
            footprintVo.setAddTime(marketFootprint.getAddTime());
            footprintVo.setGoodsId(marketFootprint.getGoodsId());
            footprintVo.setId(marketFootprint.getId());
            // 查找商品表获取相应数据
            MarketGoods goods = marketGoodsMapper.selectByPrimaryKey(marketFootprint.getGoodsId());
            if (goods == null) {
                continue;
            }
            footprintVo.setBrief(goods.getBrief());
            footprintVo.setName(goods.getName());
            footprintVo.setRetailPrice(goods.getRetailPrice());
            footprintVo.setPicUrl(goods.getPicUrl());
            footprintVos.add(footprintVo);
        }
        return footprintVos;
    }
}