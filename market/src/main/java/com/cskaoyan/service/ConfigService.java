package com.cskaoyan.service;

import com.cskaoyan.bean.admin.mall.ConfigMallVO;

import java.util.Map;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-21 16:08
 */


public interface ConfigService {
    ConfigMallVO selectMall();


    void selectConfigMall(Map<String, String> map);


    ConfigMallVO selectExpress();

    void selectConfigExpress(Map<String, String> map);

    ConfigMallVO selectOrder();

    void selectConfigOrder(Map<String, String> map);

    ConfigMallVO selectWx();


    void selectConfigwx(Map<String, String> map);
}
