package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketComment;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.comment.WxComentListVO;
import com.cskaoyan.bean.wx.comment.WxCommentBO;
import com.cskaoyan.bean.wx.comment.WxCommentCountVO;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/21 16:16
 */
public interface CommentService {
    CommonData<MarketComment> list(BasePageInfo info, String userId, String valueId);
    CommonData<WxComentListVO> wxList(WxCommentBO commentBO);

    void delete(MarketComment comment);


    WxCommentCountVO count(WxCommentBO commentBO);

    void insert(MarketComment comment,MarketUser user);
}
