package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketLog;
import com.cskaoyan.bean.po.MarketLogExample;
import com.cskaoyan.mapper.MarketLogMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/21 09:17
 */
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    MarketLogMapper marketLogMapper;

    @Override
    public CommonData list(BasePageInfo basePageInfo, String name) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketLogExample marketLogExample = new MarketLogExample();
        MarketLogExample.Criteria criteria = marketLogExample.createCriteria();
        if (!StringUtil.isEmpty(name)) {
            criteria.andAdminLike("%" + name + "%");
        }
        marketLogExample.setOrderByClause("add_time desc");
        List<MarketLog> marketLogs = marketLogMapper.selectByExample(marketLogExample);
        if (marketLogs != null) {
            PageInfo<MarketLog> marketLogPageInfo = new PageInfo<>(marketLogs);
            return CommonData.data(marketLogPageInfo);
        }
        return null;
    }
}
