package com.cskaoyan.service;

import com.cskaoyan.bean.wx.search.SearchIndexVo;

import java.util.List;

public interface SearchService {
    SearchIndexVo index();

    List<String> helper(String keyword);

    int clearHistory();
}
