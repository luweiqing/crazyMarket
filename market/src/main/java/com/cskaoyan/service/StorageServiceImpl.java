package com.cskaoyan.service;


import com.cskaoyan.aliyun.AliyunComponent;
import com.cskaoyan.bean.admin.storage.vo.StorageUpdateVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketStorage;
import com.cskaoyan.bean.po.MarketStorageExample;
import com.cskaoyan.mapper.MarketStorageMapper;


import com.cskaoyan.util.Constant;

import com.cskaoyan.util.StringUtil;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cskaoyan.bean.admin.storage.vo.StorageCreateVo;


import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;


// /**
// <<<<<<< HEAD
//  *
//  * @author gzh
//  * @version 1.0
//  * @date 2022/11/20 14:44:37
//  */
// @Service
// public class StorageServiceImpl implements StorageService{
//     @Autowired
//     MarketStorageMapper storageMapper;
//
//     // 添加brand
//     @Override
//     public MarketStorage createStorage(MultipartFile file,String uuuid) {
//         MarketStorageExample storageExample = new MarketStorageExample ();
//         MarketStorageExample.Criteria criteria = storageExample.createCriteria ();
//         // String uuid = UUID.randomUUID ().toString ();
//
//         MarketStorage storage = new MarketStorage ();
//         storage.setAddTime (new Date ());
//         // String name = file.getOriginalFilename ();
//         // String name1 = uuid+name;
//
//         storage.setKey (uuuid);
//         storage.setName (file.getOriginalFilename ());
//         storage.setSize ((int) file.getSize ());
//         storage.setType (file.getContentType ());
//         storage.setUpdateTime (new Date ());
//         storage.setUrl (uuuid);
//         storageMapper.insertSelective (storage);
//         return storage ;

/**
 *
 * @author lwq
 * @since 2022/11/20 20:41
 */
// @Transactional
@Service
public class StorageServiceImpl implements StorageService {
    @Autowired
    MarketStorageMapper marketStorageMapper;
    @Autowired
    AliyunComponent aliyunComponent;

    /**
     * 上传对象
     *
     * @param file
     * @return com.cskaoyan.bean.admin.storage.vo.StorageCreateVo
     * @author lwq
     * @since 2022/11/21 16:00
     */
  //  @Override
//    public StorageCreateVo create(MultipartFile file) throws IOException {
//        String contentType = file.getContentType();
//        String[] split = contentType.split("/");
//        String key = UUID.randomUUID() + "." + split[1];
//        // String data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
//        MarketStorage marketStorage = new MarketStorage(null, key, file.getOriginalFilename(), contentType, (int) file.getSize(), Constant.imgUrl + key, new Date(), new Date(), null);
//        int affectRows = marketStorageMapper.insertSelective(marketStorage);
//        if (affectRows == 1) {
//            File saveFile = new File(Constant.diskUrl, key);
//            if (!saveFile.getParentFile().exists()) {
//                saveFile.getParentFile().mkdirs();
//            }
//            file.transferTo(saveFile);
//            return new StorageCreateVo(marketStorage.getId(), marketStorage.getKey(), marketStorage.getName(), marketStorage.getType(), marketStorage.getSize(), marketStorage.getUrl(), marketStorage.getAddTime().toString(), marketStorage.getUpdateTime().toString());
//        }
//        return null;
//
//    }
    /*
     * @Author makabaka
     * @Description //TODO [file]   上面是存本地，下面是存阿里云
     * @Date 15:33 2022/11/23
     * @Param [file]
     * @return
     **/
    @Override
    public StorageCreateVo create(MultipartFile file) throws IOException {
        String contentType = file.getContentType();
        String[] split = contentType.split("/");
        String key = UUID.randomUUID() + "." + split[1];
        // String data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        InputStream inputStream = file.getInputStream();
        aliyunComponent.fileUpload(key, inputStream);
        String url = "https://" + aliyunComponent.getOss().getBucket() + "." + aliyunComponent.getOss().getEndPoint() + "/" + key;
        MarketStorage marketStorage = new MarketStorage(null, key, file.getOriginalFilename(), contentType, (int) file.getSize(), url, new Date(), new Date(), null);
        int affectRows = marketStorageMapper.insertSelective(marketStorage);
        if (affectRows == 1) {
            return new StorageCreateVo(marketStorage.getId(), marketStorage.getKey(), marketStorage.getName(), marketStorage.getType(), marketStorage.getSize(), marketStorage.getUrl(), marketStorage.getAddTime().toString(), marketStorage.getUpdateTime().toString());

        }
        return  null;

    }


    /**
     * 查询对象
     *
     * @param basePageInfo
     * @param key
     * @param name
     * @return com.cskaoyan.bean.common.CommonData
     * @author lwq
     * @since 2022/11/21 16:01
     */
    @Override
    public CommonData list(BasePageInfo basePageInfo, String key, String name) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketStorageExample marketStorageExample = new MarketStorageExample();
        MarketStorageExample.Criteria criteria = marketStorageExample.createCriteria();
        if (!StringUtil.isEmpty(key)) {
            criteria.andKeyEqualTo(key);
        }
        if (!StringUtil.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);
        List<MarketStorage> marketStorages = marketStorageMapper.selectByExample(marketStorageExample);
        if (marketStorages != null) {
            PageInfo<MarketStorage> pageInfo = new PageInfo<>(marketStorages);
            return CommonData.data(pageInfo);
        }
        return null;
    }

    /**
     * 修改对象
     *
     * @param marketStorage
     * @return com.cskaoyan.bean.admin.storage.vo.StorageUpdateVo
     * @author lwq
     * @since 2022/11/21 16:18
     */
    @Override
    public StorageUpdateVo update(MarketStorage marketStorage) {
        marketStorage.setUpdateTime(new Date());
        int affectedRows = marketStorageMapper.updateByPrimaryKeySelective(marketStorage);
        if (affectedRows == 1) {
            return new StorageUpdateVo(marketStorage.getId(), marketStorage.getKey(), marketStorage.getName(), marketStorage.getType(), marketStorage.getSize(), marketStorage.getUrl(), marketStorage.getAddTime(), marketStorage.getUpdateTime());
        }
        return null;
    }

    /**
     * 逻辑删除对象
     *
     * @param marketStorage
     * @return int
     * @author lwq
     * @since 2022/11/21 16:31
     */
    @Override
    public int delete(MarketStorage marketStorage) {
        marketStorage.setDeleted(true);
        int affectedRows = marketStorageMapper.updateByPrimaryKeySelective(marketStorage);
        return affectedRows;
    }


}
