package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.common.User;

public interface UserService {
    CommonData<User> query(String username, BasePageInfo info);
}
