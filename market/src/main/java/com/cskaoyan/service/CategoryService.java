package com.cskaoyan.service;

import com.cskaoyan.bean.admin.category.bo.CategoryUpdateBo;
import com.cskaoyan.bean.admin.category.vo.CategoryL1vo;
import com.cskaoyan.bean.admin.topic.vo.CategoryWithChildrenResponseVO;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCategory;
import com.cskaoyan.bean.wx.cataloge.CatalogData;
import com.cskaoyan.bean.wx.cataloge.CatalogVo;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 13:48
 */
public interface CategoryService {
    CategoryWithChildrenResponseVO selectList();


    CommonData queryl1();

    int insertCategory(MarketCategory marketCategory);

    int deleteCategory(MarketCategory marketCategory);


    int updateCategory(MarketCategory marketCategory);

    CatalogVo selectCategoryList();

    CatalogData selectCurrent(Integer id);

    // CatalogData selectCurrent(Integer id);
}
