package com.cskaoyan.service;

import com.cskaoyan.aliyun.AliyunComponent;
import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.wx.auth.AuthRegisterBo;
import com.cskaoyan.bean.wx.auth.AuthRegisterVo;
import com.cskaoyan.bean.wx.auth.UserInfoDate;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.Md5Util;
import com.cskaoyan.util.ShiroUtil;
import org.apache.shiro.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Description: 登录登出部分业务
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  22:16
 * @Version: 1.0
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    MarketAdminMapper marketAdminMapper;
    @Autowired
    MarketRoleMapper marketRoleMapper;
    @Autowired
    MarketPermissionMapper marketPermissionMapper;
    @Autowired
    MarketPermissionOptionsMapper marketPermissionOptionsMapper;
    @Autowired
    AliyunComponent aliyunComponent;
    @Autowired
    MarketUserMapper marketUserMapper;
    private String codeNum = "";

    @Override
    public InfoData info(MarketAdmin marketAdmin) {
        if (marketAdmin == null){
            return null;
        }
        // 获取当前登录对象中roleIds
        Integer[] roleIds = marketAdmin.getRoleIds();
        // 查询role表,获取角色
        MarketRoleExample marketRoleExample = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = marketRoleExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andIdIn(Arrays.asList(roleIds));
        List<MarketRole> marketRoles = marketRoleMapper.selectByExample(marketRoleExample);
        ArrayList<String> roles = new ArrayList<>();
        for (MarketRole marketRole : marketRoles) {
            roles.add(marketRole.getName());
        }
        // 查询permission
        MarketPermissionExample marketPermissionExample = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria1 = marketPermissionExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria1.andRoleIdIn(Arrays.asList(roleIds));
        List<MarketPermission> marketPermissions = marketPermissionMapper.selectByExample(marketPermissionExample);
        ArrayList<String> permissions = new ArrayList<>();
        for (MarketPermission marketPermission : marketPermissions) {
            permissions.add(marketPermission.getPermission());
        }
        // 查询permission_option表api
        List<String> perms = marketPermissionOptionsMapper.selectApi(permissions);
        // 封装
        InfoData infoData = new InfoData();
        infoData.setName(marketAdmin.getUsername());
        infoData.setAvatar(marketAdmin.getAvatar());
        infoData.setRoles(roles);
        infoData.setPerms(perms);
        return infoData;
    }

    // 短信发送
    @Override
    public int regCaptcha(String mobileStr) {
        // 生成六位随机数
        Random randObj = new Random();
        String randomNum = Integer.toString(100000 + randObj.nextInt(900000));
        System.out.println(randomNum);
        // 同步到静态成员变量
        codeNum = randomNum;
        // 发送短信
        System.out.println(mobileStr);
        aliyunComponent.sendMsg(mobileStr, randomNum);
        return 1;
    }

    @Override
    public AuthRegisterVo register(AuthRegisterBo authRegisterBo) {
        // 校验验证码
        String code = authRegisterBo.getCode();
        if (code == null || !code.equals(codeNum)){
            return null;
        }
        // 判断用户名是否已存在
        String username = authRegisterBo.getUsername();
        if (username == null){
            return null;
        }
        MarketUserExample marketUserExample = new MarketUserExample();
        MarketUserExample.Criteria criteria = marketUserExample.createCriteria();
        criteria.andUsernameEqualTo(username);
        criteria.andDeletedEqualTo(false);
        List<MarketUser> marketUsers = marketUserMapper.selectByExample(marketUserExample);
        // 用户存在
        if (marketUsers.size() != 0){
            return null;
        }
        // 用户不存在，注册用户
        MarketUser marketUser = new MarketUser();
        marketUser.setUsername(username);
        marketUser.setNickname(username);
        String password = authRegisterBo.getPassword();
        String md5 = Md5Util.getMd5(password, "crazy");
        marketUser.setPassword(md5);
        marketUser.setMobile(authRegisterBo.getMobile());
        marketUser.setAddTime(new Date());
        marketUser.setUpdateTime(new Date());
        int i = marketUserMapper.insertSelective(marketUser);
        if (i == 0){
            return null;
        }
        UserInfoDate userInfo = new UserInfoDate();
        userInfo.setNickName(username);
        userInfo.setAvatarUrl("");
        AuthRegisterVo authRegisterVo = new AuthRegisterVo();
        authRegisterVo.setUserInfo(userInfo);
        Session session = ShiroUtil.getSession();
        authRegisterVo.setToken((String) session.getId());
        return authRegisterVo;
    }

    @Override
    public int reset(AuthRegisterBo authRegisterBo) {
        String code = authRegisterBo.getCode();
        if (code == null || !code.equals(codeNum)){
            return 0;
        }
        String mobile = authRegisterBo.getMobile();
        String password = authRegisterBo.getPassword();
        if (mobile ==null || password == null){
            return 0;
        }
        MarketUserExample marketUserExample = new MarketUserExample();
        MarketUserExample.Criteria criteria = marketUserExample.createCriteria();
        criteria.andMobileEqualTo(mobile);
        criteria.andDeletedEqualTo(false);
        List<MarketUser> marketUsers = marketUserMapper.selectByExample(marketUserExample);
        if (marketUsers.size() != 1){
            return 0;
        }
        MarketUser marketUser = new MarketUser();
        String md5 = Md5Util.getMd5(password, "crazy");
        marketUser.setPassword(md5);
        marketUser.setUpdateTime(new Date());
        int i = marketUserMapper.updateByExampleSelective(marketUser, marketUserExample);
        if (i == 0){
            return 0;
        }
        return 1;
    }
}