package com.cskaoyan.service;



import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAdmin;


public interface AdminService {
    CommonData list(BasePageInfo basePageInfo, String username);

    MarketAdmin create(MarketAdmin marketAdmin);

    MarketAdmin update(MarketAdmin marketAdmin);

    int delete(MarketAdmin marketAdmin);
}
