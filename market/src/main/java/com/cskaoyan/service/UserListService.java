package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketUser;


public interface UserListService {
    CommonData list(BasePageInfo basePageInfo, String username, String mobile);

    int update(MarketUser marketUser);

    MarketUser detail(Integer id);
}
