package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCollect;
import com.cskaoyan.bean.po.MarketCollectExample;
import com.cskaoyan.bean.po.MarketGoodsExample;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.collect.CollectListVo;
import com.cskaoyan.mapper.MarketCollectMapper;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.util.ShiroUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  15:00
 * @Version: 1.0
 */
@Service
public class CollectServiceImpl implements CollectService {
    @Autowired
    MarketCollectMapper marketCollectMapper;


    @Override
    public CommonData list(BasePageInfo basePageInfo, Integer valueId, Integer userId) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketCollectExample marketCollectExample = new MarketCollectExample();
        marketCollectExample.setOrderByClause(basePageInfo.getSort() + " " + basePageInfo.getOrder());
        MarketCollectExample.Criteria criteria = marketCollectExample.createCriteria();
        if (valueId != null) {
            criteria.andValueIdEqualTo(valueId);
        }
        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        criteria.andDeletedEqualTo(false);
        List<MarketCollect> list = marketCollectMapper.selectByExample(marketCollectExample);
        PageInfo<MarketCollect> pageInfo = new PageInfo<>(list);
        return CommonData.data(pageInfo);
    }

    // 小程序商品收藏
    @Override
    public CommonData getCollectList(BasePageInfo basePageInfo) {
        Integer userId = ShiroUtil.getUserId();
        if (userId == null){
            return null;
        }
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        List<CollectListVo> list = marketCollectMapper.selectCollectByUserId(userId);
        PageInfo<CollectListVo> pageInfo = new PageInfo<>(list);
        return CommonData.data(pageInfo);
    }

    @Override
    public void addordelete(MarketCollect marketCollect) {
        Byte type = marketCollect.getType();
        Integer valueId = marketCollect.getValueId();
        // 获取当前用户id
        Integer id = ShiroUtil.getUserId();
        if (id == null){
            return;
        }
        MarketCollectExample marketCollectExample = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = marketCollectExample.createCriteria();
        criteria.andUserIdEqualTo(id);
        criteria.andTypeEqualTo(type);
        criteria.andValueIdEqualTo(valueId);
        List<MarketCollect> list = marketCollectMapper.selectByExample(marketCollectExample);
        // 判断当前商品是否在收藏表存在，存在则改变deleted状态，不存在则添加
        if (list.size() != 0) {
            MarketCollect marketCollect1 = list.get(0);
            if (marketCollect1.getDeleted()) {
                marketCollect1.setDeleted(false);
            } else {
                marketCollect1.setDeleted(true);
            }
            marketCollect1.setUpdateTime(new Date());
            int i = marketCollectMapper.updateByPrimaryKeySelective(marketCollect1);
        } else {
            MarketCollect marketCollect1 = new MarketCollect(null, id, valueId, type, new Date(), new Date(),false);
            marketCollectMapper.insertSelective(marketCollect1);
        }
    }

    // 提取方法，获取userId
    public Integer getUserId() {
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principals = subject.getPrincipals();
        if (principals == null) {
            return 0;
        }
        MarketUser marketUser = (MarketUser) principals.getPrimaryPrincipal();
        Integer id = marketUser.getId();
        if (id == null) {
            return 0;
        }
        return id;
    }


}