package com.cskaoyan.service;

import com.cskaoyan.bean.admin.topic.bo.TopicBatchDeleteBO;
import com.cskaoyan.bean.admin.topic.vo.TopicReadVO;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketTopic;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 20:57
 */

public interface TopicService {




    CommonData<MarketTopic> TopicList(BasePageInfo info, MarketTopic marketTopic);

    MarketTopic TopicCreate(MarketTopic marketTopic);


    MarketTopic updateTopic(MarketTopic marketTopic);

    TopicReadVO TopicRead(Integer id);

    boolean TopicDelete(MarketTopic marketTopic);

    boolean topicBatchDelete(TopicBatchDeleteBO ids);


    CommonData wxTopicList(Integer page, Integer limit);

    CommonData wxTopicRealted(Integer id);

}
