package com.cskaoyan.service;

import com.cskaoyan.bean.admin.dashboard.DashboardVo;

public interface DashboardService {
    DashboardVo dashboard();

}
