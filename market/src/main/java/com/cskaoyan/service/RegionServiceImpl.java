package com.cskaoyan.service;

import com.cskaoyan.bean.admin.brand.vo.BrandListVo;

import com.cskaoyan.bean.admin.region.vo.RegionListVoa;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketRegion;
import com.cskaoyan.bean.po.MarketRegionExample;
import com.cskaoyan.mapper.MarketRegionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author
 * @version 1.0
 * @date 2022/11/21 21:29:01
 */
@Service
public class RegionServiceImpl implements RegionService{

    @Autowired
    MarketRegionMapper marketRegionMapper;


    @Override
    public CommonData<RegionListVoa> queryList() {

        List<RegionListVoa> regionListVoaList=marketRegionMapper.select();

        CommonData<RegionListVoa> regionListVoaCommonData = new CommonData<> ();
        regionListVoaCommonData.setList (regionListVoaList);
        regionListVoaCommonData.setLimit (1);
        regionListVoaCommonData.setPage (1);
        regionListVoaCommonData.setPages (1);
        regionListVoaCommonData.setTotal (1);
        return regionListVoaCommonData;

    }
}
