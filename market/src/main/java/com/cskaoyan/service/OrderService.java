package com.cskaoyan.service;

import com.cskaoyan.bean.admin.order.CourierVo;
import com.cskaoyan.bean.admin.order.OrderCommentReplyBO;
import com.cskaoyan.bean.admin.order.OrderDetailVo;
import com.cskaoyan.bean.admin.order.OrderListWithPageVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketOrder;
import com.cskaoyan.bean.po.MarketOrderGoods;
import com.cskaoyan.bean.wx.order.bo.OrderCommentBo;
import com.cskaoyan.bean.wx.order.bo.SubmitOrderBo;
import com.cskaoyan.bean.wx.order.vo.WxOrderDetailVo;
import com.cskaoyan.bean.wx.user.UserIndexVo;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface OrderService {


    OrderListWithPageVo orderList(BasePageInfo basePageInfo, Date start, Date end, Short[] orderStatusArray, String orderSn, Integer userId, Date[] timeArray);

    OrderDetailVo queryOrderDetail(Integer id);

    void orderRefund(Integer orderId, BigDecimal refund);

    void orderShip(Integer orderId, String shipChannel, String shipSn);

    List<CourierVo> getOrderChannel();


    Integer deleteOrder(Integer orderId);


    CommonData getOrderList(BasePageInfo basePageInfo, Integer showType, Integer userId);

    WxOrderDetailVo getWxOrderDetail(Integer orderId);

    MarketOrder submitOrder(SubmitOrderBo submitOrderBo, Integer userId);

    UserIndexVo index();

    void wxRefundOrder(Integer userId, Integer orderId);

    void wxCancelOrder(Integer orderId, Integer userId);

    void wxConfirmOrder(Integer orderId, Integer userId);

    MarketOrderGoods getOrderGoods(Integer orderId, Integer goodsId);

    void wxOrderComment(OrderCommentBo orderCommentBo);

    void replyComment(OrderCommentReplyBO replyBO);

}
