package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketIssue;
import com.cskaoyan.bean.po.MarketIssueExample;
import com.cskaoyan.mapper.MarketIssueMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/21 14:58
 */
@Service
public class IssueServiceImpl implements IssueService {
    @Autowired
    MarketIssueMapper marketIssueMapper;

    @Override
    public CommonData getIssueList(BasePageInfo basePageInfo, String question) {
        PageHelper.startPage(basePageInfo.getPage(),basePageInfo.getLimit());
        MarketIssueExample marketIssueExample = new MarketIssueExample();
        marketIssueExample.setOrderByClause(basePageInfo.getSort() + " " + basePageInfo.getOrder());
        MarketIssueExample.Criteria criteria = marketIssueExample.createCriteria();
        if (!StringUtil.isEmpty(question)) {
            criteria.andQuestionLike("%" + question + "%");
        }
        criteria.andDeletedEqualTo(false);
        List<MarketIssue> marketIssues = marketIssueMapper.selectByExample(marketIssueExample);
        PageInfo<MarketIssue> marketIssuePageInfo = new PageInfo<>(marketIssues);
        return CommonData.data(marketIssuePageInfo);
    }

    @Override
    @Transactional
    public MarketIssue creatIssue(String question, String answer) {
        MarketIssue marketIssue = new MarketIssue();
        Date date = new Date();
        marketIssue.setAddTime(date);
        marketIssue.setUpdateTime(date);
        marketIssue.setQuestion(question);
        marketIssue.setAnswer(answer);
        marketIssue.setDeleted(false);
        marketIssueMapper.insertSelective(marketIssue);
        return marketIssue;
    }

    @Override
    @Transactional
    public MarketIssue updateIssue(MarketIssue marketIssue) {
        marketIssueMapper.updateByPrimaryKeySelective(marketIssue);
        MarketIssue marketIssueVo = marketIssueMapper.selectByPrimaryKey(marketIssue.getId());
        return marketIssueVo;
    }

    @Override
    public void deleteIssue(MarketIssue issue) {
        issue.setDeleted(true);
        marketIssueMapper.updateByPrimaryKeySelective(issue);
    }
}
