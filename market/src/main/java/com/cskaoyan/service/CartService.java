package com.cskaoyan.service;

import com.cskaoyan.bean.wx.cart.bo.*;
import com.cskaoyan.bean.wx.cart.vo.CartCheckoutVo;
import com.cskaoyan.bean.wx.cart.vo.CartIndexVo;

public interface CartService {
    int goodsCount(int userId);

    CartIndexVo index(int userId);

    int add(CartAddBo cartAddBo, int userId);

    int update(CartUpdateBo cartUpdateBo, int userId);

    CartIndexVo checked(CartCheckedBo cartCheckedBo, int userId);

    int fastAdd(CartAddBo cartAddBo, int userId);

    CartCheckoutVo checkout(CartCheckoutBo cartCheckoutBo, int userId);

    CartIndexVo delete(CartDeleteBo cartDeleteBo, int userId);
}
