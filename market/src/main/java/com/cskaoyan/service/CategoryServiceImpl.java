package com.cskaoyan.service;

import com.cskaoyan.bean.admin.category.bo.CategoryUpdateBo;
import com.cskaoyan.bean.admin.category.bo.ChildrenBean;
import com.cskaoyan.bean.admin.category.vo.CategoryL1vo;
import com.cskaoyan.bean.admin.topic.vo.CategoryWithChildrenResponseVO;
import com.cskaoyan.bean.admin.topic.vo.CategoryWithChildrenVO;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCategory;
import com.cskaoyan.bean.po.MarketCategoryExample;
import com.cskaoyan.bean.wx.cataloge.*;
import com.cskaoyan.mapper.MarketCategoryMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 13:48
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    MarketCategoryMapper marketCategoryMapper;

    @Override
    public CategoryWithChildrenResponseVO selectList() {
        List<MarketCategory> marketCategories = getMarketCategoryListWithPidIsZeroAndDeleteIsfalse ();
        ArrayList<CategoryWithChildrenVO> categoryWithChildreninfos = new ArrayList<> ();
        for (MarketCategory marketCategory : marketCategories) {
            CategoryWithChildrenVO categoryWithChildrenVO = new CategoryWithChildrenVO (marketCategory);
            //根据pid=id查出孩子
            MarketCategoryExample marketCategoryExample = new MarketCategoryExample ();
            MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria ();
            criteria.andPidEqualTo (marketCategory.getId ());
            criteria.andDeletedEqualTo (false);
            List<MarketCategory> childrenListInfo = marketCategoryMapper.selectByExample (marketCategoryExample);
            categoryWithChildrenVO.setChildren (childrenListInfo);
            categoryWithChildreninfos.add (categoryWithChildrenVO);
        }

        CategoryWithChildrenResponseVO categoryWithChildrenResponseVO = new CategoryWithChildrenResponseVO ();
        categoryWithChildrenResponseVO.setList (categoryWithChildreninfos);
        categoryWithChildrenResponseVO.setPage (1);
        categoryWithChildrenResponseVO.setPages (1);
        long total = (long) marketCategories.size ();
        categoryWithChildrenResponseVO.setTotal (total);
        int limit = marketCategories.size ();
        categoryWithChildrenResponseVO.setLimit (limit);
        return categoryWithChildrenResponseVO;

    }

    private List<MarketCategory> getMarketCategoryListWithPidIsZeroAndDeleteIsfalse() {
        //查找一级类目  pid=0且 逻辑为被删除的
        MarketCategoryExample marketCategoryExample = new MarketCategoryExample ();
        MarketCategoryExample.Criteria criteria = marketCategoryExample.createCriteria ();
        //pid=0 总的类目
        criteria.andPidEqualTo (0);
        criteria.andDeletedEqualTo (false);
        marketCategoryExample.setOrderByClause ("name" + " " + "desc");
        List<MarketCategory> marketCategories = marketCategoryMapper.selectByExample (marketCategoryExample);

        return marketCategories;

    }

    /**
     * l1 获取类目id 和名称
     *
     * @return
     */
    @Override
    public CommonData queryl1() {
        // MarketCategoryExample example = new MarketCategoryExample ();
        // MarketCategoryExample.Criteria criteria = example.createCriteria ();
        // criteria.andLevelEqualTo ("L1");
        List<CategoryL1vo> list = marketCategoryMapper.select ();
        PageInfo<CategoryL1vo> pageInfo = new PageInfo<> (list);
        pageInfo.setPageNum (1);
        pageInfo.setPages (1);
        pageInfo.setPageSize (10);
        pageInfo.setTotal (10);
        return CommonData.data2 (pageInfo);

    }

    /**
     * 添加类目
     *
     * @param marketCategory
     * @return
     */
    @Override
    public int insertCategory(MarketCategory marketCategory) {
        // MarketCategoryExample example = new MarketCategoryExample ();
        // MarketCategoryExample.Criteria criteria = example.createCriteria ();
        // criteria.andAddTimeEqualTo (marketCategory.setAddTime (new Date ()));
        marketCategory.setAddTime (new Date ());
        marketCategory.setUpdateTime (new Date ());
        int num = marketCategoryMapper.insertSelective (marketCategory);
        if (num == 1) {
            return 200;
        }
        return 400;

    }

    /**
     * 删除类目
     *
     * @param marketCategory
     * @return
     */
    @Override
    public int deleteCategory(MarketCategory marketCategory) {


        marketCategory.setDeleted (true);
        marketCategory.setUpdateTime (new Date ());
        MarketCategoryExample example = new MarketCategoryExample ();
        MarketCategoryExample.Criteria criteria = example.createCriteria ();
        criteria.andIdEqualTo (marketCategory.getId ());
        int num = marketCategoryMapper.updateByExampleSelective (marketCategory,example);
        if (num == 1) {
            return 200;
        } else {
            return 400;
        }

    }

    @Override
    public int updateCategory(MarketCategory marketCategory) {

        if (marketCategory.getPid ()==0&&marketCategory.getLevel ()=="L2"&&marketCategory.getLevel ()!="L1"){
                return 401;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String format = simpleDateFormat.format (new Date ());
        try {
            Date date = simpleDateFormat.parse (format);
            marketCategory.setUpdateTime (date);
        } catch (ParseException e) {
            e.printStackTrace ();
        }

        MarketCategoryExample example = new MarketCategoryExample ();
        MarketCategoryExample.Criteria criteria = example.createCriteria ();
        criteria.andIdEqualTo (marketCategory.getId ());
        marketCategoryMapper.updateByExampleSelective (marketCategory, example);

        return 200;

    }


    @Override
    public CatalogVo selectCategoryList() {
        // 查询类目列表
        List<CatalogVo1> catalogVo1List = marketCategoryMapper.selectCategoryList ();
        // 当前类目
        CatalogVo1 catalogVo1 = catalogVo1List.get (0);

        CatalogVo2 catalogVo2 = marketCategoryMapper.selectCurrentCategory (catalogVo1.getId ());

        int id = catalogVo2.getId ();

        List<CatalogVo3> catalogVo3s = marketCategoryMapper.selectSubCategory (id);

        // marketCategoryMapper.selectSubCategory();
        return new CatalogVo (catalogVo1List, catalogVo2, catalogVo3s);
    }

    @Override
    public CatalogData selectCurrent(Integer id) {
        MarketCategory marketCategory = marketCategoryMapper.selectByPrimaryKey (id);
        MarketCategoryExample example = new MarketCategoryExample ();
        MarketCategoryExample.Criteria criteria = example.createCriteria ();
        criteria.andPidEqualTo (id);
        List<MarketCategory> catalogVo3s = marketCategoryMapper.selectByExample (example);
        CatalogData catalogData = new CatalogData (marketCategory, catalogVo3s);

        return catalogData;
    }
}
