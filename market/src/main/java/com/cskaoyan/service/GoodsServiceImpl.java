package com.cskaoyan.service;

import com.cskaoyan.bean.admin.goods.*;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.wx.comment.WxCommentBO;
import com.cskaoyan.bean.wx.goods.*;
import com.cskaoyan.controller.wx.WxCommentController;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.ShiroUtil;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/20 16:40
 */
@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    MarketGoodsMapper goodsMapper;

    @Autowired
    MarketCategoryMapper categoryMapper;

    @Autowired
    MarketGoodsAttributeMapper attributeMapper;

    @Autowired
    MarketGoodsSpecificationMapper specificationMapper;

    @Autowired
    MarketGoodsProductMapper productMapper;

    @Autowired
    MarketBrandMapper brandMapper;

    @Autowired
    MarketAdMapper adMapper;

    @Autowired
    MarketCouponMapper couponMapper;

    @Autowired
    MarketTopicMapper topicMapper;

    @Autowired
    MarketCommentMapper commentMapper;

    @Autowired
    MarketGrouponRulesMapper grouponRulesMapper;

    @Autowired
    MarketIssueMapper issueMapper;

    @Autowired
    MarketCollectMapper collectMapper;

    @Autowired
    CommentService commentService;

    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Autowired
    MarketSystemMapper systemMapper;

    /**
     * @param Info      分页：page/limit  排序：sort/order
     * @param goodsSn
     * @param goodsName
     * @param goodsId
     * @return
     */
    @Override
    public CommonData<MarketGoods> list(BasePageInfo Info, String goodsSn, String goodsName, Integer goodsId) {
        MarketGoodsExample goodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria = goodsExample.createCriteria();
        // 判空、拼接条件
        if (!StringUtil.isEmpty(goodsSn)) {
            criteria.andGoodsSnEqualTo(goodsSn);
        }
        if (!StringUtil.isEmpty(goodsName)) {
            criteria.andNameLike("%" + goodsName + "%");
        }
        if (goodsId != null) {
            criteria.andIdEqualTo(goodsId);
        }
        // 开启分页
        if (Info.getPage() != null && Info.getPage() > 0
                && Info.getLimit() != null && Info.getLimit() > 0){
            PageHelper.startPage(Info.getPage(), Info.getLimit());
        }
        // 排序
        if (!StringUtil.isEmpty(Info.getOrder()) && !StringUtil.isEmpty(Info.getSort())){
            goodsExample.setOrderByClause(Info.getSort() + " " + Info.getOrder());
        }
        List<MarketGoods> list = goodsMapper.selectByExample(goodsExample);
        PageInfo<MarketGoods> pageInfo = new PageInfo<>(list);

        return CommonData.data2(pageInfo);
    }


    /**
     * 根据商品id显示商品详情
     *
     * @param id
     * @return
     */
    @Override
    public GoodsDetailVO detail(Integer id) {
        MarketGoods goods = goodsMapper.selectByPrimaryKey(id);
        // 通过类目id查找类目，并把父类目编号和子类目编号放到数组里
        MarketCategory marketCategory = categoryMapper.selectByPrimaryKey(goods.getCategoryId());
        Integer[] integers = new Integer[2];
        integers[0] = marketCategory.getPid();
        integers[1] = marketCategory.getId();
        // 通过商品id找到对应的商品参数表
        MarketGoodsAttribute[] marketGoodsAttribute = attributeMapper.selectByGoodsId(id);
        // 通过商品id找到对应的商品规格表
        MarketGoodsSpecification[] specifications = specificationMapper.selectByGoodsId(id);
        // 通过商品id找到对应的商品货品表
        MarketGoodsProduct[] products = productMapper.selectByGoodsId(id);
        return new GoodsDetailVO(integers, goods, marketGoodsAttribute, specifications, products);

    }

    /**
     * 查询商品类目表和品牌，包括父类目和子类目
     *
     * @return
     */
    @Override
    public GoodsCateAndBrandVO catAndBrand() {
        // 查询蕾类目表
        // 查询所有父类目（即没有子类目的类目,pid=0）
        int pid = 0;
        GoodsCategoryVO[] fatherCategory = categoryMapper.selectIdAndName(pid);
        // 通过父类目找到子类目
        for (GoodsCategoryVO f : fatherCategory) {
            GoodsCategoryVO[] sonCategory = categoryMapper.selectIdAndName(f.getValue());
            f.setChildren(sonCategory);
        }
        // 查询品牌表
        GoodsBrandVo[] brands = brandMapper.selectIdAndName();
        return new GoodsCateAndBrandVO(fatherCategory, brands);
    }


    /**
     * 新增商品
     *
     * @param createBO
     */
    @Transactional
    @Override
    public void create(GoodsCreateBO createBO) {
        MarketGoodsAttribute[] attributes = createBO.getAttributes();
        MarketGoods goods = createBO.getGoods();
        MarketGoodsProduct[] products = createBO.getProducts();
        MarketGoodsSpecification[] specifications = createBO.getSpecifications();
        // 添加商品
        Date date = new Date();
        if (goods != null) {
            // 添加增加的时间
            goods.setAddTime(date);
            goods.setUpdateTime(date);
            // 设置规格中最低的价格为商品价格
            BigDecimal price = products[0].getPrice();
            for (MarketGoodsProduct product : products) {
                if (product.getPrice().intValue() < price.intValue()) {
                    price = product.getPrice();
                }
            }
            goods.setRetailPrice(price);
            goodsMapper.insertSelective(goods);
        }
        // 添加商品参数表
        if (attributes != null) {
            for (MarketGoodsAttribute attribute : attributes) {
                attribute.setGoodsId(goods.getId());
                attribute.setAddTime(date);
                attribute.setUpdateTime(date);
                attributeMapper.insertSelective(attribute);
            }
        }
        // 添加商品货品表
        if (products != null) {
            for (MarketGoodsProduct product : products) {
                product.setId(null);
                product.setGoodsId(goods.getId());
                product.setAddTime(date);
                product.setUpdateTime(date);
                productMapper.insertSelective(product);
            }
        }
        // 添加商品规格表
        if (specifications != null) {
            for (MarketGoodsSpecification specification : specifications) {
                specification.setGoodsId(goods.getId());
                specification.setAddTime(date);
                specification.setUpdateTime(date);
                specificationMapper.insertSelective(specification);
            }
        }
    }

    /**
     * 修改商品信息
     *
     * @param update
     */
    @Transactional
    @Override
    public void update(GoodsCreateBO update) {
        MarketGoodsAttribute[] attributes = update.getAttributes();
        MarketGoods goods = update.getGoods();
        MarketGoodsProduct[] products = update.getProducts();
        MarketGoodsSpecification[] specifications = update.getSpecifications();
        // 更新商品
        Date date = new Date();
        if (goods != null) {
            goods.setUpdateTime(date);
            BigDecimal price = products[0].getPrice();
            for (MarketGoodsProduct product : products) {
                if (product.getPrice().intValue() < price.intValue()) {
                    price = product.getPrice();
                }
            }
            goods.setRetailPrice(price);
            goodsMapper.updateByPrimaryKey(goods);
        }
        // 更新商品参数表
        if (attributes != null) {
            for (MarketGoodsAttribute attribute : attributes) {
                attribute.setUpdateTime(date);
                attributeMapper.updateByPrimaryKey(attribute);
            }
        }
        // 更新商品货品表
        if (products != null) {
            for (MarketGoodsProduct product : products) {
                product.setUpdateTime(date);
                productMapper.updateByPrimaryKey(product);
            }
        }
        // 更新商品规格表
        if (specifications != null) {
            for (MarketGoodsSpecification specification : specifications) {
                specification.setUpdateTime(date);
                specificationMapper.updateByPrimaryKey(specification);
            }
        }
    }

    /**
     * 删除商品
     *
     * @param goods
     */
    @Transactional
    @Override
    public void delete(MarketGoods goods) {
        goodsMapper.deleteByPrimaryKey(goods.getId());
    }


    /**
     * 小程序界面展示商品类目
     * id 传入时，需要做判断，如果传入的是1级类目ID，那么应该直接显示1级目录下第1个2级目录的商品
     * 如果传入的是2级目录，那么直接显示该2级目录的商品
     *
     * @param id 类目id
     * @return
     */
    @Override
    public WxGoodsCategoryVO getCategorysInfo(Integer id) {
        /*MarketCategoryExample categoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria exampleCriteria = categoryExample.createCriteria();
        exampleCriteria.andIdEqualTo(id);
        exampleCriteria.andDeletedEqualTo(false);*/
        MarketCategory category = categoryMapper.selectByPrimaryKey(id);
        Integer pid = category.getPid();
        String level = category.getLevel();
        MarketCategoryExample example = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = example.createCriteria();
        if (pid == 0 && "L1".equals(level)) {
            // 是父类目
            // 传入的id是1级类目的id，应该直接显示1级类目下第1个2级类目的商品
            MarketCategory parent = category;
            if (id > 0) {
                criteria.andPidEqualTo(id);
            }
            criteria.andLevelEqualTo("L2");
            List<MarketCategory> brother = categoryMapper.selectByExample(example);
            MarketCategory current = brother.get(0);
            return new WxGoodsCategoryVO(brother, current, parent);
        } else {
            // 是子类目
            MarketCategory current = category;
            // 查父类目
            MarketCategory parent = categoryMapper.selectByPrimaryKey(current.getPid());
            // 查兄弟类目
            criteria.andPidEqualTo(current.getPid());
            List<MarketCategory> brother = categoryMapper.selectByExample(example);
            return new WxGoodsCategoryVO(brother, current, parent);
        }
    }

    /**
     * 查询对应条件下的商品信息
     *
     * @param goods
     * @param info
     * @return
     */
    @Override
    public WxGoodsListVO selectGoodsByCondition(MarketGoods goods, BasePageInfo info) {
        // 查询条件下下的商品信息并且分页
        MarketGoodsExample goodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria = goodsExample.createCriteria();
        if (goods.getBrandId() != null && goods.getBrandId() > 0) {
            criteria.andBrandIdEqualTo(goods.getBrandId());
        }
        if (goods.getCategoryId() != null && goods.getCategoryId() > 0) {
            criteria.andCategoryIdEqualTo(goods.getCategoryId());
        }
        if (goods.getIsHot() != null) {
            criteria.andIsHotEqualTo(true);
        }
        if (goods.getIsNew() != null) {
            criteria.andIsNewEqualTo(true);
        }
        if (!StringUtil.isEmpty(goods.getKeywords())) {
            criteria.andNameLike("%" + goods.getKeywords() + "%");
        }
        if (!StringUtil.isEmpty(goods.getName())) {
            criteria.andNameLike("%" + goods.getName() + "%");
        }
        if (goods.getDeleted() != null) {
            criteria.andDeletedEqualTo(true);
        }
        // 排序
        if (!StringUtil.isEmpty(info.getOrder()) && !StringUtil.isEmpty(info.getSort())) {
            goodsExample.setOrderByClause(info.getSort() + " " + info.getOrder());
        }
        // 分页
        if (info.getPage() != null && info.getPage() > 0
            && info.getLimit() != null && info.getLimit() > 0){
            PageHelper.startPage(info.getPage(), info.getLimit());
        }
        List<MarketGoods> list = goodsMapper.selectByExample(goodsExample);
        PageInfo<MarketGoods> goodsPageInfo = new PageInfo<>(list);
        // CommonData<MarketGoods> data = CommonData.data2(goodsPageInfo);
        WxGoodsListVO<MarketGoods> data = WxGoodsListVO.data(goodsPageInfo);
        // 查询所有的子类目
        String level = "L2";
        MarketCategoryExample categoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria1 = categoryExample.createCriteria();
        criteria1.andLevelEqualTo(level);
        List<MarketCategory> categories = categoryMapper.selectByExample(categoryExample);
        data.setFilterCategoryList(categories);
        // 调用方法记录关键词搜索历史
        String keywords = goods.getKeywords();
        if (!StringUtil.isEmpty(keywords)) {
            insertSearchHistory(keywords);
        }
        return data;
    }

    // 向搜索历史表中插入数据
    private void insertSearchHistory(String keywords) {
        // 获取userId
        MarketUser marketUser = ShiroUtil.getMarketUser();
        Integer userId = marketUser.getId();
        if (userId == null) {
            // 未登录搜索，添加搜索历史到默认0用户位置
            userId = 0;
        }
        // 根据userId和keyword判断该关键词是否已存在
        MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
        MarketSearchHistoryExample.Criteria criteria = marketSearchHistoryExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andKeywordEqualTo(keywords);
        criteria.andDeletedEqualTo(false);
        List<MarketSearchHistory> marketSearchHistories = marketSearchHistoryMapper.selectByExample(marketSearchHistoryExample);
        // 当前搜索关键词已存在，更新时间
        MarketSearchHistory marketSearchHistory = new MarketSearchHistory();
        if (marketSearchHistories != null && marketSearchHistories.size() != 0) {
            marketSearchHistory.setUpdateTime(new Date());
            int i = marketSearchHistoryMapper.updateByExampleSelective(marketSearchHistory, marketSearchHistoryExample);
            return;
        }
        // 关键词搜索历史不存在，插入一条数据
        marketSearchHistory.setUserId(userId);
        marketSearchHistory.setKeyword(keywords);
        marketSearchHistory.setFrom("wx");
        marketSearchHistory.setAddTime(new Date());
        marketSearchHistory.setUpdateTime(new Date());
        marketSearchHistoryMapper.insertSelective(marketSearchHistory);
    }

    /**
     * 微信商城首页，一共分8个部分
     *
     * @return
     */
    @Override
    public WxHomeVO showWxHome() {
        MarketSystemExample systemExample = new MarketSystemExample();
        MarketSystemExample.Criteria systemExampleCriteria = systemExample.createCriteria();
        systemExampleCriteria.andKeyNameLike("%market_wx%");
        List<MarketSystem> systemList = systemMapper.selectByExample(systemExample);
        String topicValue = new String();
        String newG = new String();
        String hotG = new String();
        String brand = new String();
        String cateNum = new String();
        String goodsOfCateNum = new String();
        for (MarketSystem system : systemList) {
            if ("market_wx_index_topic".equals(system.getKeyName())){
                topicValue = system.getKeyValue();
            }
            if ("market_wx_index_hot".equals(system.getKeyName())){
                hotG = system.getKeyValue();
            }
            if ("market_wx_index_new".equals(system.getKeyName())){
                newG = system.getKeyValue();
            }
            if ("market_wx_index_brand".equals(system.getKeyName())){
                brand = system.getKeyValue();
            }
            if ("market_wx_catlog_goods".equals(system.getKeyName())){
                goodsOfCateNum = system.getKeyValue();
                // cateNum = system.getKeyValue();
            }
            if ("market_wx_catlog_list".equals(system.getKeyName())){
                cateNum = system.getKeyValue();
                // goodsOfCateNum = system.getKeyValue();
            }
        }
        // 展示首页商品横幅
        MarketAdExample adExample = new MarketAdExample();
        List<MarketAd> ads = adMapper.selectByExample(adExample);
        // 所有父类目
        MarketCategoryExample categoryExample = new MarketCategoryExample();
        MarketCategoryExample.Criteria criteria = categoryExample.createCriteria();
        criteria.andLevelEqualTo("L1");
        criteria.andPidEqualTo(0);
        criteria.andDeletedEqualTo(false);
        List<MarketCategory> categories = categoryMapper.selectByExample(categoryExample);
        // 显示最多4条优惠券
        String couponOrderByclause = "add_time desc limit 4";
        MarketCouponExample couponExample = new MarketCouponExample();
        MarketCouponExample.Criteria criteria5 = couponExample.createCriteria();
        criteria5.andTypeEqualTo((short) 0);
        couponExample.setOrderByClause(couponOrderByclause);
        List<MarketCoupon> coupons = couponMapper.selectByExample(couponExample);
        // 显示4个品牌制造商
        String brandOrderByclause = "sort_order desc limit " + brand;
        MarketBrandExample brandExample = new MarketBrandExample();
        brandExample.setOrderByClause(brandOrderByclause);
        MarketBrandExample.Criteria criteria2 = brandExample.createCriteria();
        criteria2.andDeletedEqualTo(false);
        List<MarketBrand> brands = brandMapper.selectByExample(brandExample);
        // 显示4个新品
        String newGoodsOrderByclause = "add_time desc limit " + newG;
        MarketGoodsExample goodsExample = new MarketGoodsExample();
        goodsExample.setOrderByClause(newGoodsOrderByclause);
        MarketGoodsExample.Criteria criteria3 = goodsExample.createCriteria();
        criteria3.andIsNewEqualTo(true);
        List<MarketGoods> newGoods = goodsMapper.selectByExample(goodsExample);
        // 显示4个热卖商品
        String hotGoodsOrderByclause = "add_time desc limit " + hotG;
        MarketGoodsExample goodsExample1 = new MarketGoodsExample();
        goodsExample1.setOrderByClause(hotGoodsOrderByclause);
        MarketGoodsExample.Criteria criteria4 = goodsExample1.createCriteria();
        criteria4.andIsHotEqualTo(true);
        List<MarketGoods> hotGoods = goodsMapper.selectByExample(goodsExample1);
        // 显示专题
        MarketTopicExample topicExample = new MarketTopicExample();
        String topicGoodsOrderByclause = "add_time desc limit " + topicValue;
        topicExample.setOrderByClause(topicGoodsOrderByclause);
        List<MarketTopic> topics = topicMapper.selectByExample(topicExample);
        // 最底部显示4个类目的商品，每个类目4个商品,类目就是首页的父类目中前4个
        List<WxFloorGoodsVO> floorGoodsVOS = new LinkedList<>();
        int size = categories.size();
        // int anInt = Integer.parseInt(goodsOfCateNum);
        int anInt = Integer.parseInt(cateNum) - 1;
        // 查找该类目的子类目
        for (int i = 0; i <= (size >= anInt ? anInt : size); i++) {
            MarketCategory category = categories.get(i);
            Integer categoryId = category.getId();
            List<Integer> idList = new LinkedList<>();
            idList.add(categoryId);
            MarketCategoryExample categoryExample1 = new MarketCategoryExample();
            MarketCategoryExample.Criteria criteria1 = categoryExample1.createCriteria();
            criteria1.andPidEqualTo(categoryId);
            criteria1.andDeletedEqualTo(false);
            List<MarketCategory> categoryList = categoryMapper.selectByExample(categoryExample1);
            // 把子类目的id加入list
            for (MarketCategory marketCategory : categoryList) {
                idList.add(marketCategory.getId());
            }
            // 查找父类目和子类目下的商品，只选择4个
            List<MarketGoods> goods = goodsMapper.selectFloorGoods(idList,Integer.parseInt(goodsOfCateNum));
            WxFloorGoodsVO floorGoodsVO = new WxFloorGoodsVO();
            floorGoodsVO.setId(categoryId);
            floorGoodsVO.setName(category.getName());
            floorGoodsVO.setGoodsList(goods);
            floorGoodsVOS.add(floorGoodsVO);
        }
        return new WxHomeVO(ads, brands, categories,
                coupons, floorGoodsVOS, hotGoods,
                newGoods, topics);
    }

    /**
     * 查询所有商品的数量
     *
     * @return
     */
    @Override
    public int count() {
        MarketGoodsExample goodsExample = new MarketGoodsExample();
        List<MarketGoods> goods = goodsMapper.selectByExample(goodsExample);
        return goods.size();
    }

    /**
     * 显示商品详情，包括10个部分
     *
     * @param id
     * @return
     */
    @Override
    public WxGoodsDetailVO selectGoodsDetail(Integer id) {
        // 查询商品信息
        MarketGoods goods = goodsMapper.selectByPrimaryKey(id);
        // 查询商品参数
        MarketGoodsAttributeExample attributeExample = new MarketGoodsAttributeExample();
        MarketGoodsAttributeExample.Criteria attributeExampleCriteria = attributeExample.createCriteria();
        attributeExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGoodsAttribute> attributes = attributeMapper.selectByExample(attributeExample);
        // 查询品牌
        MarketBrand brand = brandMapper.selectByPrimaryKey(goods.getBrandId());
        // 查询评论，只在商品页面显示5条
        String commentOrderByclause = "add_time desc limit 5";
        MarketCommentExample commentExample = new MarketCommentExample();
        commentExample.setOrderByClause(commentOrderByclause);
        MarketCommentExample.Criteria commentExampleCriteria = commentExample.createCriteria();
        commentExampleCriteria.andValueIdEqualTo(id);
        List<MarketComment> comments = commentMapper.selectByExample(commentExample);
        WxGoodsDetailCommentVO commentVO = new WxGoodsDetailCommentVO();
        commentVO.setData(comments);
        commentVO.setCount(commentService.count(new WxCommentBO(goods.getId())).getAllCount());
        // 查询团购
        MarketGrouponRulesExample grouponRulesExample = new MarketGrouponRulesExample();
        MarketGrouponRulesExample.Criteria rulesExampleCriteria = grouponRulesExample.createCriteria();
        rulesExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGrouponRules> grouponRules = grouponRulesMapper.selectByExample(grouponRulesExample);
        // 查询常见问题
        MarketIssueExample issueExample = new MarketIssueExample();
        List<MarketIssue> issues = issueMapper.selectByExample(issueExample);
        // 查询商品货品表
        MarketGoodsProductExample productExample = new MarketGoodsProductExample();
        MarketGoodsProductExample.Criteria productExampleCriteria = productExample.createCriteria();
        productExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGoodsProduct> products = productMapper.selectByExample(productExample);
        // 查询是否可分享,如果可分享，获得分享图片的url
        boolean share = false;
        String shareImage = "";
        if (!StringUtil.isEmpty(goods.getShareUrl())) {
            share = true;
            shareImage = goods.getShareUrl();
        }
        // 查询规格表
        MarketGoodsSpecificationExample specificationExample = new MarketGoodsSpecificationExample();
        MarketGoodsSpecificationExample.Criteria specificationExampleCriteria = specificationExample.createCriteria();
        specificationExampleCriteria.andGoodsIdEqualTo(id);
        List<MarketGoodsSpecification> specifications = specificationMapper.selectByExample(specificationExample);
        String specification = specifications.get(0).getSpecification();
        WxGoodsDetailSpecVO specVO = new WxGoodsDetailSpecVO(specification, specifications);
        List<WxGoodsDetailSpecVO> goodsDetailSpecVOS = new LinkedList<>();
        goodsDetailSpecVOS.add(specVO);
        // 查看是否收藏
        MarketCollectExample collectExample = new MarketCollectExample();
        MarketCollectExample.Criteria criteria = collectExample.createCriteria();
        int hasCollect = 0;
        // 如果当前没有登陆，那么没有收藏
        try {
            Subject subject = SecurityUtils.getSubject();
            MarketUser user = (MarketUser) subject.getPrincipals().getPrimaryPrincipal();
            if (user != null) {
                criteria.andUserIdEqualTo(user.getId());
            }
            criteria.andValueIdEqualTo(goods.getId());
            List<MarketCollect> collects = collectMapper.selectByExample(collectExample);
            if (collects.size() > 0) {
                hasCollect = 1;
            }
        } catch (Exception e) {

        }
        return new WxGoodsDetailVO(attributes, brand, commentVO,
                grouponRules, goods, issues, products, share, shareImage,
                goodsDetailSpecVOS, hasCollect);
    }

    /**
     * 显示同类目下相关商品
     *
     * @param id
     * @return
     */
    @Override
    public CommonData<MarketGoods> selectRelatedGoods(Integer id) {
        // 分页显示，固定每页显示6个同类目商品
        int limit = 6;
        int page = 1;
        MarketGoods goods = goodsMapper.selectByPrimaryKey(id);
        MarketGoodsExample example = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria = example.createCriteria();
        criteria.andCategoryIdEqualTo(goods.getCategoryId());
        // 开启分页
        PageHelper.startPage(page, limit);
        List<MarketGoods> relatedGoods = goodsMapper.selectByExample(example);
        // PageInfo<MarketGoods> pageInfo = new PageInfo<>(relatedGoods);
        // CommonData<MarketGoods> data = CommonData.data2(pageInfo);
        CommonData<MarketGoods> data = new CommonData<>(relatedGoods.size(), (relatedGoods.size() / limit + 1), limit, page, relatedGoods);
        return data;
    }


}
