package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketFeedback;
import com.cskaoyan.bean.po.MarketFeedbackExample;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.mapper.MarketFeedbackMapper;
import com.cskaoyan.util.ShiroUtil;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  16:20
 * @Version: 1.0
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    MarketFeedbackMapper marketFeedbackMapper;

    @Override
    public CommonData list(BasePageInfo basePageInfo, String username, Integer id) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketFeedbackExample marketFeedbackExample = new MarketFeedbackExample();
        marketFeedbackExample.setOrderByClause(basePageInfo.getSort() + " " + basePageInfo.getOrder());
        MarketFeedbackExample.Criteria criteria = marketFeedbackExample.createCriteria();
        if (!StringUtil.isEmpty(username)) {
            criteria.andUsernameLike("%" + username + "%");
        }
        if (id != null) {
            criteria.andIdEqualTo(id);
        }
        criteria.andDeletedEqualTo(false);
        List<MarketFeedback> list = marketFeedbackMapper.selectByExample(marketFeedbackExample);
        PageInfo<MarketFeedback> pageInfo = new PageInfo<>(list);
        return CommonData.data(pageInfo);
    }

    @Override
    public int submit(MarketFeedback marketFeedback) {
        MarketUser marketUser = ShiroUtil.getMarketUser();
        if (marketUser == null){
            return 0;
        }
        Integer userId = marketUser.getId();
        String username = marketUser.getUsername();
        if (userId == null || username == null) {
            return 0;
        }
        marketFeedback.setAddTime(new Date());
        marketFeedback.setUserId(userId);
        marketFeedback.setUsername(username);
        int i = marketFeedbackMapper.insertSelective(marketFeedback);
        if (i == 1) {
            return 1;
        }
        return 0;
    }
}