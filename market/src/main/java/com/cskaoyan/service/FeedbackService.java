package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketFeedback;

public interface FeedbackService {
    CommonData list(BasePageInfo basePageInfo, String username, Integer id);

    int submit(MarketFeedback marketFeedback);
}
