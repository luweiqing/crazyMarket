package com.cskaoyan.service;

import com.cskaoyan.bean.admin.profile.ProfilePasswordBo;

public interface ProfileService {
    int password(ProfilePasswordBo profilePasswordBo);
}
