package com.cskaoyan.service;

import com.cskaoyan.bean.admin.profile.ProfilePasswordBo;
import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.bean.po.MarketAdminExample;
import com.cskaoyan.mapper.MarketAdminMapper;
import com.cskaoyan.util.Md5Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  23:05
 * @Version: 1.0
 */
@Service
public class ProfileServiceImpl implements ProfileService {
    @Autowired
    MarketAdminMapper marketAdminMapper;

    @Override
    public int password(ProfilePasswordBo profilePasswordBo) {
        Subject subject = SecurityUtils.getSubject();
        PrincipalCollection principals = subject.getPrincipals();
        if (principals == null) {
            return 0;
        }
        MarketAdmin marketAdmin = (MarketAdmin) principals.getPrimaryPrincipal();
        // 判断输入的原密码是否正确
        String oldPassword = profilePasswordBo.getOldPassword();
        String md5 = Md5Util.getMd5(oldPassword, "crazy");
        String password = marketAdmin.getPassword();
        if (!password.equals(md5)) {
            return 0;
        }
        // 进行密码修改操作
        Integer adminId = marketAdmin.getId();
        MarketAdminExample marketAdminExample = new MarketAdminExample();
        marketAdminExample.createCriteria().andIdEqualTo(adminId);
        // 获取新密码，并进行md5加密后修改密码
        String newPassword = profilePasswordBo.getNewPassword();
        String newPasswordMd5 = Md5Util.getMd5(newPassword, "crazy");
        MarketAdmin marketAdmin1 = new MarketAdmin();
        marketAdmin1.setPassword(newPasswordMd5);
        int i = marketAdminMapper.updateByExampleSelective(marketAdmin1, marketAdminExample);
        if (i != 1) {
            return 0;
        }
        return 1;
    }
}