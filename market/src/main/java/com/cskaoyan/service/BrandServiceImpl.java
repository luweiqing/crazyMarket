package com.cskaoyan.service;

import com.cskaoyan.bean.admin.brand.bo.BrandCreateBo;
import com.cskaoyan.bean.admin.brand.vo.BrandListVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.BaseRespVo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketBrand;
import com.cskaoyan.bean.po.MarketBrandExample;
import com.cskaoyan.bean.po.MarketStorageExample;
import com.cskaoyan.mapper.MarketBrandMapper;
import com.cskaoyan.util.StringUtil;
// import com.cskaoyan.utils.StringUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.lang.model.element.VariableElement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author gzh
 * @version 1.0
 * @date 2022/11/20 13:43:05
 */
@Service
public class BrandServiceImpl implements BrandService {
    @Autowired
    MarketBrandMapper brandMapper;


    /**
     * brand 页面查询
     *
     * @param info
     * @param id
     * @param name
     * @return
     */
    @Override
    public CommonData<MarketBrand> queryBrand(BasePageInfo info, Integer id, String name) {
        PageHelper.startPage (info.getPage (), info.getLimit ());

        MarketBrandExample example = new MarketBrandExample ();
        // example.setOrderByClause (info.getSort ()+""+info.getOrder ());
        MarketBrandExample.Criteria criteria = example.createCriteria ();
        if (id != null) {
            criteria.andIdEqualTo (id);
        }
        if (!StringUtil.isEmpty (name)) {
            criteria.andNameLike ("%" + name + "%");
        }
        criteria.andDeletedEqualTo (false);
        List<MarketBrand> list = brandMapper.selectByExample (example);
        PageInfo<MarketBrand> pageInfo = new PageInfo<> (list);
        return CommonData.data (pageInfo);
    }

    @Override
    public CommonData list(Integer page, Integer limit) {
        // 分页
        PageHelper.startPage (page, limit);
        MarketBrandExample example = new MarketBrandExample ();
        MarketBrandExample.Criteria criteria = example.createCriteria ();
        // criteria.andDeletedEqualTo (false);
        List<MarketBrand> list = brandMapper.selectByExample (example);
        PageInfo<MarketBrand> pageInfo = new PageInfo<> (list);
        return CommonData.data (pageInfo);

    }

    @Override
    public int createBrand(MarketBrand marketBrand) {
        if (StringUtil.isEmpty (marketBrand.getDesc ())||marketBrand.getFloorPrice ()==null){
            return 404;
        }
        // MarketBrandExample example = new MarketBrandExample ();
        // MarketBrandExample.Criteria criteria = example.createCriteria ();
        marketBrand.setDeleted (false);
        marketBrand.setUpdateTime (new Date ());
        marketBrand.setAddTime (new Date ());
        int i = brandMapper.insertSelective (marketBrand);
        if (i == 1) {
            return 200;
        }else {
            return 400;
        }


    }

    @Override
    public int deleteBrand(MarketBrand marketBrand) {
        marketBrand.setDeleted (true);
        marketBrand.setUpdateTime (new Date ());
        MarketBrandExample example = new MarketBrandExample ();
        MarketBrandExample.Criteria criteria = example.createCriteria ();
        criteria.andIdEqualTo (marketBrand.getId ());
        int i = brandMapper.updateByExampleSelective (marketBrand, example);
        if (i==1){
            return 200;
        }else {
            return 404;
        }

    }


    @Override
    public int updateBrand(MarketBrand marketBrand) {
        SimpleDateFormat sdf = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format (new Date ());
        Date date = null;
        try {
            date = sdf.parse (format);
            marketBrand.setUpdateTime (date);
        } catch (ParseException e) {
            e.printStackTrace ();
        }
        MarketBrandExample example = new MarketBrandExample ();
        MarketBrandExample.Criteria criteria = example.createCriteria ();
        criteria.andIdEqualTo (marketBrand.getId ());
        int num = brandMapper.updateByExampleSelective (marketBrand, example);
        if (num == 1) {
            return 200;
        } else {
            return 404;
        }
    }



    @Override
    public MarketBrand detail(Integer id) {
        // MarketBrandExample example = new MarketBrandExample ();
        // MarketBrandExample.Criteria criteria = example.createCriteria ();
        // criteria.andIdEqualTo (id);
        MarketBrand list = brandMapper.selectByPrimaryKey (id);

        return list;
    }
}
