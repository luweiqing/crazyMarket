package com.cskaoyan.service;

import com.cskaoyan.bean.admin.stat.StatVo;

public interface StatService {
    StatVo getStatUser();

    StatVo getStatOrder();

    StatVo getStatGoods();

}
