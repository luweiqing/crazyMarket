package com.cskaoyan.service;

import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.po.MarketUserExample;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.common.User;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.mapper.UserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserServiceImpl implements UserService{

    @Autowired
    UserMapper userMapper;

    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public CommonData<User> query(String username, BasePageInfo info) {
        PageHelper.startPage(info.getPage(), info.getLimit());
        MarketUserExample marketUserExample = new MarketUserExample();
        MarketUserExample.Criteria criteria = marketUserExample.createCriteria();
        if (username != null && !"".equals(username)) {
            criteria.andUsernameLike(username);
        }
        List<MarketUser> marketUsers = marketUserMapper.selectByExample(marketUserExample);

        PageInfo<MarketUser> pageInfo = new PageInfo<>(marketUsers);

        return CommonData.data (pageInfo);
    }

}
