package com.cskaoyan.service;

import com.cskaoyan.bean.admin.mall.ConfigMallVO;
import com.cskaoyan.bean.po.MarketSystem;
import com.cskaoyan.bean.po.MarketSystemExample;
import com.cskaoyan.mapper.MarketSystemMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-21 16:08
 */
@Service
public class ConfigServiceImpl implements ConfigService {

    @Autowired
    MarketSystemMapper marketSystemMapper;

    @Override
    public ConfigMallVO selectMall() {
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("%mall%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);
        ConfigMallVO configMallVO = new ConfigMallVO();
        for (MarketSystem marketSystem : marketSystems) {
            switch (marketSystem.getKeyName()) {
                case "market_mall_latitude":
                    configMallVO.setMarket_mall_latitude(marketSystem.getKeyValue());
                    break;
                case "market_mall_name":
                    configMallVO.setMarket_mall_name(marketSystem.getKeyValue());
                    break;
                case "market_mall_qq":
                    configMallVO.setMarket_mall_qq(marketSystem.getKeyValue());
                    break;
                case "market_mall_longitude":
                    configMallVO.setMarket_mall_longitude(marketSystem.getKeyValue());
                    break;
                case "market_mall_phone":
                    configMallVO.setMarket_mall_phone(marketSystem.getKeyValue());
                    break;
                case "market_mall_address":
                    configMallVO.setMarket_mall_address(marketSystem.getKeyValue());
                    break;

            }
        }
        return configMallVO;


    }

    @Override
    public void selectConfigMall(Map<String, String> map) {
        Set<String> set = map.keySet();
        for (String s : set) {
            if (map.get(s) != null) {
                MarketSystemExample marketSystemExample = new MarketSystemExample();
                MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
                criteria.andKeyNameEqualTo(s);
                MarketSystem marketSystem = new MarketSystem();
                marketSystem.setKeyValue(map.get(s));
                marketSystemMapper.updateByExampleSelective(marketSystem, marketSystemExample);

            }

        }


    }

    @Override
    public ConfigMallVO selectExpress() {
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("%express_freight%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);
        ConfigMallVO configMallVO = new ConfigMallVO();
        for (MarketSystem marketSystem : marketSystems) {
            switch (marketSystem.getKeyName()) {
                case "market_express_freight_value":
                    configMallVO.setMarket_express_freight_value(marketSystem.getKeyValue());
                    break;
                case "market_express_freight_min":
                    configMallVO.setMarket_express_freight_min(marketSystem.getKeyValue());
                    break;


            }
        }
        return configMallVO;

    }

    @Override
    public void selectConfigExpress(Map<String, String> map) {
        Set<String> set = map.keySet();
        for (String s : set) {
            if (map.get(s) != null) {
                MarketSystemExample marketSystemExample = new MarketSystemExample();
                MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
                criteria.andKeyNameEqualTo(s);
                MarketSystem marketSystem = new MarketSystem();
                marketSystem.setKeyValue(map.get(s));
                marketSystemMapper.updateByExampleSelective(marketSystem, marketSystemExample);

            }

        }

    }

    @Override
    public ConfigMallVO selectOrder() {
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("%order%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);
        ConfigMallVO configMallVO = new ConfigMallVO();
        for (MarketSystem marketSystem : marketSystems) {
            switch (marketSystem.getKeyName()) {
                case "market_order_unconfirm":
                    configMallVO.setMarket_order_unconfirm(marketSystem.getKeyValue());
                    break;
                case "market_order_comment":
                    configMallVO.setMarket_order_comment(marketSystem.getKeyValue());
                    break;
                case "market_order_unpaid":
                    configMallVO.setMarket_order_unpaid(marketSystem.getKeyValue());
                    break;


            }
        }
        return configMallVO;

    }

    @Override
    public void selectConfigOrder(Map<String, String> map) {
        Set<String> set = map.keySet();
        for (String s : set) {
            if (map.get(s) != null) {
                MarketSystemExample marketSystemExample = new MarketSystemExample();
                MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
                criteria.andKeyNameEqualTo(s);
                MarketSystem marketSystem = new MarketSystem();
                marketSystem.setKeyValue(map.get(s));
                marketSystemMapper.updateByExampleSelective(marketSystem, marketSystemExample);

            }

        }
    }

    @Override
    public ConfigMallVO selectWx() {
        MarketSystemExample marketSystemExample = new MarketSystemExample();
        MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
        criteria.andKeyNameLike("%wx%");
        List<MarketSystem> marketSystems = marketSystemMapper.selectByExample(marketSystemExample);
        ConfigMallVO configMallVO = new ConfigMallVO();
        for (MarketSystem marketSystem : marketSystems) {
            switch (marketSystem.getKeyName()) {
                case "market_wx_index_new":
                    configMallVO.setMarket_wx_index_new(marketSystem.getKeyValue());
                    break;
                case "market_wx_share":
                    configMallVO.setMarket_mall_name(marketSystem.getKeyValue());
                    break;
                case "market_wx_index_hot":
                    configMallVO.setMarket_wx_index_hot(marketSystem.getKeyValue());
                    break;
                case "market_wx_catlog_goods":
                    configMallVO.setMarket_wx_catlog_goods(marketSystem.getKeyValue());
                    break;
                case "market_wx_catlog_list":
                    configMallVO.setMarket_wx_catlog_list(marketSystem.getKeyValue());
                    break;
                case "market_wx_index_brand":
                    configMallVO.setMarket_wx_index_brand(marketSystem.getKeyValue());
                    break;
                case "market_wx_index_topic":
                    configMallVO.setMarket_wx_index_topic(marketSystem.getKeyValue());
                    break;

            }
        }
        return configMallVO;

    }

    @Override
    public void selectConfigwx(Map<String, String> map) {

        Set<String> set = map.keySet();
        for (String s : set) {
            if (map.get(s) != null) {
                MarketSystemExample marketSystemExample = new MarketSystemExample();
                MarketSystemExample.Criteria criteria = marketSystemExample.createCriteria();
                criteria.andKeyNameEqualTo(s);
                MarketSystem marketSystem = new MarketSystem();
                marketSystem.setKeyValue(map.get(s));
                marketSystemMapper.updateByExampleSelective(marketSystem, marketSystemExample);

            }

        }
    }





}


