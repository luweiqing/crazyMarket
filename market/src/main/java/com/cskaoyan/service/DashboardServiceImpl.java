package com.cskaoyan.service;

import com.cskaoyan.bean.admin.dashboard.DashboardVo;
import com.cskaoyan.bean.po.MarketGoodsExample;
import com.cskaoyan.bean.po.MarketGoodsProductExample;
import com.cskaoyan.bean.po.MarketOrderExample;
import com.cskaoyan.bean.po.MarketUserExample;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketGoodsProductMapper;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:后台页面首页数据显示
 * @Author: Pinocao
 * @CreateTime: 2022-11-19  21:10
 * @Version: 1.0
 */
@Service
public class DashboardServiceImpl implements DashboardService {
    @Autowired
    MarketGoodsMapper marketGoodsMapper;
    @Autowired
    MarketUserMapper marketUserMapper;
    @Autowired
    MarketGoodsProductMapper marketGoodsProductMapper;
    @Autowired
    MarketOrderMapper marketOrderMapper;

    @Override
    public DashboardVo dashboard() {
        // 查询商品总数
        MarketGoodsExample marketGoodsExample = new MarketGoodsExample();
        MarketGoodsExample.Criteria criteria = marketGoodsExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        long goodsTotal = marketGoodsMapper.countByExample(marketGoodsExample);
        // 查询用户总数
        MarketUserExample marketUserExample = new MarketUserExample();
        MarketUserExample.Criteria criteria1 = marketUserExample.createCriteria();
        criteria1.andDeletedEqualTo(false);
        long userTotal = marketUserMapper.countByExample(marketUserExample);
        // 查询产品总数
        MarketGoodsProductExample marketGoodsProductExample = new MarketGoodsProductExample();
        MarketGoodsProductExample.Criteria criteria2 = marketGoodsProductExample.createCriteria();
        criteria2.andDeletedEqualTo(false);
        long productTotal = marketGoodsProductMapper.countByExample(marketGoodsProductExample);
        // 查询订单总数
        MarketOrderExample marketOrderExample = new MarketOrderExample();
        MarketOrderExample.Criteria criteria3 = marketOrderExample.createCriteria();
        criteria3.andDeletedEqualTo(false);
        long orderTotal = marketOrderMapper.countByExample(marketOrderExample);
        // 封装到VO并返回
        DashboardVo dashboardVo = new DashboardVo();
        dashboardVo.setGoodsTotal(goodsTotal);
        dashboardVo.setUserTotal(userTotal);
        dashboardVo.setProductTotal(productTotal);
        dashboardVo.setOrderTotal(orderTotal);
        return dashboardVo;
    }
}