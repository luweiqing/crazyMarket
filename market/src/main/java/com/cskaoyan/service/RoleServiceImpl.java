package com.cskaoyan.service;

import com.cskaoyan.bean.admin.role.bo.RoleSetPermissionsBo;
import com.cskaoyan.bean.admin.role.vo.*;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


/**
 * @author lwq
 * @since 2022/11/20 19:12
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {
    @Autowired
    MarketRoleOptionsMapper marketRoleOptionsMapper;

    @Autowired
    MarketRoleMapper marketRoleMapper;

    @Autowired
    MarketPermissionOptionsMapper marketPermissionOptionsMapper;

    @Autowired
    MarketPermissionMapper marketPermissionMapper;
    @Autowired
    MarketAdminMapper marketAdminMapper;

    /**
     * 查询角色的职位
     *
     * @return com.cskaoyan.bean.common.CommonData
     * @author lwq
     * @since 2022/11/20 19:13
     */
    @Override
    public CommonData options() {
        int total = marketRoleOptionsMapper.selectCount();
        List<MarketRoleOptions> marketRoleOptionsList = marketRoleOptionsMapper.select();
        PageInfo<MarketRoleOptions> pageInfo = new PageInfo<>(marketRoleOptionsList);
        pageInfo.setPageNum(1);
        pageInfo.setPages(1);
        pageInfo.setPageSize(total);
        pageInfo.setTotal(total);
        return CommonData.data(pageInfo);
    }

    /**
     * 查询角色
     *
     * @param basePageInfo
     * @param name
     * @return com.cskaoyan.bean.common.CommonData
     * @author lwq
     * @since 2022/11/21 11:54
     */
    @Override
    public CommonData list(BasePageInfo basePageInfo, String name) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketRoleExample marketRoleExample = new MarketRoleExample();
        MarketRoleExample.Criteria criteria = marketRoleExample.createCriteria();
        if (!StringUtil.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);
        List<MarketRole> marketRoles = marketRoleMapper.selectByExample(marketRoleExample);
        if (marketRoles != null) {
            PageInfo<MarketRole> pageInfo = new PageInfo<>(marketRoles);
            return CommonData.data(pageInfo);
        }
        return null;
    }

    /**
     * 添加角色
     *
     * @param marketRole
     * @return com.cskaoyan.bean.po.MarketRole
     * @author lwq
     * @since 2022/11/21 14:11
     */
    @Override
    public RoleCreateVo create(MarketRole marketRole) {
        marketRole.setEnabled(false);
        marketRole.setAddTime(new Date());
        marketRole.setUpdateTime(new Date());
        int affectedRows = 0;
        affectedRows = marketRoleMapper.insertSelective(marketRole);
        MarketRoleOptions marketRoleOptions = new MarketRoleOptions(null, marketRole.getId(), marketRole.getName(), false);
        affectedRows = marketRoleOptionsMapper.insert(marketRoleOptions);
        if (affectedRows == 1) {
            return new RoleCreateVo(marketRole.getId(), marketRole.getName(), marketRole.getDesc(), marketRole.getAddTime(), marketRole.getUpdateTime());
        }
        return null;
    }

    /**
     * 修改角色
     *
     * @param marketRole
     * @return int
     * @author lwq
     * @since 2022/11/21 14:31
     */
    @Override
    public int update(MarketRole marketRole) {
        marketRole.setUpdateTime(new Date());
        MarketRoleOptions marketRoleOptions = new MarketRoleOptions(null, marketRole.getId(), marketRole.getName(), false);
        int affectedRows = 0;
        affectedRows = marketRoleOptionsMapper.update(marketRoleOptions);
        affectedRows = marketRoleMapper.updateByPrimaryKeySelective(marketRole);
        return affectedRows;
    }

    /**
     * 删除角色
     *
     * @param marketRole
     * @return int
     * @author lwq
     * @since 2022/11/21 15:12
     */
    @Override
    public int delete(MarketRole marketRole) {
        MarketAdminExample marketAdminExample = new MarketAdminExample();
        MarketAdminExample.Criteria criteria = marketAdminExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<MarketAdmin> marketAdmins = marketAdminMapper.selectByExample(marketAdminExample);
        Map<Integer, Integer> map = new HashMap<>();
        for (MarketAdmin marketAdmin : marketAdmins) {
            Integer[] roleIds = marketAdmin.getRoleIds();
            for (Integer roleId : roleIds) {
                if (map.containsKey(roleId)) {
                    map.put(roleId, map.get(roleId) + 1);
                } else {
                    map.put(roleId, 1);
                }
            }
        }
        if (map.get(marketRole.getId()) == null || map.get(marketRole.getId()) == 0) {
            marketRole.setEnabled(false);
        }
        if (marketRole.getEnabled() == true) {
            // 当前角色存在管理员，不能删除
            return 2;
        }
        marketRole.setDeleted(true);
        MarketRoleOptions marketRoleOptions = new MarketRoleOptions(null, marketRole.getId(), marketRole.getName(), true);
        int affectedRows = 0;
        affectedRows = marketRoleOptionsMapper.update(marketRoleOptions);
        affectedRows = marketRoleMapper.updateByPrimaryKeySelective(marketRole);
        return affectedRows;
    }

    /**
     * 查询角色权限
     *
     * @param roleId
     * @return com.cskaoyan.bean.admin.role.vo.RoleGetPermissionsVo
     * @author lwq
     * @since 2022/11/21 20:07
     */
    @Override
    public RoleGetPermissionsVo getPermissions(Integer roleId) {
        List<RoleSystemPermissionAVo> roleSystemPermissionAVos = marketPermissionOptionsMapper.select();
        MarketPermissionExample marketPermissionExample = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria = marketPermissionExample.createCriteria();
        if (roleId != null) {
            criteria.andRoleIdEqualTo(roleId);
        }
        criteria.andDeletedEqualTo(false);
        List<MarketPermission> marketPermissions = marketPermissionMapper.selectByExample(marketPermissionExample);
        List<String> assignedPermissions = new ArrayList<>();
        for (MarketPermission marketPermission : marketPermissions) {
            assignedPermissions.add(marketPermission.getPermission());
        }
        return new RoleGetPermissionsVo(roleSystemPermissionAVos, assignedPermissions);
    }

    /**
     * 将json数据插入到权限选项数据库中
     *
     * @param roleGetPermissionsVo
     * @return void
     * @author lwq
     * @since 2022/11/21 20:07
     */
    @Override
    public void change(RoleGetPermissionsVo roleGetPermissionsVo) {
        /*List<RoleSystemPermissionAVo> systemPermissions = roleGetPermissionsVo.getSystemPermissions();
        for (RoleSystemPermissionAVo systemPermissionA : systemPermissions) {
            MarketPermissionOptions marketPermissionOptionsA = new MarketPermissionOptions(null, 0, systemPermissionA.getId(), systemPermissionA.getLabel(), null);
            marketPermissionOptionsMapper.insert(marketPermissionOptionsA);
            List<RoleSystemPermissionBVo> systemPermissionBList = systemPermissionA.getChildren();
            for (RoleSystemPermissionBVo systemPermissionB : systemPermissionBList) {
                MarketPermissionOptions marketPermissionOptionsB = new MarketPermissionOptions(null, marketPermissionOptionsA.getId(), systemPermissionB.getId(), systemPermissionB.getLabel(), null);
                marketPermissionOptionsMapper.insert(marketPermissionOptionsB);
                List<RoleSystemPermissionCVo> systemPermissionCList = systemPermissionB.getChildren();
                for (RoleSystemPermissionCVo systemPermissionC : systemPermissionCList) {
                    MarketPermissionOptions marketPermissionOptionsC = new MarketPermissionOptions(null, marketPermissionOptionsB.getId(), systemPermissionC.getId(), systemPermissionC.getLabel(), systemPermissionC.getApi());
                    marketPermissionOptionsMapper.insert(marketPermissionOptionsC);
                }
            }
        }*/
    }

    /**
     * 修改角色权限
     *
     * @param roleSetPermissionsBo
     * @return int
     * @author lwq
     * @since 2022/11/21 21:00
     */
    @Override
    public int setPermissions(RoleSetPermissionsBo roleSetPermissionsBo) {
        Integer roleId = roleSetPermissionsBo.getRoleId();
        MarketPermissionExample marketPermissionExample = new MarketPermissionExample();
        MarketPermissionExample.Criteria criteria = marketPermissionExample.createCriteria();
        if (roleId != null) {
            criteria.andRoleIdEqualTo(roleId);
        }
        criteria.andDeletedEqualTo(false);
        marketPermissionMapper.deleteByExample(marketPermissionExample);
        int affectedRows = 0;
        /*for (String permission : roleSetPermissionsBo.getPermissions()) {
            MarketPermission marketPermission = new MarketPermission(null, roleId, permission, new Date(), new Date(), false);
            marketPermissionMapper.insertSelective(marketPermission);
            affectedRows++;
        }*/
        affectedRows = marketPermissionMapper.insertByList(roleId, roleSetPermissionsBo.getPermissions());
        MarketPermission marketPermission = new MarketPermission();
        marketPermission.setAddTime(new Date());
        marketPermission.setUpdateTime(new Date());
        affectedRows = marketPermissionMapper.updateByExampleSelective(marketPermission, marketPermissionExample);
        return affectedRows;
    }

}
