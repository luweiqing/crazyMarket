package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCollect;

public interface CollectService {
    CommonData list(BasePageInfo basePageInfo, Integer valueId, Integer userId);

    CommonData getCollectList(BasePageInfo basePageInfo);

    void addordelete(MarketCollect marketCollect);
}
