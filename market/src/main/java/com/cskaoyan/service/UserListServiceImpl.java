package com.cskaoyan.service;



import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.common.User;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.po.MarketUserExample;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.util.Md5Util;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-19  21:53
 * @Version: 1.0
 */
@Service
public class UserListServiceImpl implements UserListService {
    @Autowired
    MarketUserMapper marketUserMapper;

    @Override
    public CommonData list(BasePageInfo basePageInfo, String username, String mobile) {
        // 启用分页
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        // 根据条件查询，username需要模糊查询
        MarketUserExample marketUserExample = new MarketUserExample();
        MarketUserExample.Criteria criteria = marketUserExample.createCriteria();
        // username和mobile判空
        if (!StringUtil.isEmpty(username)) {
            criteria.andUsernameLike("%" + username + "%");
        }
        if (!StringUtil.isEmpty(mobile)) {
            criteria.andMobileEqualTo(mobile);
        }
        criteria.andDeletedEqualTo(false);
        // 使用排序功能
        marketUserExample.setOrderByClause(basePageInfo.getSort() + " " + basePageInfo.getOrder());
        List<MarketUser> list = marketUserMapper.selectByExample(marketUserExample);
        // 获取pageInfo，以及相关数据
        PageInfo<MarketUser> pageInfo = new PageInfo<>(list);
        // int pageNum = pageInfo.getPageNum();// 获取当前页数
        // int pageSize = pageInfo.getPageSize();// 获取每页数据数
        // int pages = pageInfo.getPages();// 获取总页数
        // long total = pageInfo.getTotal();// 获取总数据数
        return CommonData.data(pageInfo);
    }

    @Override
    public int update(MarketUser marketUser) {
        if (marketUser.getId()==null) {
            return 0;
        }
        String password = marketUser.getPassword();
        String md5 = Md5Util.getMd5(password, "crazy");
        marketUser.setPassword(md5);
        int i = marketUserMapper.updateByPrimaryKeySelective(marketUser);
        if (i != 1) {
            return 0;
        }
        return 1;
    }

    @Override
    public MarketUser detail(Integer id) {
        MarketUser marketUser = marketUserMapper.selectByPrimaryKey(id);
        return marketUser;
    }

}