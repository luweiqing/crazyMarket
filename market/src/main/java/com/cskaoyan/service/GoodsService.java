package com.cskaoyan.service;

import com.cskaoyan.bean.admin.goods.GoodsCateAndBrandVO;
import com.cskaoyan.bean.admin.goods.GoodsCreateBO;
import com.cskaoyan.bean.admin.goods.GoodsDetailVO;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.wx.goods.*;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/20 16:40
 */
public interface GoodsService {
    CommonData<MarketGoods> list(BasePageInfo pageInfo, String goodsSn, String goodsName, Integer goodsId);

    GoodsDetailVO detail(Integer id);

    GoodsCateAndBrandVO catAndBrand();

    void create(GoodsCreateBO createBO);

    void update(GoodsCreateBO update);

    void delete(MarketGoods goods);

    WxGoodsCategoryVO getCategorysInfo(Integer id);

    WxGoodsListVO selectGoodsByCondition(MarketGoods goods, BasePageInfo info);

    WxHomeVO showWxHome();

    int count();

    WxGoodsDetailVO selectGoodsDetail(Integer id);

    CommonData<MarketGoods> selectRelatedGoods(Integer id);

}
