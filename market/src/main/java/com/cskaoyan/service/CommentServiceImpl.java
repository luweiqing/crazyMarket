package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketComment;
import com.cskaoyan.bean.po.MarketCommentExample;
import com.cskaoyan.bean.po.MarketUser;
import com.cskaoyan.bean.wx.comment.WxComentListVO;
import com.cskaoyan.bean.wx.comment.WxCommentBO;
import com.cskaoyan.bean.wx.comment.WxCommentCountVO;
import com.cskaoyan.bean.wx.comment.WxCommentUserInfoVO;
import com.cskaoyan.mapper.MarketCommentMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/21 16:16
 */
@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    MarketCommentMapper commentMapper;

    @Autowired
    MarketUserMapper userMapper;

    /**
     * @param info    分页信息
     * @param userId  用户id
     * @param valueId 商品id
     * @return
     */
    @Override
    public CommonData<MarketComment> list(BasePageInfo info, String userId, String valueId) {
        MarketCommentExample commentExample = new MarketCommentExample();
        // 排序
        commentExample.setOrderByClause(info.getSort() + " " + info.getOrder());

        MarketCommentExample.Criteria criteria = commentExample.createCriteria();
        // 判空，拼接条件
        if (!StringUtil.isEmpty(userId)) {
            criteria.andUserIdEqualTo(Integer.parseInt(userId));
        }
        if (!StringUtil.isEmpty(valueId)) {
            criteria.andValueIdEqualTo(Integer.parseInt(valueId));
        }
        criteria.andDeletedEqualTo(false);
        // 只显示未回复的评论
        // criteria.andAdminContentIsNull();
        criteria.andAdminContentEqualTo("");
        // 开启分页
        PageHelper.startPage(info.getPage(), info.getLimit());
        List<MarketComment> comments = commentMapper.selectByExample(commentExample);
        PageInfo<MarketComment> commentPageInfo = new PageInfo<>(comments);
        return CommonData.data2(commentPageInfo);
    }

    /**
     * 小程序商品详情显示评论
     *
     * @param commentBO
     * @return
     */
    @Override
    public CommonData<WxComentListVO> wxList(WxCommentBO commentBO) {
        MarketCommentExample example = new MarketCommentExample();
        MarketCommentExample.Criteria criteria = example.createCriteria();
        // type = 0 时表示商品评论 valueId 表示商品id
        // type = 1 时表示专题评论 valueId 表示专题
        criteria.andValueIdEqualTo(commentBO.getValueId());
        criteria.andTypeEqualTo(commentBO.getType());
        //showType = 0 表示查询所有评论,showType = 1查询有图评论
        if (commentBO.getShowType() == 1) {
            criteria.andHasPictureEqualTo(true);
        }
        // 开启分页
        PageHelper.startPage(commentBO.getPage(),commentBO.getLimit());
        List<MarketComment> comments = commentMapper.selectByExample(example);
        List<WxComentListVO> wxComentListVOS = new LinkedList<>();
        // 查找每一条评论的信息，和对应的user信息
        for (MarketComment comment : comments) {
            Integer userId = comment.getUserId();
            MarketUser user = userMapper.selectByPrimaryKey(userId);
            WxCommentUserInfoVO userInfo = new WxCommentUserInfoVO(user.getAvatar(), user.getNickname());
            WxComentListVO comentListVO = new WxComentListVO(comment.getAddTime(), comment.getAdminContent(), comment.getContent(),
                    comment.getPicUrls(), userInfo);
            wxComentListVOS.add(comentListVO);
        }
        // 返回分页结果
        PageInfo<WxComentListVO> listVOPageInfo = new PageInfo<>(wxComentListVOS);
        CommonData<WxComentListVO> commonData = CommonData.data2(listVOPageInfo);
        // commonData.setTotal(comments.size());
        // commonData.setPages(comments.size()/commentBO.getLimit() + 1);
        // return commonData;
        // CommonData<WxComentListVO> commonData = new CommonData<>();
        commonData.setList(wxComentListVOS);
        commonData.setPage(commentBO.getPage());
        commonData.setPages(wxComentListVOS.size()/commentBO.getLimit() + 1);
        commonData.setLimit(commentBO.getLimit());
        commonData.setTotal(wxComentListVOS.size());
        return commonData;
    }

    /**
     * 删除商品的评论,把评论逻辑删除
     *
     * @param comment
     */
    @Transactional
    @Override
    public void delete(MarketComment comment) {
            comment.setDeleted(true);
            commentMapper.updateByPrimaryKeySelective(comment);

    }


    /**
     * 统计所有评论和有图片的评论
     *
     * @return
     */
    @Override
    public WxCommentCountVO count(WxCommentBO commentBO) {
        MarketCommentExample example = new MarketCommentExample();
        MarketCommentExample.Criteria criteria = example.createCriteria();
        criteria.andValueIdEqualTo(commentBO.getValueId());
        criteria.andTypeEqualTo(commentBO.getType());
        List<MarketComment> comments = commentMapper.selectByExample(example);
        int hasPicCount = 0;
        for (MarketComment comment : comments) {
            if (comment.getHasPicture()) {
                hasPicCount++;
            }
        }
        return new WxCommentCountVO(comments.size(), hasPicCount);
    }


    /**
     * 新增商品评论
     *
     * @param comment
     */
    @Transactional
    @Override
    public void insert(MarketComment comment,MarketUser user) {
        comment.setUserId(user.getId());
        Date date = new Date();
        comment.setAddTime(date);
        comment.setUpdateTime(date);
        commentMapper.insertSelective(comment);
    }
}
