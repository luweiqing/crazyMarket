package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAddress;

public interface AddressService {
    CommonData list(BasePageInfo basePageInfo, Integer userId, String name);

    CommonData querylist();

    int insertAddress(MarketAddress marketAddress,Integer id);

    MarketAddress detailId(Integer id);


    int deleteAddress(Integer id);
}
