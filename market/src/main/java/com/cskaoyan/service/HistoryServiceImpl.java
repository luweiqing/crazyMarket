package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketSearchHistory;
import com.cskaoyan.bean.po.MarketSearchHistoryExample;
import com.cskaoyan.mapper.MarketSearchHistoryMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  15:56
 * @Version: 1.0
 */
@Service
public class HistoryServiceImpl implements HistoryService {
    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Override
    public CommonData list(BasePageInfo basePageInfo, Integer userId, String keyword) {
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
        marketSearchHistoryExample.setOrderByClause(basePageInfo.getSort()+" "+basePageInfo.getOrder());
        MarketSearchHistoryExample.Criteria criteria = marketSearchHistoryExample.createCriteria();
        if (userId!=null) {
            criteria.andUserIdEqualTo(userId);
        }
        criteria.andUserIdNotEqualTo(0);
        if (!StringUtil.isEmpty(keyword)) {
            criteria.andKeywordLike("%"+keyword+"%");
        }
        criteria.andDeletedEqualTo(false);
        List<MarketSearchHistory> list = marketSearchHistoryMapper.selectByExample(marketSearchHistoryExample);
        PageInfo<MarketSearchHistory> pageInfo = new PageInfo<>(list);
        return CommonData.data(pageInfo);
    }
}