package com.cskaoyan.service;

import com.cskaoyan.bean.admin.stat.GoodsStat;
import com.cskaoyan.bean.admin.stat.OrderStat;
import com.cskaoyan.bean.admin.stat.StatVo;
import com.cskaoyan.bean.admin.stat.UserStat;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketOrderMapper;
import com.cskaoyan.mapper.MarketUserMapper;
import com.cskaoyan.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/21 19:32
 */
@Service
public class StatServiceImpl implements StatService {
    @Autowired
    MarketUserMapper marketUserMapper;
    @Autowired
    MarketGoodsMapper marketGoodsMapper;
    @Autowired
    MarketOrderMapper marketOrderMapper;

    @Override
    public StatVo getStatUser() {
        List<UserStat> userStats = marketUserMapper.getStatUser();
        StatVo<UserStat> userStatStatVo = new StatVo<>(Constant.USER_COLUMS,userStats);
        return userStatStatVo;
    }

    @Override
    public StatVo getStatOrder() {
        List<OrderStat> orderStats = marketOrderMapper.getStatOrder();
        for (OrderStat orderStat : orderStats) {
            BigDecimal amount = orderStat.getAmount();
            Double pcr = amount.doubleValue()/orderStat.getOrders();
            orderStat.setPcr(pcr);
        }
        StatVo<OrderStat> userStatStatVo = new StatVo<>(Constant.ORDER_COLUMS,orderStats);
        return userStatStatVo;
    }

    @Override
    public StatVo getStatGoods() {
        List<GoodsStat> goodsStats = marketGoodsMapper.getStatGoods();
        StatVo<GoodsStat> userStatStatVo = new StatVo<>(Constant.GOODS_COLUMS,goodsStats);
        return userStatStatVo;
    }
}
