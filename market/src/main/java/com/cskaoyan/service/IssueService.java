package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketIssue;

public interface IssueService {
    CommonData getIssueList(BasePageInfo basePageInfo, String question);

    MarketIssue creatIssue(String question, String answer);

    MarketIssue updateIssue(MarketIssue marketIssue);

    void deleteIssue(MarketIssue issue);
}
