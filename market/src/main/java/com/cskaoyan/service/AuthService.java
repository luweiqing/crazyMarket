package com.cskaoyan.service;

import com.cskaoyan.bean.po.InfoData;
import com.cskaoyan.bean.po.MarketAdmin;
import com.cskaoyan.bean.wx.auth.AuthRegisterBo;
import com.cskaoyan.bean.wx.auth.AuthRegisterVo;

public interface AuthService {
    InfoData info(MarketAdmin marketAdmin);

    int regCaptcha(String mobileStr);

    AuthRegisterVo register(AuthRegisterBo authRegisterBo);

    int reset(AuthRegisterBo authRegisterBo);
}
