package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAd;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-19 20:56
 */
public interface AdService {


    CommonData<MarketAd> list(BasePageInfo info, String name, String content);

    MarketAd AdUpdate(MarketAd marketAd);


    boolean deleteAd(MarketAd marketAd);

    MarketAd AdCreate(MarketAd marketAdinfo);
}
