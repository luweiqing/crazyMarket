package com.cskaoyan.service;

import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.wx.search.SearchIndexVo;
import com.cskaoyan.mapper.MarketKeywordMapper;
import com.cskaoyan.mapper.MarketSearchHistoryMapper;
import com.cskaoyan.util.ShiroUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  15:49
 * @Version: 1.0
 */
@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    MarketKeywordMapper marketKeywordMapper;
    @Autowired
    MarketSearchHistoryMapper marketSearchHistoryMapper;

    @Override
    public SearchIndexVo index() {
        // 查询默认关键词
        MarketKeywordExample marketKeywordExample = new MarketKeywordExample();
        MarketKeywordExample.Criteria criteria = marketKeywordExample.createCriteria();
        criteria.andIsDefaultEqualTo(true);
        criteria.andDeletedEqualTo(false);
        List<MarketKeyword> dafultKeyList = marketKeywordMapper.selectByExample(marketKeywordExample);
        // 查询热门关键词
        MarketKeywordExample marketKeywordExample1 = new MarketKeywordExample();
        MarketKeywordExample.Criteria criteria1 = marketKeywordExample1.createCriteria();
        criteria1.andIsHotEqualTo(true);
        criteria1.andDeletedEqualTo(false);
        List<MarketKeyword> hotKeyList = marketKeywordMapper.selectByExample(marketKeywordExample1);
        // 查询搜索历史关键词
        Integer userId = ShiroUtil.getUserId();
        if (userId == null){
            userId = 0;
        }
        MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
        marketSearchHistoryExample.setOrderByClause("update_time desc");
        MarketSearchHistoryExample.Criteria criteria2 = marketSearchHistoryExample.createCriteria();
        criteria2.andUserIdEqualTo(userId);
        criteria2.andDeletedEqualTo(false);
        List<MarketSearchHistory> searchHistories = marketSearchHistoryMapper.selectByExample(marketSearchHistoryExample);
        SearchIndexVo searchIndexVo = new SearchIndexVo(dafultKeyList.get(0),hotKeyList,searchHistories);
        return searchIndexVo;
    }

    @Override
    public List<String> helper(String keyword) {
        String keyword1 = "%" + keyword + "%";
        List<String> list = marketKeywordMapper.selectLikeKeyword(keyword1);
        return list;
    }

    @Override
    public int clearHistory() {
        Integer userId = ShiroUtil.getUserId();
        if (userId == null){
            userId = 0;
        }
        // 逻辑删除
        MarketSearchHistoryExample marketSearchHistoryExample = new MarketSearchHistoryExample();
        MarketSearchHistoryExample.Criteria criteria = marketSearchHistoryExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        criteria.andUserIdEqualTo(userId);
        MarketSearchHistory record = new MarketSearchHistory();
        record.setDeleted(true);
        int i = marketSearchHistoryMapper.updateByExampleSelective(record, marketSearchHistoryExample);
        return 1;
    }
}