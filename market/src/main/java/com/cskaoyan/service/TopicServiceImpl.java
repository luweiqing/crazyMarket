package com.cskaoyan.service;

import com.cskaoyan.bean.admin.topic.bo.TopicBatchDeleteBO;
import com.cskaoyan.bean.admin.topic.vo.TopicReadVO;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.po.MarketTopic;
import com.cskaoyan.bean.po.MarketTopicExample;
import com.cskaoyan.mapper.MarketGoodsMapper;
import com.cskaoyan.mapper.MarketTopicMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 20:58
 */
@Service
public class TopicServiceImpl implements TopicService {
    @Autowired
    MarketTopicMapper marketTopicMapper;
    @Autowired
    MarketGoodsMapper marketGoodsMapper;

    @Override
    public CommonData<MarketTopic> TopicList(BasePageInfo basePageInfo, MarketTopic marketTopic) {

        //分页插件
        PageHelper.startPage(basePageInfo.getPage(), basePageInfo.getLimit());
        //查询操作
        MarketTopicExample marketTopicExample = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = marketTopicExample.createCriteria();
        //专题标题模糊查询
        if (marketTopic.getTitle() != null) {
            criteria.andTitleLike("%" + marketTopic.getTitle() + "%");
        }
        //专题子标题模糊查询
        if (marketTopic.getSubtitle() != null) {
            criteria.andSubtitleLike("%" + marketTopic.getSubtitle() + "%");
        }
        //查看的是逻辑为删除的，deleted=0的
        criteria.andDeletedEqualTo(false);
        //排序规则
        if (basePageInfo.getSort() != null) {
            if (basePageInfo.getOrder() != null) {
                marketTopicExample.setOrderByClause(basePageInfo.getSort() + " " + basePageInfo.getOrder());
            } else {
                marketTopicExample.setOrderByClause(basePageInfo.getSort());
            }
        }
        List<MarketTopic> marketTopics = marketTopicMapper.selectByExampleWithBLOBs(marketTopicExample);
        PageInfo<MarketTopic> marketTopicPageInfo = new PageInfo<>(marketTopics);
        return CommonData.data(marketTopicPageInfo);
    }

    @Override
    public MarketTopic TopicCreate(MarketTopic marketTopic) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //时间
        String format = sdf.format(new Date());
        //转换为时间类型
        try {
            Date time = sdf.parse(format);
            //添加添加时间和更新时间
            marketTopic.setAddTime(time);
            marketTopic.setUpdateTime(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //插入操作，需要修改对应的xml使得获取id
        marketTopicMapper.insertSelective(marketTopic);
        return marketTopic;

    }

    @Override
    public MarketTopic updateTopic(MarketTopic marketTopic) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String format = sdf.format(new Date());
        try {
            Date time = sdf.parse(format);
            marketTopic.setUpdateTime(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        MarketTopicExample marketTopicExample = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = marketTopicExample.createCriteria();
        criteria.andIdEqualTo(marketTopic.getId());
        int i = marketTopicMapper.updateByExampleSelective(marketTopic, marketTopicExample);
        if (i == 1) {
            return marketTopic;
        } else {
            return null;
        }


    }

    @Override
    public TopicReadVO TopicRead(Integer id) {
        MarketTopic marketTopic = marketTopicMapper.selectByPrimaryKey(id);
        Integer[] goods = marketTopic.getGoods();
        ArrayList<MarketGoods> marketGoodsArrayList = new ArrayList<>();
        for (Integer good : goods) {
            MarketGoods marketGoods = marketGoodsMapper.selectByPrimaryKey(good);
            marketGoodsArrayList.add(marketGoods);
        }
        TopicReadVO topicReadVO = new TopicReadVO(marketGoodsArrayList, marketTopic);
        return topicReadVO;


    }

    @Override
    public boolean TopicDelete(MarketTopic marketTopic) {
        boolean result = topicDeleteById(marketTopic.getId());
        return result;
    }

    @Override
    public boolean topicBatchDelete(TopicBatchDeleteBO ids) {
        Integer[] idslist = ids.getIds();
        for (Integer id : idslist) {
            boolean result = topicDeleteById(id);
            if (result == false) {
                return false;
            }
        }
        return true;


    }


    private boolean topicDeleteById(Integer id) {
        MarketTopic marketTopic = new MarketTopic();
        marketTopic.setDeleted(true);

        MarketTopicExample marketTopicExample = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = marketTopicExample.createCriteria();
        criteria.andIdEqualTo(id);
        int i = marketTopicMapper.updateByExampleSelective(marketTopic, marketTopicExample);
        if (i == 1) {
            return true;
        }
        return false;
    }


    /*
     * @Author makabaka
     * @Description //TODO
     * @Date 16:59 2022/11/22
     * @Param
     * @return
     **/


    @Override
    public CommonData wxTopicList(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        MarketTopicExample marketTopicExample = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = marketTopicExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<MarketTopic> marketTopics = marketTopicMapper.selectByExample(marketTopicExample);

        PageInfo<MarketTopic> marketTopicPageInfo = new PageInfo<>(marketTopics);
        return CommonData.data(marketTopicPageInfo);


    }

    @Override
    public CommonData wxTopicRealted(Integer id) {
        PageHelper.startPage(1, 4);
        MarketTopicExample marketTopicExample = new MarketTopicExample();
        MarketTopicExample.Criteria criteria = marketTopicExample.createCriteria();
        criteria.andIdEqualTo(id);
        criteria.andDeletedEqualTo(false);

        List<MarketTopic> marketTopics = marketTopicMapper.selectByExample(marketTopicExample);
        PageInfo<MarketTopic> marketTopicPageInfo = new PageInfo<>(marketTopics);

        return CommonData.data(marketTopicPageInfo);


    }


}
