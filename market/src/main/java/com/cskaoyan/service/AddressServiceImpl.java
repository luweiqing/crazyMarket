package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketAddress;
import com.cskaoyan.bean.po.MarketAddressExample;
import com.cskaoyan.bean.po.MarketCategoryExample;
import com.cskaoyan.mapper.MarketAddressMapper;
import com.cskaoyan.util.StringUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-20  14:10
 * @Version: 1.0
 */
@Service
public class AddressServiceImpl implements AddressService {
    @Autowired
    MarketAddressMapper marketAddressMapper;

    @Override
    public CommonData list(BasePageInfo basePageInfo, Integer userId, String name) {
        PageHelper.startPage (basePageInfo.getPage (), basePageInfo.getLimit ());
        MarketAddressExample marketAddressExample = new MarketAddressExample ();
        MarketAddressExample.Criteria criteria = marketAddressExample.createCriteria ();
        marketAddressExample.setOrderByClause (basePageInfo.getSort () + " " + basePageInfo.getOrder ());
        if (userId != null) {
            criteria.andUserIdEqualTo (userId);
        }
        if (!StringUtil.isEmpty (name)) {
            criteria.andNameLike ("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);
        List<MarketAddress> list = marketAddressMapper.selectByExample (marketAddressExample);
        PageInfo<MarketAddress> pageInfo = new PageInfo<> (list);
        return CommonData.data (pageInfo);
    }

    @Override
    public CommonData querylist() {
        MarketAddressExample example = new MarketAddressExample ();
        MarketAddressExample.Criteria criteria = example.createCriteria ();
        criteria.andDeletedEqualTo (false);
        List<MarketAddress> marketAddresses = marketAddressMapper.selectByExample (example);
        PageInfo<MarketAddress> pageInfo = new PageInfo<> (marketAddresses);
        return CommonData.data (pageInfo);
    }
    @Override
    public int insertAddress(MarketAddress marketAddress,Integer id) {

        if (StringUtil.isEmpty (marketAddress.getTel ()) || marketAddress.getTel ().length () != 11) {
            return 405;
        }
        if (isNumLegal (marketAddress.getTel ()) == false) {
            return 405;
        }
        if (marketAddress.getIsDefault ()) {
                // 设置默认值
                MarketAddressExample example = new MarketAddressExample ();
                MarketAddressExample.Criteria criteria = example.createCriteria ();
                criteria.andIsDefaultEqualTo (true);
            MarketAddress marketAddress1 = new MarketAddress ();
            marketAddress1.setIsDefault (false);
            int i = marketAddressMapper.updateByExampleSelective (marketAddress1, example);
            }
        if (marketAddress.getId () != 0) {
            marketAddress.setUpdateTime (new Date ());
            MarketAddressExample example = new MarketAddressExample ();
            MarketAddressExample.Criteria criteria = example.createCriteria ();
            criteria.andIdEqualTo (marketAddress.getId ());

            int i = marketAddressMapper.updateByExampleSelective (marketAddress, example);

            if (i!=1){
                return 0;
            }
        }else {
            marketAddress.setUserId (id);
            marketAddress.setUpdateTime (new Date ());
            marketAddress.setAddTime (new Date ());
            int i = marketAddressMapper.insertSelective (marketAddress);

            if (i!=1){
                return 0;
            }
        }
        return marketAddress.getId ();
    }


    @Override
    public MarketAddress detailId(Integer id) {
        MarketAddressExample example = new MarketAddressExample ();
        MarketAddressExample.Criteria criteria = example.createCriteria ();
        criteria.andIdEqualTo (id);
        MarketAddress marketAddress = marketAddressMapper.selectByPrimaryKey (id);
        return marketAddress;
    }

    @Override
    public int deleteAddress(Integer id) {
        MarketAddress marketAddress = new MarketAddress ();
        marketAddress.setDeleted (true);
        marketAddress.setUpdateTime (new Date ());
        MarketAddressExample example = new MarketAddressExample ();
        MarketAddressExample.Criteria criteria = example.createCriteria ();
        criteria.andIdEqualTo (id);
        marketAddressMapper.updateByExampleSelective (marketAddress, example);
        return 200;
    }

    private static boolean isNumLegal(String input) {
        String reExp = "^((13[0-9])|(15[^4])|(18[0,2,3,5-9])|(17[0-8])|(147))\\d{8}$";

        Pattern p = Pattern.compile (reExp);
        Matcher m = p.matcher (input);
        return m.matches ();


    }
}