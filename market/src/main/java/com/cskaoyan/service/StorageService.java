package com.cskaoyan.service;

import com.cskaoyan.bean.admin.storage.vo.StorageCreateVo;

import com.cskaoyan.bean.admin.storage.vo.StorageUpdateVo;
import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketStorage;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;




public interface StorageService {
    StorageCreateVo create(MultipartFile file) throws IOException;

    CommonData list(BasePageInfo basePageInfo, String key, String name);

    StorageUpdateVo update(MarketStorage marketStorage);

    int delete(MarketStorage marketStorage);



}
