package com.cskaoyan.service;


import com.cskaoyan.bean.admin.region.vo.RegionListVoa;
import com.cskaoyan.bean.common.CommonData;

/**
 * @author
 * @version 1.0
 * @date 2022/11/21 21:28:38
 */
public interface RegionService {

    CommonData<RegionListVoa> queryList();
}
