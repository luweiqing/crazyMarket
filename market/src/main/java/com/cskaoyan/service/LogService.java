package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;

public interface LogService {
    CommonData list(BasePageInfo basePageInfo, String name);
}
