package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketKeyword;

public interface KeywordService {
    CommonData getKeywordList(BasePageInfo basePageInfo, String keyword, String url);

    MarketKeyword creatKeyword(MarketKeyword marketKeyword);

    MarketKeyword updateKeyword(MarketKeyword marketKeyword);

    void deleteKeyword(MarketKeyword marketKeyword);

}
