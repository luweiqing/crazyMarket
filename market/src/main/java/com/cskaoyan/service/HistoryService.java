package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;

public interface HistoryService {
    CommonData list(BasePageInfo basePageInfo, Integer userId, String keyword);
}
