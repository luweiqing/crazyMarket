package com.cskaoyan.service;

import com.cskaoyan.bean.common.BasePageInfo;
import com.cskaoyan.bean.common.CommonData;

public interface FootprintService {
    CommonData list(BasePageInfo basePageInfo, Integer goodsId, Integer userId);

    CommonData getFootprintList(BasePageInfo basePageInfo, Integer userId);

    void deleteFootprint(Integer id);
    // id 为商品ID,在访问商品详情的时候把足迹加入足迹表中
    void addFootprint(Integer id, Integer userId);
}
