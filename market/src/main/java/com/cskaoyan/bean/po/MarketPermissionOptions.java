package com.cskaoyan.bean.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/21 17:50
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MarketPermissionOptions {
    private Integer id;
    private Integer pId;
    private String name;
    private String label;
    private String api;
}
