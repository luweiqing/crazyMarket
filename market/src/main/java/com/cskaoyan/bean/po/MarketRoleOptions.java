package com.cskaoyan.bean.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/20 19:14
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketRoleOptions {
    private Integer id;
    private Integer value;
    private String label;
    private boolean deleted;
}
