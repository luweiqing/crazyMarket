package com.cskaoyan.bean.wx.comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 22:11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxCommentCountVO {
    Integer allCount;
    Integer hasPicCount;
}
