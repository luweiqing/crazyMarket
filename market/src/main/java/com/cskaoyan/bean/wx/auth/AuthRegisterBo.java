package com.cskaoyan.bean.wx.auth;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-25  10:42
 * @Version: 1.0
 */
public class AuthRegisterBo {
    /**
     * password : qqqwww
     * code : 135006
     * wxCode : 023jSv200HbQ0P1wjg100awIFz1jSv2X
     * mobile : 13372613346
     * username : qqqwww
     */
    private String password;
    private String code;
    private String wxCode;
    private String mobile;
    private String username;

    public void setPassword(String password) {
        this.password = password;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setWxCode(String wxCode) {
        this.wxCode = wxCode;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public String getCode() {
        return code;
    }

    public String getWxCode() {
        return wxCode;
    }

    public String getMobile() {
        return mobile;
    }

    public String getUsername() {
        return username;
    }
}