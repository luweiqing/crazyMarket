package com.cskaoyan.bean.wx.order.vo;

import com.cskaoyan.bean.po.MarketOrderGoods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/22 10:28
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class WxOrderListVo {

    private String orderStatusText;
    private Short aftersaleStatus;
    private Boolean isGroupin;
    private String orderSn;
    private BigDecimal actualPrice;
    private Integer id;
    private List<MarketOrderGoods> goodsList;
    private HandleOption handleOption;
}
