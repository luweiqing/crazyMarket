package com.cskaoyan.bean.wx.order.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhenyu
 * @since 2022/11/22 11:12
 */

@NoArgsConstructor
@AllArgsConstructor
@Data

public class HandleOption {

    private boolean cancel;
    private boolean delete;
    private boolean pay;
    private boolean comment;
    private boolean confirm;
    private boolean refund;
    private boolean rebuy;
    private boolean aftersale;


}
