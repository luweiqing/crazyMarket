package com.cskaoyan.bean.wx.order.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangzhenyu
 * @since 2022/11/22 15:20
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class WxOrderInfoOfWxOrderDetailVo {

    private String expName;
    private String consignee;
    private String address;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GTM+8")
    private Date addTime;
    private String orderSn;
    private BigDecimal actualPrice;
    private String mobile;
    private String message;
    private String expCode;
    private String orderStatusText;
    private Short aftersaleStatus;
    private BigDecimal goodsPrice;
    private String expNo;
    private BigDecimal couponPrice;
    private Integer id;
    private BigDecimal freightPrice;
    private HandleOption handleOption;

}
