package com.cskaoyan.bean.wx.comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 21:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCommentBO {
    Integer valueId;
    byte type;
    Integer limit;
    Integer page;
    byte showType;

    public WxCommentBO(Integer valueId) {
        this.valueId = valueId;
    }
}
