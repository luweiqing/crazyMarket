package com.cskaoyan.bean.wx.auth;

import lombok.Data;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  15:01
 * @Version: 1.0
 */
@Data
public class AuthLoginVo {
    private UserInfoDate userInfo;
    private String token;
}