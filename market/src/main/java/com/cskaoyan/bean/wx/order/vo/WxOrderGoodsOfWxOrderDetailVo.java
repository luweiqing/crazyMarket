package com.cskaoyan.bean.wx.order.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/22 15:24
 */

@NoArgsConstructor
@Data
public class WxOrderGoodsOfWxOrderDetailVo {

    private Integer id;
    private Integer orderId;
    private Integer goodsId;
    private String goodsName;
    private String goodsSn;
    private Integer productId;
    private Integer number;
    private Integer price;
    private List<String> specifications;
    private String picUrl;
    private Integer comment;
    private String addTime;
    private String updateTime;
    private Boolean deleted;
}
