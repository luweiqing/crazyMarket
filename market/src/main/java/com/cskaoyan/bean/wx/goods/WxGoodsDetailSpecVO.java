package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.po.MarketGoodsSpecification;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 17:09
 */
@NoArgsConstructor
@AllArgsConstructor
public class WxGoodsDetailSpecVO {
    String name;
    List<MarketGoodsSpecification> valueList;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MarketGoodsSpecification> getValueList() {
        return valueList;
    }

    public void setValueList(List<MarketGoodsSpecification> valueList) {
        this.valueList = valueList;
    }
}
