package com.cskaoyan.bean.wx.search;

import com.cskaoyan.bean.po.MarketKeyword;
import com.cskaoyan.bean.po.MarketSearchHistory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  15:23
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchIndexVo {
    private MarketKeyword defaultKeyword;
    private List<MarketKeyword> hotKeywordList;
    private List<MarketSearchHistory> historyKeywordList;
}