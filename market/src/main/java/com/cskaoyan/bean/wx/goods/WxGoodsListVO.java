package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketCategory;
import com.cskaoyan.bean.po.MarketGoods;
import com.github.pagehelper.PageInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 10:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxGoodsListVO<T> {
    List<MarketCategory> filterCategoryList;

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<T> list;


    public static <T> WxGoodsListVO<T> data(PageInfo<T> pageInfo) {
        int pageNum = pageInfo.getPageNum();
        int pageSize = pageInfo.getPageSize();
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();
        List<T> list = pageInfo.getList();
        return new WxGoodsListVO<T>(null,(int) total,pages,pageSize,pageNum,list);
    }


}
