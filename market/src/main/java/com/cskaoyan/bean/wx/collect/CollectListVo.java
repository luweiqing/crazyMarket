package com.cskaoyan.bean.wx.collect;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  22:07
 * @Version: 1.0
 */
public class CollectListVo {
    /**
     * brief : 水洗亚麻，透气亲肤
     * picUrl : http://yanxuan.nosdn.127.net/8a9ee5ba08929cc9e40b973607d2f633.png
     * valueId : 1057036
     * name : 日式纯色水洗亚麻抱枕
     * id : 50
     * type : 0
     * retailPrice : 79.0
     */
    private String brief;
    private String picUrl;
    private int valueId;
    private String name;
    private int id;
    private int type;
    private double retailPrice;

    public void setBrief(String brief) {
        this.brief = brief;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setRetailPrice(double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public String getBrief() {
        return brief;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public int getValueId() {
        return valueId;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public double getRetailPrice() {
        return retailPrice;
    }
}