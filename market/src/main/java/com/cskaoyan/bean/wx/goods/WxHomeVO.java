package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.po.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 14:13
 */
@AllArgsConstructor
@NoArgsConstructor
public class WxHomeVO {
    List<MarketAd> banner;
    List<MarketBrand> brandList;
    List<MarketCategory> channel;
    List<MarketCoupon> couponList;
    List<WxFloorGoodsVO> floorGoodsList;
    List<MarketGoods> hotGoodsList;
    List<MarketGoods> newGoodsList;
    List<MarketTopic> topicList;

    public List<MarketAd> getBanner() {
        return banner;
    }

    public void setBanner(List<MarketAd> banner) {
        this.banner = banner;
    }

    public List<MarketBrand> getBrandList() {
        return brandList;
    }

    public void setBrandList(List<MarketBrand> brandList) {
        this.brandList = brandList;
    }

    public List<MarketCategory> getChannel() {
        return channel;
    }

    public void setChannel(List<MarketCategory> channel) {
        this.channel = channel;
    }

    public List<MarketCoupon> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<MarketCoupon> couponList) {
        this.couponList = couponList;
    }

    public List<WxFloorGoodsVO> getFloorGoodsList() {
        return floorGoodsList;
    }

    public void setFloorGoodsList(List<WxFloorGoodsVO> floorGoodsList) {
        this.floorGoodsList = floorGoodsList;
    }

    public List<MarketGoods> getHotGoodsList() {
        return hotGoodsList;
    }

    public void setHotGoodsList(List<MarketGoods> hotGoodsList) {
        this.hotGoodsList = hotGoodsList;
    }

    public List<MarketGoods> getNewGoodsList() {
        return newGoodsList;
    }

    public void setNewGoodsList(List<MarketGoods> newGoodsList) {
        this.newGoodsList = newGoodsList;
    }

    public List<MarketTopic> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<MarketTopic> topicList) {
        this.topicList = topicList;
    }
}
