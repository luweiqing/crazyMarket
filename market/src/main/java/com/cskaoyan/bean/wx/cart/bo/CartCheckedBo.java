package com.cskaoyan.bean.wx.cart.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/22 19:48
 */

@NoArgsConstructor
@Data
public class CartCheckedBo {
    private List<Integer> productIds;
    private Integer isChecked;
}
