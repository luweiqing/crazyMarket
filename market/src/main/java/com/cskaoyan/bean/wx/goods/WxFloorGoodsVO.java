package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.po.MarketGoods;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 15:05
 */
@NoArgsConstructor
@AllArgsConstructor
public class WxFloorGoodsVO {
    List<MarketGoods> goodsList;
    Integer id;
    String name;

    public List<MarketGoods> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<MarketGoods> goodsList) {
        this.goodsList = goodsList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
