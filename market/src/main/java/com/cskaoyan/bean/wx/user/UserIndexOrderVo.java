package com.cskaoyan.bean.wx.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  21:35
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserIndexOrderVo {
    private UserIndexVo order;
}