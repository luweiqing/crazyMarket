package com.cskaoyan.bean.wx.cart.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author lwq
 * @since 2022/11/23 10:06
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CartCheckoutCheckedGoodsVo {

    private Integer id;
    private Integer userId;
    private Integer goodsId;
    private String goodsSn;
    private String goodsName;
    private Integer productId;
    private Integer price;
    private Integer number;
    private List<String> specifications;
    private Boolean checked;
    private String picUrl;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date addTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    private Boolean deleted;
}
