package com.cskaoyan.bean.wx.cataloge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * http://182.92.235.201:8083/wx/catalog/index
 * @author gzh
 * @version 1.0
 * @date 2022/11/22 21:31:52
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CatalogVo {
    // 类目列表
    List<CatalogVo1> categoryList;
    // 当前类目
    CatalogVo2 currentCategory;
    // 子类目
    List<CatalogVo3> currentSubCategory;

}
