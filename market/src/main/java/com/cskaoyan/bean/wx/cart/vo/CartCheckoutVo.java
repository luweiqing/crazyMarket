package com.cskaoyan.bean.wx.cart.vo;

import com.cskaoyan.bean.po.MarketCart;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/23 09:57
 */

@NoArgsConstructor
@Data
public class CartCheckoutVo {

    private Integer grouponRulesId;
    private Double actualPrice;
    private Double orderTotalPrice;
    private Integer cartId;
    private Integer userCouponId;
    private Integer couponId;
    private Double goodsTotalPrice;
    private Integer addressId;
    private Double grouponPrice;
    private CartCheckoutCheckedAddressVo checkedAddress;
    private Double couponPrice;
    private Integer availableCouponLength;
    private Double freightPrice;
    private List<MarketCart> checkedGoodsList;
}
