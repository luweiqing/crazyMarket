package com.cskaoyan.bean.wx.order.vo;

import com.cskaoyan.bean.po.MarketOrderGoods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/22 15:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxOrderDetailVo {
    private List expressInfo;
    private WxOrderInfoOfWxOrderDetailVo orderInfo;
    private List<MarketOrderGoods> orderGoods;
}
