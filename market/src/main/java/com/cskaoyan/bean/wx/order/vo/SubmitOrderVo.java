package com.cskaoyan.bean.wx.order.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhenyu
 * @since 2022/11/22 20:01
 */

@NoArgsConstructor
@Data
@AllArgsConstructor
public class SubmitOrderVo {

    private Integer orderId;
    private Integer grouponLinkId;
}
