package com.cskaoyan.bean.wx.cart.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/22 15:52
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CartIndexCartTotalVo {
    private Integer goodsCount;
    private Integer checkedGoodsCount;
    private Double goodsAmount;
    private Double checkedGoodsAmount;
}
