package com.cskaoyan.bean.wx.coupon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-22 10:55
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxCouponSelectListVO {

    /**
     * id : 18662   用户优惠券id
     * cid : 141    优惠券id
     * name : 满10元10元 优惠券名字
     * desc : 优惠券描述
     * tag : 优惠券标签tag
     * min : 10   满多少
     * discount : 10  减多少
     * startTime : 2022-10-15 15:06:13 有效起始时间
     * endTime : 2022-11-14 15:06:13  有效结束时间
     * available : true  是否可用
     */

    private Integer id;
    private Integer cid;
    private String name;
    private String desc;
    private String tag;
    private BigDecimal min;
    private BigDecimal discount;

    private Date startTime;

    private Date endTime;
    private Boolean available=true;
}
