package com.cskaoyan.bean.wx.coupon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-22 15:05
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxCouponExchangeBO {

    private String code;
}
