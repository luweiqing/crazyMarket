package com.cskaoyan.bean.wx.auth;

import lombok.Data;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  15:03
 * @Version: 1.0
 */
@Data
public class UserInfoDate {
    private String nickName;
    private String avatarUrl;
}