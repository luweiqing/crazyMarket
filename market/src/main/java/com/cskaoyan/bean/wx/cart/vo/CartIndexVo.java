package com.cskaoyan.bean.wx.cart.vo;

import com.cskaoyan.bean.po.MarketCart;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/22 15:40
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CartIndexVo {
    private List<MarketCart> cartList;
    private CartIndexCartTotalVo cartTotal;
}
