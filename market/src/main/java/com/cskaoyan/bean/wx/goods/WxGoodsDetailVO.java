package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.po.*;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 17:03
 */
@AllArgsConstructor
@NoArgsConstructor
public class WxGoodsDetailVO {
    List<MarketGoodsAttribute> attribute;
    MarketBrand brand;
    WxGoodsDetailCommentVO comment;
    List<MarketGrouponRules> groupon;
    MarketGoods info;
    List<MarketIssue> issue;
    List<MarketGoodsProduct> productList;
    boolean share;
    String shareImage;
    List<WxGoodsDetailSpecVO> specificationList;
    Integer userHasCollect;

    public String getShareImage() {
        return shareImage;
    }

    public void setShareImage(String shareImage) {
        this.shareImage = shareImage;
    }

    public List<MarketGoodsAttribute> getAttribute() {
        return attribute;
    }

    public void setAttribute(List<MarketGoodsAttribute> attribute) {
        this.attribute = attribute;
    }

    public MarketBrand getBrand() {
        return brand;
    }

    public void setBrand(MarketBrand brand) {
        this.brand = brand;
    }

    public WxGoodsDetailCommentVO getComment() {
        return comment;
    }

    public void setComment(WxGoodsDetailCommentVO comment) {
        this.comment = comment;
    }

    public List<MarketGrouponRules> getGroupon() {
        return groupon;
    }

    public void setGroupon(List<MarketGrouponRules> groupon) {
        this.groupon = groupon;
    }

    public MarketGoods getInfo() {
        return info;
    }

    public void setInfo(MarketGoods info) {
        this.info = info;
    }

    public List<MarketIssue> getIssue() {
        return issue;
    }

    public void setIssue(List<MarketIssue> issue) {
        this.issue = issue;
    }

    public List<MarketGoodsProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<MarketGoodsProduct> productList) {
        this.productList = productList;
    }

    public boolean isShare() {
        return share;
    }

    public void setShare(boolean share) {
        this.share = share;
    }

    public List<WxGoodsDetailSpecVO> getSpecificationList() {
        return specificationList;
    }

    public void setSpecificationList(List<WxGoodsDetailSpecVO> specificationList) {
        this.specificationList = specificationList;
    }

    public Integer getUserHasCollect() {
        return userHasCollect;
    }

    public void setUserHasCollect(Integer userHasCollect) {
        this.userHasCollect = userHasCollect;
    }
}
