package com.cskaoyan.bean.wx.cart.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/22 19:29
 */

@NoArgsConstructor
@Data
public class CartUpdateBo {
    private Integer productId;
    private Integer goodsId;
    private Integer number;
    private Integer id;
}
