package com.cskaoyan.bean.wx.cataloge;

import com.cskaoyan.bean.po.MarketCategory;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author
 * @version 1.0
 * @date 2022/11/23 10:16:36
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CatalogData {
    MarketCategory currentCategory;
    List<MarketCategory> currentSubCategory;
}
