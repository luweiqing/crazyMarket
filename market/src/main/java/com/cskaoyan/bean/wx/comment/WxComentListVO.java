package com.cskaoyan.bean.wx.comment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 21:47
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxComentListVO {
    Date addTime;
    String adminContent;
    String content;
    String[] picList;
    WxCommentUserInfoVO userInfo;
}
