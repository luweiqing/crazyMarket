package com.cskaoyan.bean.wx.cart.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author lwq
 * @since 2022/11/23 10:00
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CartCheckoutCheckedAddressVo {

    private Integer id;
    private String name;
    private Integer userId;
    private String province;
    private String city;
    private String county;
    private String addressDetail;
    private String areaCode;
    private String tel;
    private Boolean isDefault;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date addTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
    private Boolean deleted;
}
