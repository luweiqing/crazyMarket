package com.cskaoyan.bean.wx.cataloge;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author
 * @version 1.0
 * @date 2022/11/22 21:38:44
 */
@AllArgsConstructor
@Data
@NoArgsConstructor
public class CatalogVo3 {
    private int id;
    private String name;
    private String keywords;
    private String desc;
    private int pid;
    private String iconUrl;
    private String picUrl;
    private String level;
    private int sortOrder;
    private Date addTime;
    private Date updateTime;
    private boolean deleted;
}
