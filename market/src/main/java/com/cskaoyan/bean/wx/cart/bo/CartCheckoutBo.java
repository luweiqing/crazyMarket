package com.cskaoyan.bean.wx.cart.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/23 09:54
 */
@Data
@NoArgsConstructor
public class CartCheckoutBo {
    private Integer cartId;
    private Integer addressId;
    private Integer couponId;
    private Integer userCouponId;
    private Integer grouponRulesId;

}
