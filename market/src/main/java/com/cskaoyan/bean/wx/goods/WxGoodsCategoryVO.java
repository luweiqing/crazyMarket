package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.po.MarketCategory;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.LinkedList;
import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 10:25
 */
@AllArgsConstructor
@NoArgsConstructor
public class WxGoodsCategoryVO {
    List<MarketCategory> brotherCategory;
    MarketCategory currentCategory;
    MarketCategory parentCategory;

    public List<MarketCategory> getBrotherCategory() {
        return brotherCategory;
    }

    public void setBrotherCategory(List<MarketCategory> brotherCategory) {
        this.brotherCategory = brotherCategory;
    }

    public MarketCategory getCurrentCategory() {
        return currentCategory;
    }

    public void setCurrentCategory(MarketCategory currentCategory) {
        this.currentCategory = currentCategory;
    }

    public MarketCategory getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(MarketCategory parentCategory) {
        this.parentCategory = parentCategory;
    }
}
