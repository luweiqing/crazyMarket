package com.cskaoyan.bean.wx.order.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用于接收订单中对商品做出的评价
 * @author wangzhenyu
 * @since 2022/11/23 14:50
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderCommentBo {

    private Integer orderGoodsId;
    private String content;
    private Short star;
    private Boolean hasPicture;
    private String[] picUrls;
}
