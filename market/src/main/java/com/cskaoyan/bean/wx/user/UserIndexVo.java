package com.cskaoyan.bean.wx.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-22  19:42
 * @Version: 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserIndexVo {
    private Integer unpaid; // 待付款，状态码101
    private Integer unship; // 代发货，状态码201
    private Integer unrecv; // 待收货，状态码301
    private Integer uncomment; // 待评价，状态码401
}