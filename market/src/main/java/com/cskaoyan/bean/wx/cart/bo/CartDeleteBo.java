package com.cskaoyan.bean.wx.cart.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/23 15:23
 */

@NoArgsConstructor
@Data
public class CartDeleteBo {

    private List<Integer> productIds;
}
