package com.cskaoyan.bean.wx.footprint;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangzhenyu
 * @since 2022/11/24 14:04
 */

@NoArgsConstructor
@Data
public class FootprintVo {

    private String brief;
    private String picUrl;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    private Integer goodsId;
    private String name;
    private Integer id;
    private BigDecimal retailPrice;
}
