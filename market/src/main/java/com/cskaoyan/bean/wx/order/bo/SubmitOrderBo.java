package com.cskaoyan.bean.wx.order.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用于接收来自前端提交的订单数据
 * @author wangzhenyu
 * @since 2022/11/22 19:59
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubmitOrderBo {

    private Integer cartId;
    private Integer addressId;
    private Integer couponId;
    private Integer userCouponId;
    private String message;
    private Integer grouponRulesId;
    private Integer grouponLinkId;
}
