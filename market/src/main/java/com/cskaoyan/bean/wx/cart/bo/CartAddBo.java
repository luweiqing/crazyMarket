package com.cskaoyan.bean.wx.cart.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/22 16:59
 */

@NoArgsConstructor
@Data
public class CartAddBo {
    private Integer goodsId;
    private Integer number;
    private Integer productId;
}
