package com.cskaoyan.bean.wx.auth;

import lombok.Data;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-25  10:45
 * @Version: 1.0
 */
@Data
public class AuthRegisterVo {
    private UserInfoDate userInfo;
    private String token;
}