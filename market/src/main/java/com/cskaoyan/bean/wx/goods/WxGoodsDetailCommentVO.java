package com.cskaoyan.bean.wx.goods;

import com.cskaoyan.bean.common.CommonData;
import com.cskaoyan.bean.po.MarketComment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/22 17:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WxGoodsDetailCommentVO {
    Integer count;
    List<MarketComment> data;
    // CommonData<MarketComment> data;


}
