package com.cskaoyan.bean.admin.stat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangzhenyu
 * @since 2022/11/21 19:19
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderStat {
    private Integer orders;
    private Integer customers;
    private Double pcr;
    private BigDecimal amount;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GTM+8")
    private Date day;

}
