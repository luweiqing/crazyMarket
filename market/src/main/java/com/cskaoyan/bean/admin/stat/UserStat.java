package com.cskaoyan.bean.admin.stat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author wangzhenyu
 * @since 2022/11/21 19:16
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserStat {
    // 统计日期
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GTM+8")
    private Date day;
    // 当天新增用户的数量
    private Integer users;
}
