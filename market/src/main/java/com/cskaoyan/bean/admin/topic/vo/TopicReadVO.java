package com.cskaoyan.bean.admin.topic.vo;

import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.po.MarketTopic;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 21:45
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicReadVO {

    private List<MarketGoods> goodsList;
    private MarketTopic topic;
}
