package com.cskaoyan.bean.admin.region.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 县区
 * @author
 * @version 1.0
 * @date 2022/11/19 22:51:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChildrenBeanVoc {
    private int id;
    private String name;
    private int type;
    private int code;
}
