package com.cskaoyan.bean.admin.goods;

import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.po.MarketGoodsAttribute;
import com.cskaoyan.bean.po.MarketGoodsProduct;
import com.cskaoyan.bean.po.MarketGoodsSpecification;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/20 20:50
 */
public class GoodsDetailVO {
    Integer[] categoryIds;
    MarketGoods goods;
    MarketGoodsAttribute[] attributes;
    MarketGoodsSpecification[] specifications;
    MarketGoodsProduct[] products;

    public Integer[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(Integer[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    public MarketGoods getGoods() {
        return goods;
    }

    public void setGoods(MarketGoods goods) {
        this.goods = goods;
    }

    public MarketGoodsAttribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(MarketGoodsAttribute[] attributes) {
        this.attributes = attributes;
    }

    public MarketGoodsSpecification[] getSpecifications() {
        return specifications;
    }

    public void setSpecifications(MarketGoodsSpecification[] specifications) {
        this.specifications = specifications;
    }

    public MarketGoodsProduct[] getProducts() {
        return products;
    }

    public void setProducts(MarketGoodsProduct[] products) {
        this.products = products;
    }

    public GoodsDetailVO() {
    }

    public GoodsDetailVO(Integer[] categoryIds, MarketGoods goods, MarketGoodsAttribute[] attributes, MarketGoodsSpecification[] specifications, MarketGoodsProduct[] products) {
        this.categoryIds = categoryIds;
        this.goods = goods;
        this.attributes = attributes;
        this.specifications = specifications;
        this.products = products;
    }
}
