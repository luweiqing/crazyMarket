package com.cskaoyan.bean.admin.role.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/21 20:58
 */

@NoArgsConstructor
@Data
public class RoleSetPermissionsBo {

    private Integer roleId;
    private List<String> permissions;
}
