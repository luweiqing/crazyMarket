package com.cskaoyan.bean.admin.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhenyu
 * @since 2022/11/21 11:10
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourierVo {
    private String code;
    private String name;
}
