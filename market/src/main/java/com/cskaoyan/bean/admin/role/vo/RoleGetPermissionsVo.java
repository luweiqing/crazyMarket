package com.cskaoyan.bean.admin.role.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * "systemPermissions": [],
 * "assignedPermissions": []
 *
 * @author lwq
 * @since 2022/11/21 18:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleGetPermissionsVo {
    private List<RoleSystemPermissionAVo> systemPermissions;
    private List<String> assignedPermissions;
}
