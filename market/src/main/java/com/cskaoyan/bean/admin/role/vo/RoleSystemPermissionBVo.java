package com.cskaoyan.bean.admin.role.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * "id": "类目管理",
 * "label": "类目管理",
 * "children": []
 *
 * @author lwq
 * @since 2022/11/21 18:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleSystemPermissionBVo {
    private String id;
    private String label;
    private List<RoleSystemPermissionCVo> children;
}
