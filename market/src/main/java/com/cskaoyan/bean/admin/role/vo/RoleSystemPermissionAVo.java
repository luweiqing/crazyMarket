package com.cskaoyan.bean.admin.role.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * "id": "商场管理",
 * "label": "商场管理",
 * "children": []
 *
 * @author lwq
 * @since 2022/11/21 18:42
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleSystemPermissionAVo {
    private String id;
    private String label;
    private List<RoleSystemPermissionBVo> children;
}
