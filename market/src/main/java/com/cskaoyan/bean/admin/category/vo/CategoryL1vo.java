package com.cskaoyan.bean.admin.category.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author
 * @version 1.0
 * @date 2022/11/21 14:36:39
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CategoryL1vo {


    private Integer value;
    private String label;

}
