package com.cskaoyan.bean.admin.stat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用于封装行列数据
 * @author wangzhenyu
 * @since 2022/11/21 17:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatVo<T> {
    private String[] columns;
    private List<T> rows;
}
