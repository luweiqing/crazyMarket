package com.cskaoyan.bean.admin.role.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author lwq
 * @since 2022/11/21 14:42
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleCreateVo {
    private Integer id;
    private String name;
    private String desc;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date addTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
