package com.cskaoyan.bean.admin.profile;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-23  23:11
 * @Version: 1.0
 */
public class ProfilePasswordBo {
    /**
     * newPassword2 : admin123
     * oldPassword : admin123
     * newPassword : admin123
     */
    private String newPassword2;
    private String oldPassword;
    private String newPassword;

    public void setNewPassword2(String newPassword2) {
        this.newPassword2 = newPassword2;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPassword2() {
        return newPassword2;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }
}