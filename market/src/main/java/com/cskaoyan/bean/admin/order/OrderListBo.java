package com.cskaoyan.bean.admin.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 接收请求订单信息的类
 * @author wangzhenyu
 * @since 2022/11/19 16:39
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderListBo {
    //      * @param start            查询的订单开始时间
    //      * @param end              查询的订单结束时间
    //      * @param orderSn          订单编号
    //      * @param userId           用户ID
    //      * @param orderStatusArray 订单状态的数组
    //      * @param timeArray        订单的时间范围
    // @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    String start;
    // @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    String end;
    String orderSn;
    Integer userId;
    Short[] orderStatusArray;
    // @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    String[] timeArray;

}
