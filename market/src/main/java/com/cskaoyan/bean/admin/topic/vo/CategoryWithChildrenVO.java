package com.cskaoyan.bean.admin.topic.vo;

import com.cskaoyan.bean.po.MarketCategory;

import java.util.Date;
import java.util.List;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 14:47
 */


public class CategoryWithChildrenVO {
    private Integer id;

    private String name;

    private String keywords;

    private String desc;

    private Integer pid;

    private String iconUrl;

    private String picUrl;

    private String level;

    private Byte sortOrder;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    private List<MarketCategory> children;


    public CategoryWithChildrenVO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Byte getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Byte sortOrder) {
        this.sortOrder = sortOrder;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<MarketCategory> getChildren() {
        return children;
    }

    public void setChildren(List<MarketCategory> children) {
        this.children = children;
    }

    public CategoryWithChildrenVO(MarketCategory marketCategory) {
        this.id = marketCategory.getId();
        this.name = marketCategory.getName();
        this.keywords = marketCategory.getKeywords();
        this.desc = marketCategory.getDesc();
        this.pid = marketCategory.getPid();
        this.iconUrl = marketCategory.getIconUrl();
        this.picUrl = marketCategory.getPicUrl();
        this.level = marketCategory.getLevel();
        this.sortOrder = marketCategory.getSortOrder();
        this.addTime = marketCategory.getAddTime();
        this.updateTime = marketCategory.getUpdateTime();
        this.deleted = marketCategory.getDeleted();

    }
}
