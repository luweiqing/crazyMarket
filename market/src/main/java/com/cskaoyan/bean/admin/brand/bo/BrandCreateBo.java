package com.cskaoyan.bean.admin.brand.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *{"name":"111","desc":"111","floorPrice":"111","picUrl":"http://182.92.235.201:8083/wx/storage/fetch/qtble0mt0mte4yyi6eh5.png"}
 * @author gzh
 * @version 1.0
 * @date 2022/11/20 15:22:44
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class BrandCreateBo {

    private String name;
    private String desc;
    private String floorPrice;
    private String picUrl;
}
