package com.cskaoyan.bean.admin.order;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author wangzhenyu
 * @since 2022/11/19 16:13
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderListVo {

    /**
     * consignee : 桐人
     * address : 北京市市辖区东城区 故宫
     * comments : 0
     * addTime : 2022-07-15 22:54:17
     * orderSn : 20220715931722
     * actualPrice : 599
     * shipTime : 2022-07-15 23:24:48
     * mobile : 18899990000
     * orderStatus : 401
     * confirmTime : 2022-07-15 23:24:54
     * updateTime : 2022-07-15 23:25:08
     * message :
     * shipChannel : EMS
     * userId : 1
     * grouponPrice : 0
     * deleted : false
     * aftersaleStatus : 0
     * goodsPrice : 599
     * couponPrice : 0
     * orderPrice : 599
     * id : 6
     * freightPrice : 0
     * integralPrice : 0
     * shipSn : qqqq
     */

    private Integer id;
    private Integer userId;
    private String orderSn;
    private Short orderStatus;
    private Short aftersaleStatus;
    private String consignee;
    private String mobile;
    private String address;
    private String message;
    private BigDecimal goodsPrice;
    private BigDecimal freightPrice;
    private BigDecimal couponPrice;
    private BigDecimal integralPrice;
    private BigDecimal grouponPrice;
    private BigDecimal orderPrice;
    private BigDecimal actualPrice;
    private String shipSn;
    private String shipChannel;
    private Date shipTime;
    private Date confirmTime;
    private Short comments;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;




}
