package com.cskaoyan.bean.admin.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/24 17:41
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderCommentReplyBO {

    /**
     * commentId : 1059
     * content : 回复
     */
    private int commentId;
    private String content;


}
