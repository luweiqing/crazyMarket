package com.cskaoyan.bean.admin.storage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lwq
 * @since 2022/11/20 20:33
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StorageCreateVo {
    private Integer id;
    private String key;
    private String name;
    private String type;
    private Integer size;
    private String url;
    private String addTime;
    private String updateTime;
}
