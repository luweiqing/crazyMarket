package com.cskaoyan.bean.admin.category.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author
 * @version 1.0
 * @date 2022/11/21 17:10:56
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChildrenBean {
    private int id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    private Integer pid;
}
