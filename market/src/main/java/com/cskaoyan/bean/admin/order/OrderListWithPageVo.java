package com.cskaoyan.bean.admin.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/20 00:40
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class OrderListWithPageVo {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<OrderListVo> list;
}
