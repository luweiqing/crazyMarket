package com.cskaoyan.bean.admin.region.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *省
 * @author gzh
 * @version 1.0
 * @date 2022/11/19 16:32:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegionListVoa {

    private int id;
    private String name;
    private int type;
    private int code;
    private List<ChildrenBeanVob> children;

}
