package com.cskaoyan.bean.admin.topic.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 15:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryWithChildrenResponseVO<T> {
    Integer limit;
    Integer pages;
    Integer page;
    Long total;
    List<T> list;

}
