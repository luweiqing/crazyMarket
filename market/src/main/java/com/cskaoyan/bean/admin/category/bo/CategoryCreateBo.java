package com.cskaoyan.bean.admin.category.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author gzh
 * @version 1.0
 * @date 2022/11/21 15:48:53
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class CategoryCreateBo {

    private String name;
    private String keywords;
    private String level;
    private Integer pid;
    private String desc;
    private String iconUrl;
    private String picUrl;
}
