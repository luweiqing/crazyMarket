package com.cskaoyan.bean.admin.brand.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author gzh
 * @version 1.0
 * @date 2022/11/20 13:48:47
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BrandListVo {

        private int id;
        private String name;
        private String desc;
        private String picUrl;
        private int sortOrder;
        private int floorPrice;
        private Date addTime;
        private Date updateTime;
        private boolean deleted;



}
