package com.cskaoyan.bean.admin.dashboard;

/**
 * @Description:
 * @Author: Pinocao
 * @CreateTime: 2022-11-19  21:08
 * @Version: 1.0
 */
public class DashboardVo {
    /**
     * goodsTotal : 239
     * userTotal : 1
     * productTotal : 244
     * orderTotal : 18
     */
    private long goodsTotal;
    private long userTotal;
    private long productTotal;
    private long orderTotal;

    public void setGoodsTotal(long goodsTotal) {
        this.goodsTotal = goodsTotal;
    }

    public void setUserTotal(long userTotal) {
        this.userTotal = userTotal;
    }

    public void setProductTotal(long productTotal) {
        this.productTotal = productTotal;
    }

    public void setOrderTotal(long orderTotal) {
        this.orderTotal = orderTotal;
    }

    public long getGoodsTotal() {
        return goodsTotal;
    }

    public long getUserTotal() {
        return userTotal;
    }

    public long getProductTotal() {
        return productTotal;
    }

    public long getOrderTotal() {
        return orderTotal;
    }
}