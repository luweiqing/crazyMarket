package com.cskaoyan.bean.admin.admin.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author lwq
 * @since 2022/11/19 15:42
 */

@NoArgsConstructor
@Data
@AllArgsConstructor
public class AdminListVo {
    private Integer id;
    private String username;
    private String avatar;
    private Integer[] roleIds;
}
