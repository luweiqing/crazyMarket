package com.cskaoyan.bean.admin.storage.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author lwq
 * @since 2022/11/21 16:14
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class StorageUpdateVo {

    private Integer id;
    private String key;
    private String name;
    private String type;
    private Integer size;
    private String url;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date addTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;
}
