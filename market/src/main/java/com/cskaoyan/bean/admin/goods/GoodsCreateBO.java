package com.cskaoyan.bean.admin.goods;

import com.cskaoyan.bean.po.MarketGoods;
import com.cskaoyan.bean.po.MarketGoodsAttribute;
import com.cskaoyan.bean.po.MarketGoodsProduct;
import com.cskaoyan.bean.po.MarketGoodsSpecification;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/21 11:17
 */
@NoArgsConstructor
@AllArgsConstructor
public class GoodsCreateBO {
    MarketGoodsAttribute[] attributes;
    MarketGoods goods;
    MarketGoodsProduct[] products;
    MarketGoodsSpecification[] specifications;

    public MarketGoodsAttribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(MarketGoodsAttribute[] attributes) {
        this.attributes = attributes;
    }

    public MarketGoods getGoods() {
        return goods;
    }

    public void setGoods(MarketGoods goods) {
        this.goods = goods;
    }

    public MarketGoodsProduct[] getProducts() {
        return products;
    }

    public void setProducts(MarketGoodsProduct[] products) {
        this.products = products;
    }

    public MarketGoodsSpecification[] getSpecifications() {
        return specifications;
    }

    public void setSpecifications(MarketGoodsSpecification[] specifications) {
        this.specifications = specifications;
    }
}
