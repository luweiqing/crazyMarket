package com.cskaoyan.bean.admin.role.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * "id": "admin:category:update",
 * "label": "编辑",
 * "api": "POST /admin/category/update"
 *
 * @author lwq
 * @since 2022/11/21 18:59
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleSystemPermissionCVo {
    private String id;
    private String label;
    private String api;
}
