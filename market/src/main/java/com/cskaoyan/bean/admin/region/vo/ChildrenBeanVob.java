package com.cskaoyan.bean.admin.region.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 市
 * @author gzh
 * @version 1.0
 * @date 2022/11/19 22:43:06
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ChildrenBeanVob {
    private int id;
    private String name;
    private int type;
    private int code;
    private List<ChildrenBeanVoc> children;
}
