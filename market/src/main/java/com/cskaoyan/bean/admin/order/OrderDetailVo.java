package com.cskaoyan.bean.admin.order;

import com.cskaoyan.bean.po.MarketOrder;
import com.cskaoyan.bean.po.MarketOrderGoods;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author wangzhenyu
 * @since 2022/11/20 19:03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailVo {
    private List<MarketOrderGoods> orderGoods;
    private UserInOrderDetail user;
    private MarketOrder order;
}
