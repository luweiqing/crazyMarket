package com.cskaoyan.bean.admin.order;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wangzhenyu
 * @since 2022/11/20 19:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserInOrderDetail {
    private String nickname;
    private String avatar;

}
