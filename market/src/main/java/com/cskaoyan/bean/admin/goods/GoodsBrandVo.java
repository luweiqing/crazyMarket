package com.cskaoyan.bean.admin.goods;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/21 15:28
 */
@NoArgsConstructor
@AllArgsConstructor
public class GoodsBrandVo {
    Integer value;
    String label;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
