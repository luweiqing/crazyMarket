package com.cskaoyan.bean.admin.goods;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/21 15:30
 */
@AllArgsConstructor
@NoArgsConstructor
public class GoodsCateAndBrandVO {
    GoodsCategoryVO[] categoryList;
    GoodsBrandVo[] brandList;

    public GoodsCategoryVO[] getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(GoodsCategoryVO[] categoryList) {
        this.categoryList = categoryList;
    }

    public GoodsBrandVo[] getBrandList() {
        return brandList;
    }

    public void setBrandList(GoodsBrandVo[] brandList) {
        this.brandList = brandList;
    }
}
