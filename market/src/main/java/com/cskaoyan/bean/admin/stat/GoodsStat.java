package com.cskaoyan.bean.admin.stat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;

/**
 * @author wangzhenyu
 * @since 2022/11/21 17:27
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class GoodsStat {
    // amount:当天的下单货品总额
    private BigDecimal amount;
    // 当天的订单数量
    private Integer orders;
    // 统计的是哪一天的
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GTM+8")
    private String day;
    // 下单货品的总数量
    private Integer products;
}
