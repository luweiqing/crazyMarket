package com.cskaoyan.bean.admin.goods;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * @Description：
 * @Author： 黄武
 * @Date： 2022/11/20 21:55
 */
@NoArgsConstructor
@AllArgsConstructor
public class GoodsCategoryVO {
    Integer value;
    String label;
    GoodsCategoryVO[] children;

    public GoodsCategoryVO[] getChildren() {
        return children;
    }

    public void setChildren(GoodsCategoryVO[] children) {
        this.children = children;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


}
