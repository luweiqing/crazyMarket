package com.cskaoyan.bean.admin.topic.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-20 22:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TopicBatchDeleteBO {
    private Integer[] ids;
}
