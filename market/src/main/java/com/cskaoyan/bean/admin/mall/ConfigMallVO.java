package com.cskaoyan.bean.admin.mall;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-21 16:04
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ConfigMallVO {

    private String market_order_unpaid;
    private String market_wx_index_new;
    private String market_mall_latitude;
    private String market_order_unconfirm;
    private String market_wx_share;
    private String market_express_freight_min;
    private String market_mall_name;
    private String market_express_freight_value;
    private String market_mall_qq;
    private String market_wx_index_hot;
    private String market_order_comment;
    private String market_wx_catlog_goods;
    private String market_mall_longitude;
    private String market_mall_phone;
    private String market_wx_catlog_list;
    private String market_mall_address;
    private String market_wx_index_brand;
    private String market_wx_index_topic;
}
