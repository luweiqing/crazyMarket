package com.cskaoyan.bean.admin.category.bo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author gzh
 * @version 1.0
 * @date 2022/11/21 17:09:41
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class CategoryUpdateBo {

    private Integer id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    private Integer pid;

    private List<ChildrenBean> children;


}
