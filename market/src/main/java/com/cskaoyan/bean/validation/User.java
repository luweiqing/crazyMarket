package com.cskaoyan.bean.validation;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

/**
 * @author stone
 * @date 2022/09/08 10:14
 */
@Data
public class User{
    @Length(min=6)
    String username;
    @Length(min=6,max=10)
    String password;
    @Min(18)
    @Max(value = 70,message = "age limit 18~70")
    Integer age;
}
