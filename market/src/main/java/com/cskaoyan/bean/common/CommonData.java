package com.cskaoyan.bean.common;




import com.github.pagehelper.PageInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author stone
 * @date 2022/04/06 10:13
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommonData<T> {

    /*limit: 20
    list: [{id: 1152101, goodsSn: "1152101", name: "魔兽世界 部落 奥格瑞玛 拉杆箱 可登机", categoryId: 1012001, brandId: 0,…},…]
    page: 6
    pages: 13
    total: 241*/
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<T> list;


    public static CommonData data(PageInfo pageInfo) {
        CommonData data = new CommonData();
        data.setTotal((int) pageInfo.getTotal());
        data.setPages(pageInfo.getPages());
        data.setPage(pageInfo.getPageNum());
        data.setLimit(pageInfo.getPageSize());
        data.setList(pageInfo.getList());
        return data;
    }


    // public static CommonData data(PageInfo pageInfo) {
    //     CommonData data = new CommonData();
    //     data.setTotal((int) pageInfo.getTotal());
    //     data.setPages(pageInfo.getPages());
    //     data.setPage(pageInfo.getPageNum());
    //     data.setLimit(pageInfo.getPageSize());
    //     data.setList(pageInfo.getList());
    //     return data;
    // }



    public static <T> CommonData<T> data2(PageInfo<T> pageInfo) {
        int pageNum = pageInfo.getPageNum();
        int pageSize = pageInfo.getPageSize();
        int pages = pageInfo.getPages();
        long total = pageInfo.getTotal();
        List<T> list = pageInfo.getList();
        return new CommonData<T>((int) total,pages,pageSize,pageNum,list);
    }
}
