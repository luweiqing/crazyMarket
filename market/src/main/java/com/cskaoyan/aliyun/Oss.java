package com.cskaoyan.aliyun;

import lombok.Data;
/*
 * @Author makabaka
 * @Description //TODO 
 * @Date 20:58 2022/11/22
 * @Param 
 * @return 
 **/
@Data
public class Oss {
    String bucket;
    String endPoint;
}
