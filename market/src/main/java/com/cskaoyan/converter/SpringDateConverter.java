package com.cskaoyan.converter;

import lombok.SneakyThrows;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * TODO
 *
 * @author makabaka
 * @date 2022-11-21 20:29
 */
@Component
public class SpringDateConverter implements Converter<String, Date> {


    @SneakyThrows
    @Override
    public Date convert(String source) {
        SimpleDateFormat simpleDateFormat;
        if(source.length()==19){
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        }else {
            simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        }
        Date date = simpleDateFormat.parse(source);
        return  date;

    }
}
